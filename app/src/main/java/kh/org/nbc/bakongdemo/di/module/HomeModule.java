package kh.org.nbc.bakongdemo.di.module;

import dagger.Binds;
import dagger.Module;
import kh.org.nbc.bakongdemo.screen.home.HomeRepository;
import kh.org.nbc.bakongdemo.screen.home.HomeRepositoryImpl;

@Module
public abstract class HomeModule {

    @Binds
    abstract HomeRepository provideHomeRepository(HomeRepositoryImpl homeRepository);

}
