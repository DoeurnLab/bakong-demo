package kh.org.nbc.bakongdemo.screen.login.data.request

import com.google.gson.annotations.SerializedName

data class GrantRecoveryAccountRequest(
    @SerializedName("transaction")
    val transaction: ByteArray
) {
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as GrantRecoveryAccountRequest

        if (!transaction.contentEquals(other.transaction)) return false

        return true
    }

    override fun hashCode(): Int {
        return transaction.contentHashCode()
    }
}