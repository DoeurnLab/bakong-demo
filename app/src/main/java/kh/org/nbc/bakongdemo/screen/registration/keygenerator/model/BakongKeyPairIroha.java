package kh.org.nbc.bakongdemo.screen.registration.keygenerator.model;

public class BakongKeyPairIroha implements BakongKeyPair {

    private final String mPublicKey;
    private final String mPrivateKey;

    public BakongKeyPairIroha(String publicKey, String privateKey) {
        mPublicKey = publicKey;
        mPrivateKey = privateKey;
    }

    @Override
    public String getPublicKey() {
        return mPublicKey;
    }

    @Override
    public String getPrivateKey() {
        return mPrivateKey;
    }
}
