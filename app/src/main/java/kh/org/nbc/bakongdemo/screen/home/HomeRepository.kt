package kh.org.nbc.bakongdemo.screen.home

import io.reactivex.Single
import kh.org.nbc.bakongdemo.screen.home.data.AccountAsset
import kh.org.nbc.bakongdemo.screen.home.data.TransactionData
import kh.org.nbc.bakongdemo.screen.home.data.TransactionRequest

interface HomeRepository {
    fun getBalance(): Single<AccountAsset>
    fun getTransactions(request: TransactionRequest): Single<TransactionData>
}