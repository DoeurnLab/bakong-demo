package kh.org.nbc.bakongdemo.network.data;

public interface NetworkApiCreator {

    <T> T create(final Class<T> service);

}
