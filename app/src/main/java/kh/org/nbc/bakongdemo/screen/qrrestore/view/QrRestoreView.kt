package kh.org.nbc.bakongdemo.screen.qrrestore.view


interface QrRestoreView {
    fun showQrScanner()

    fun showRestoreErrorMessage()

    fun enableButton(flag: Boolean)

    fun showPasscodeErrorMessage()

    fun showProgress(show: Boolean)

    fun showStoragePermissionRequest()

    fun showPermissionNotGranted()
}