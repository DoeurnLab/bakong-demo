package kh.org.nbc.bakongdemo.application

import android.app.Application
import android.util.Log
import kh.org.nbc.bakongdemo.di.component.AppComponent
import kh.org.nbc.bakongdemo.di.component.DaggerAppComponent


open class BakongApplication : Application() {

    var rielAccount: Double? = null
    var usdAccount: Double? = null

    //Instance of the AppComponent that will be used by all the Activities in the project
    val appComponent: AppComponent by lazy {
        // Create an instance of AppComponent using its Factory constructor
        // We pass the applicationContext that will be used as Context in the graph
        DaggerAppComponent.factory().create(applicationContext)
    }

    override fun onCreate() {
        super.onCreate()
        Log.d("", "")

    }




}