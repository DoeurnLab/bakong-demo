package kh.org.nbc.bakongdemo.screen.username.data;

import java.util.Objects;

public class AccountCreationModel {

    private String mAccountId;
    private boolean mAccountVerified;
    private String mHash;

    public String getAccountId() {
        return mAccountId;
    }

    public void setAccountId(String accountId) {
        mAccountId = accountId;
    }

    public boolean isAccountVerified() {
        return mAccountVerified;
    }

    public void setAccountVerified(boolean accountVerified) {
        mAccountVerified = accountVerified;
    }

    public String getHash() {
        return mHash;
    }

    public void setHash(String hash) {
        mHash = hash;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        AccountCreationModel model = (AccountCreationModel) o;
        return mAccountVerified == model.mAccountVerified &&
                Objects.equals(mAccountId, model.mAccountId) &&
                Objects.equals(mHash, model.mHash);
    }

    @Override
    public int hashCode() {
        return Objects.hash(mAccountId, mAccountVerified, mHash);
    }

    @Override
    public String toString() {
        return "AccountCreationModel{" +
                "mAccountId='" + mAccountId + '\'' +
                ", mAccountVerified=" + mAccountVerified +
                ", mHash='" + mHash + '\'' +
                "} " + super.toString();
    }
}
