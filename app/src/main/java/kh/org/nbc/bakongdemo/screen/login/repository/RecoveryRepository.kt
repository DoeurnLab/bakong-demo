package kh.org.nbc.bakongdemo.screen.login.repository

import io.reactivex.Single
import kh.org.nbc.bakongdemo.screen.login.data.response.CreateRecoveryData
import kh.org.nbc.bakongdemo.screen.login.data.response.GrantRecoveryData

interface RecoveryRepository {
    fun createRecoveryAccount(accountId: String, hash: String): Single<CreateRecoveryData>

    fun grantRecoveryAccount(transaction: ByteArray): Single<GrantRecoveryData>

    fun status(): Single<Boolean>
}