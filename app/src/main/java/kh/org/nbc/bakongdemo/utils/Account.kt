package kh.org.nbc.bakongdemo.utils

data class Account(
    val id: String,
    val money: Money? = null
) {
    val currency: Currency?
        get() = money?.currency
}