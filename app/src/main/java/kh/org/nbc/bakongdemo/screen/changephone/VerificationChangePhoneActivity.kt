package kh.org.nbc.bakongdemo.screen.changephone

import android.content.Intent
import android.os.Bundle
import android.os.CountDownTimer
import android.view.View
import androidx.lifecycle.Observer
import kh.org.nbc.bakongdemo.R
import kh.org.nbc.bakongdemo.application.BakongApplication
import kh.org.nbc.bakongdemo.base.BaseActivity
import kh.org.nbc.bakongdemo.screen.login.LoginActivity
import kh.org.nbc.bakongdemo.utils.toast
import kh.org.nbc.bakongdemo.utils.gone
import kh.org.nbc.bakongdemo.utils.show
import kotlinx.android.synthetic.main.activity_verification.button_request_code
import kotlinx.android.synthetic.main.activity_verification.text_view_timer
import kotlinx.android.synthetic.main.activity_verification.view_timer
import kotlinx.android.synthetic.main.activity_verification_change_phone.*
import kotlinx.android.synthetic.main.layout_toolbar.*
import javax.inject.Inject

class VerificationChangePhoneActivity : BaseActivity() {

    @Inject
    lateinit var viewModel: ChangePhoneViewModel
    private lateinit var mTimer: CountDownTimer

    override fun onCreate(savedInstanceState: Bundle?) {
        (application as BakongApplication).appComponent.inject(this)
        super.onCreate(savedInstanceState)
        setContentView( R.layout.activity_verification_change_phone)

        viewModel.loading.observe(this, Observer {
            if (it) {
                progressBar.show()
            } else {
                progressBar.gone()
            }
        })

        edit_text_otp_number.requestFocus()

        viewModel.success.observe(this, Observer {
            if (it) {
                val intent = Intent(this, LoginActivity::class.java)
                intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK
                startActivity(intent)
            }else{
                toast("Code verification was failed")
            }
        })


        viewModel.error.observe(this, Observer {
            onError(it)
        })
        button_back.show()
        button_back.setOnClickListener {
            finish()
        }

        startTimer()
        button_request_code.setOnClickListener {
            viewModel.onResendCode()
            startTimer()
        }
        text_view_title.text = resources.getString(R.string.verification)
        button_next.show()

        button_next.setOnClickListener {
            viewModel.verify(edit_text_otp_number.text.toString())
        }
    }

    private fun startTimer() {
        enableButton(false)
        mTimer = object : CountDownTimer(TIMER, INTERVAL) {
            override fun onTick(millisUntilFinished: Long) {
                val timer = millisUntilFinished / 1000
                text_view_timer.text = "00:$timer"
            }

            override fun onFinish() {
                enableButton(true)
            }
        }
        mTimer.start()


    }

    private fun enableButton(enable: Boolean) {
        button_request_code.isEnabled = enable
        button_request_code.isClickable = enable
        if (enable) {
            view_timer.visibility = View.INVISIBLE
            button_request_code.alpha = 1f
        } else {
            button_request_code.alpha = 0.4f
            view_timer.visibility = View.VISIBLE
        }
    }

    companion object {
        const val TIMER = 60000L
        const val INTERVAL = 1000L
    }
}