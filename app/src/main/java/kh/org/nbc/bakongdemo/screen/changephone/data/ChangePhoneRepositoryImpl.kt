package kh.org.nbc.bakongdemo.screen.changephone.data

import io.reactivex.Single
import kh.org.nbc.bakongdemo.screen.changephone.data.request.ChangePhoneRequest
import jp.co.soramitsu.feature_change_phone_impl.data.request.VerifyCodeRequest
import kh.org.nbc.bakongdemo.screen.changephone.data.response.ChangePhoneData
import kh.org.nbc.bakongdemo.screen.qrrestore.utils.MessageSignature
import kh.org.nbc.bakongdemo.screen.registration.api.BakongApi
import kh.org.nbc.bakongdemo.screen.registration.data.InternationalPhone
import kh.org.nbc.bakongdemo.screen.registration.data.RestoreMessage
import kh.org.nbc.bakongdemo.storage.Storage
import javax.inject.Inject

class ChangePhoneRepositoryImpl @Inject constructor(
    private val storage: Storage,
    private val mBakongApi: BakongApi
) : ChangePhoneRepository {

    override fun restoreMessage(): Single<RestoreMessage> {
        return mBakongApi.fetchRestoreMessage()
            .map { response -> response.data }
    }

    override fun changePhone(
        phone: InternationalPhone,
        requestId: Long,
        messageSignature: MessageSignature,
        publicKey: String
    ): Single<ChangePhoneData> {
        return mBakongApi.changePhone(
            ChangePhoneRequest(
                deviceId = storage.getDeviceId()!!,
                phoneNumber = phone.getPhoneNumber(),
                publicKey = publicKey,
                signature = messageSignature.signature,
                versionOS = "Android",
                requestId = requestId
            )
        ).map { response -> response.data }
    }

    override fun verifyPhone(
        verificationCode: String,
        requestId: Long,
        publicKey: String
    ): Single<Boolean> {
        return mBakongApi.verifyChangePhone(
            VerifyCodeRequest(
                publicKey = publicKey,
                requestId = requestId,
                verificationCode = verificationCode
            )
        ).map { response -> response.data.verified }
    }
}