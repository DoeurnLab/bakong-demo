package kh.org.nbc.bakongdemo.screen.registration.data

import com.google.gson.annotations.SerializedName

data class Registration(
    @SerializedName("success")
    val isSuccess: Boolean,
    @SerializedName("loginOptions")
    val loginOptions: List<String>?,
    @SerializedName("bankDomain")
    val bankDomain: String
)