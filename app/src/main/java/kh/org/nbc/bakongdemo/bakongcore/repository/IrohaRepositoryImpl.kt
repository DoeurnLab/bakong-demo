package kh.org.nbc.bakongdemo.bakongcore.repository

import io.reactivex.Single
import iroha.protocol.Primitive
import iroha.protocol.TransactionOuterClass
import jp.co.soramitsu.iroha.java.Transaction
import jp.co.soramitsu.iroha.java.Utils
import kh.org.nbc.bakongdemo.screen.registration.keygenerator.model.BakongKeyPair
import kh.org.nbc.bakongdemo.screen.registration.keygenerator.model.BakongKeyPairIroha
import kh.org.nbc.bakongdemo.screen.send.data.PaymentPayload
import kh.org.nbc.bakongdemo.screen.send.data.PaymentSignature
import kh.org.nbc.bakongdemo.storage.Storage
import kh.org.nbc.bakongdemo.utils.GrantEditTransaction
import kh.org.nbc.bakongdemo.utils.GrantPermissionTransaction
import java.security.KeyPair
import javax.inject.Inject
import javax.xml.bind.DatatypeConverter

class IrohaRepositoryImpl @Inject
internal constructor(
    private val storage: Storage
) : IrohaRepository {

    private val keysInternal: BakongKeyPair?
        get() {
            var keyPair: BakongKeyPair? = null
            val publicKey = storage.getPublicKey()
            val privateKey = storage.getPrivateKey()
            if (!publicKey.isNullOrEmpty() && !privateKey.isNullOrEmpty()) {
                keyPair = BakongKeyPairIroha(publicKey, privateKey)
            }
            return keyPair
        }

    private val keyPair: KeyPair
        get() {
            val bakongKeyPair = keysInternal
            return Utils.parseHexKeypair(
                bakongKeyPair!!.getPublicKey(),
                bakongKeyPair.getPrivateKey()
            )
        }


    override fun createGrantEditSignatoryTransactionHash(accountId: String): Single<GrantEditTransaction> {
        return Single.fromCallable {
            grantEditSignatoryTransactionHash(accountId)
        }
    }

    private fun grantEditSignatoryTransactionHash(accountId: String): GrantEditTransaction {
        return Transaction.builder(accountId)
            .grantPermissions(
                "$accountId.recovery", listOf(
                    Primitive.GrantablePermission.can_add_my_signatory,
                    Primitive.GrantablePermission.can_remove_my_signatory
                )
            )
            .build()
            .build()
            .let {
                GrantEditTransaction(
                    Utils.toHex(
                        Utils.reducedHash(it)
                    ), it.toByteArray()
                )
            }
    }

    override fun grantPermission(grantPermissionTransaction: GrantPermissionTransaction): Single<ByteArray> {
        return Single.fromCallable { grantPermissionSignatory(grantPermissionTransaction) }
    }

    override fun signPayment(paymentPayload: PaymentPayload): Single<PaymentSignature> {
        return Single.fromCallable { sign(paymentPayload) }
    }

    private fun grantPermissionSignatory(grantPermissionTransaction: GrantPermissionTransaction): ByteArray? {
        val transaction = Transaction.parseFrom(grantPermissionTransaction.transaction)
        return transaction
            .makeMutable()
            .setBatchMeta(
                TransactionOuterClass.Transaction.Payload.BatchMeta.BatchType.ATOMIC,
                listOf(
                    Utils.toHex(Utils.reducedHash(transaction)),
                    grantPermissionTransaction.grandPermissionHash,
                    grantPermissionTransaction.addSignatoryHash
                )
            )
            .build()
            .sign(keyPair)
            .build()
            .toByteArray()
    }

    private fun sign(paymentPayload: PaymentPayload): PaymentSignature {
        val transaction = Transaction.builder(paymentPayload.senderAccountId, paymentPayload.time)
            .transferAsset(
                paymentPayload.senderAccountId,
                paymentPayload.receiverAccountId,
                paymentPayload.assetId,
                paymentPayload.description,
                paymentPayload.amount
            )
            .sign(keyPair)
            .build()

        val hash = Utils.hash(transaction)

        return PaymentSignature(transaction.toByteArray(), DatatypeConverter.printHexBinary(hash))
    }



}
