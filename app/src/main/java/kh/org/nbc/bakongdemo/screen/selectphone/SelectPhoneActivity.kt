package kh.org.nbc.bakongdemo.screen.selectphone

import android.content.Intent
import android.os.Bundle
import android.text.TextUtils
import androidx.lifecycle.Observer
import kh.org.nbc.bakongdemo.R
import kh.org.nbc.bakongdemo.application.BakongApplication
import kh.org.nbc.bakongdemo.base.BaseActivity
import kh.org.nbc.bakongdemo.screen.send.SendActivity
import kh.org.nbc.bakongdemo.utils.hide
import kh.org.nbc.bakongdemo.utils.show
import kotlinx.android.synthetic.main.activity_select_phone.*
import kotlinx.android.synthetic.main.layout_toolbar.text_view_title
import kotlinx.android.synthetic.main.layout_toolbar.button_back
import javax.inject.Inject

class SelectPhoneActivity : BaseActivity() {

    @Inject
    lateinit var viewModel: SelectPhoneViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        (application as BakongApplication).appComponent.inject(this)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_select_phone)

        button_next_screen.setOnClickListener {
            val phoneNumber = edit_text_phone_number.text
            if (!TextUtils.isEmpty(phoneNumber) && phoneNumber.startsWith("855")){
                viewModel.initializePaymentPhone(phoneNumber.toString())
            }
        }


        text_view_title.text = resources.getString(R.string.phone_number)
        button_back.show()
        button_back.setOnClickListener {
            finish()
        }
        viewModel.loading.observe(this, Observer {
            if (it){
                showLoading()
            }else hideLoading()
        })

        viewModel.initializePaymentPhone.observe(this, Observer {
            val intent = Intent(this, SendActivity::class.java)
            intent.putExtra(REC_ID, it.receiverAccountId)
            intent.putExtra(FULL_NAME, it.receiverFullName)
            startActivity(intent)
            finish()
        })

        viewModel.error.observe(this, Observer {
            onError(it)
        })
    }

    private fun showLoading(){
        progressBar.show()
    }
    private fun hideLoading(){
        progressBar.hide()
    }

    companion object{
        const val REC_ID = "receive id"
        const val FULL_NAME = "full name"
    }
}