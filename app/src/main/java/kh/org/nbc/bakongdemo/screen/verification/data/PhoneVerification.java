package kh.org.nbc.bakongdemo.screen.verification.data;

import com.google.gson.annotations.SerializedName;

import java.util.Objects;

public class PhoneVerification {

    @SerializedName("verified")
    private boolean mVerified;

    public boolean isVerified() {
        return mVerified;
    }

    public void setVerified(boolean verified) {
        mVerified = verified;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PhoneVerification that = (PhoneVerification) o;
        return mVerified == that.mVerified;
    }

    @Override
    public int hashCode() {
        return Objects.hash(mVerified);
    }

    @Override
    public String toString() {
        return "PhoneVerification{" +
                "mVerified=" + mVerified +
                '}';
    }
}
