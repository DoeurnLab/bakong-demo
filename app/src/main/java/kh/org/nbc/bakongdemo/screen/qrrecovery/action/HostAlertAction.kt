package kh.org.nbc.bakongdemo.screen.qrrecovery.action

import androidx.fragment.app.DialogFragment
import kh.org.nbc.bakongdemo.bakongcore.alert.AlertAction
import kotlinx.android.parcel.Parcelize

@Parcelize
open class HostAlertAction(private val actionId: Int, val messageRes: Int? = null) : AlertAction(messageRes) {
    override fun onAction(dialogFragment: DialogFragment) {
        val listener = when {
            dialogFragment.parentFragment is HostAlertActionListener -> dialogFragment.parentFragment as HostAlertActionListener
            dialogFragment.activity is HostAlertActionListener -> dialogFragment.activity as HostAlertActionListener
            else -> null
        }
        listener?.onAlertClicked(dialogFragment, actionId)
    }
}

interface HostAlertActionListener {
    fun onAlertClicked(dialogFragment: DialogFragment, actionId: Int)
}

@Parcelize
class OkAction(val positiveMessageRes: Int? = null) : HostAlertAction(ACTION_ID, positiveMessageRes) {
    companion object {
        const val ACTION_ID = -1
    }
}

@Parcelize
class NegativeAction(val negativeMessageRes: Int? = null) : HostAlertAction(ACTION_ID, negativeMessageRes) {
    companion object {
        const val ACTION_ID = -2
    }
}

@Parcelize
class DismissAction : AlertAction() {
    override fun onAction(dialogFragment: DialogFragment) {
        dialogFragment.dismiss()
        dialogFragment.activity?.onBackPressed()
    }
}