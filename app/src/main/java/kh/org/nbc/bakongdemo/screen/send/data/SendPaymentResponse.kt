package kh.org.nbc.bakongdemo.screen.send.data

import com.google.gson.annotations.SerializedName

class SendPaymentResponse {

    @SerializedName("receiverAccountId")
    var receiverAccountId: String? = null

    @SerializedName("receiverFullName")
    var receiverFullName: String? = null


    @SerializedName("assetId")
    var assetId: String? = null

    @SerializedName("description")
    var description: String? = null

    @SerializedName("amount")
    var amount: String? = null

    @SerializedName("transaction")
    var transactionD: String? = null

    @SerializedName("trxHash")
    var trxHash: String? = null

    override fun toString(): String {
        return "SendPaymentResponse(receiverAccountId=$receiverAccountId, receiverFullName=$receiverFullName, assetId=$assetId, description=$description, amount=$amount, transactionD=$transactionD, trxHash=$trxHash)"
    }


}