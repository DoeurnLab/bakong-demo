package kh.org.nbc.bakongdemo.screen.qrrestore.view

import android.Manifest
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Bundle
import android.text.TextUtils
import android.view.View
import android.widget.Toast
import androidx.lifecycle.Observer
import com.google.zxing.integration.android.IntentIntegrator
import kh.org.nbc.bakongdemo.R
import kh.org.nbc.bakongdemo.application.BakongApplication
import kh.org.nbc.bakongdemo.base.BaseActivity
import kh.org.nbc.bakongdemo.screen.login.LoginActivity
import kh.org.nbc.bakongdemo.screen.qrrestore.data.QrRestoreViewModel
import kh.org.nbc.bakongdemo.screen.qrrestore.interactor.QrLauncher
import kh.org.nbc.bakongdemo.storage.Storage
import kh.org.nbc.bakongdemo.utils.gone
import kh.org.nbc.bakongdemo.utils.requestPermission
import kh.org.nbc.bakongdemo.utils.show
import kh.org.nbc.bakongdemo.utils.toast
import kotlinx.android.synthetic.main.activity_qr_restore.*
import kotlinx.android.synthetic.main.layout_toolbar.*
import javax.inject.Inject

class QrRestoreActivity : BaseActivity(),
    QrRestoreView {

    @Inject
    lateinit var qrLauncher: QrLauncher

    @Inject
    lateinit var storage: Storage

    @Inject
    lateinit var viewModel: QrRestoreViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        (application as BakongApplication).appComponent.inject(this)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_qr_restore)

        viewModel.restoreSuccess.observe(this, Observer { success->

            if (success){
                storage.setRegistered(true)
                startActivity(Intent(this, LoginActivity::class.java))
            }else{
                showRestoreErrorMessage()
                enableButton(true)
            }
        })
        viewModel.error.observe(this, Observer {
            onError(it)
        })

        viewModel.stateError.observe(this, Observer {
            if (it){
                showPasscodeErrorMessage()
                enableButton(true)
            }
        })

        button_next_screen.setOnClickListener {
            if (TextUtils.isEmpty(edit_text_password.text)){
                edit_text_password.error = "Enter your password"
            }else{
                showStoragePermissionRequest()
            }
        }

        viewModel.loading.observe(this, Observer {
            if (it) {
                progressBar.show()
            } else {
                progressBar.gone()
            }
            enableButton(!it)

        })
        text_view_title.text = resources.getString(R.string.password)
        button_back.show()
        button_back.setOnClickListener { finish() }
    }

    private fun onPermissionResult(granted: Boolean) {
        if (granted) {
            showQrScanner()
        } else {
            showPermissionNotGranted()
        }
    }

    override fun showQrScanner() {
        qrLauncher.launch(this)
    }
    override fun showRestoreErrorMessage() {
        toast(R.string.registration_restore_fail)
    }

    override fun showPasscodeErrorMessage() {
        toast(R.string.registration_passcode_fail, Toast.LENGTH_LONG)
    }

    override fun showProgress(show: Boolean) {
        progressBar.visibility = if (show) View.VISIBLE else View.GONE
    }


    override fun enableButton(flag: Boolean) {
        button_next.isClickable = flag
        button_next.isEnabled = flag
    }

    override fun showStoragePermissionRequest() {
        val requested = requestPermission(
            Manifest.permission.WRITE_EXTERNAL_STORAGE, EXTERNAL_STORAGE_PERMISSION_CODE,
            getString(R.string.restore_file_access_permision),
            getString(R.string.registration_storage_access_permision_title),
            supportFragmentManager
        )
        if (!requested) onPermissionResult(true)
    }

    override fun showPermissionNotGranted() {
        toast(R.string.registration_qr_restore_permission_not_granted_message)
        enableButton(true)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        val result = IntentIntegrator.parseActivityResult(requestCode, resultCode, data)
        if (data != null && data.hasExtra(QrLauncher.SCAN_ERROR)) {
            toast(R.string.upload_only_qr_code)
            enableButton(true)
        } else if (result != null && result.contents != null) {
            viewModel.restoreAccount(edit_text_password.text.toString(), result.contents)
            return
        } else {
            enableButton(true)
        }
        super.onActivityResult(requestCode, resultCode, data)
    }


    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        when (requestCode) {
            EXTERNAL_STORAGE_PERMISSION_CODE -> {
                val granted =
                    grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED
                onPermissionResult(granted)
            }
        }
    }

    companion object {
        const val SCAN_ERROR = "scan_error"
        private const val EXTERNAL_STORAGE_PERMISSION_CODE = 112

    }
}