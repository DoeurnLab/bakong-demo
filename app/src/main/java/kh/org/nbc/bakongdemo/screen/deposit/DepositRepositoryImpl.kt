package kh.org.nbc.bakongdemo.screen.deposit

import io.reactivex.Single
import kh.org.nbc.bakongdemo.bakongcore.repository.IrohaRepository
import kh.org.nbc.bakongdemo.screen.deposit.data.*
import kh.org.nbc.bakongdemo.screen.registration.api.BakongApi
import kh.org.nbc.bakongdemo.screen.send.data.PaymentPayload
import kh.org.nbc.bakongdemo.screen.send.data.PaymentSignature
import kh.org.nbc.bakongdemo.screen.send.data.ServerAmountFormatter
import kh.org.nbc.bakongdemo.screen.send.data.ServerCurrencyToAssetIdMapper
import kh.org.nbc.bakongdemo.storage.Storage
import javax.inject.Inject

class DepositRepositoryImpl @Inject constructor(
    private val api: BakongApi,
    private val storage: Storage,
    private val irohaRepository: IrohaRepository
): DepositRepository {
    override fun getDepositBankList(): Single<List<BankListResponse>> {
        return api.getBankList().map { it.data }
    }

    override fun accountCheck(
        accountNumber: String,
        participantCode: String
    ): Single<AccountCheckResponse> {
        return api.checkAccount(accountNumber, participantCode).map { it.data }
    }

    override fun deposit( depositPaymentData: DepositPaymentData): Single<DepositResponse> {

        return Single.just(depositPaymentData)
            .map { data: DepositPaymentData ->
                val payload = PaymentPayload()
                payload.time = System.currentTimeMillis()
                payload.assetId = ServerCurrencyToAssetIdMapper.mapCurrencyToAssetId(
                    data.money.currency
                )
                payload.description = "Vandoeurn Test"
                payload.receiverAccountId = data.bankId
                payload.senderAccountId = storage.getAccountId()
                payload.amount = data.money.amount
                payload
            }
            .flatMap(irohaRepository::signPayment)
            .map { signature -> createConfirmRequest(depositPaymentData, signature) }
            .flatMap(api::deposit)
            .map { it.data }
    }

    private fun createConfirmRequest(
        data: DepositPaymentData,
        signature: PaymentSignature
    ): Deposit? {
        val request = Deposit()
        request.receiverAccount = data.accountNumber
        request.amount = ServerAmountFormatter.formatAmount(data.money.amount)
        request.assetId = ServerCurrencyToAssetIdMapper.mapCurrencyToAssetId(data.money.currency)
        request.bankAccountId = data.bankId
        request.description = data.description
        request.signByte = signature.signedBytes
        request.trxHash = signature.transactionHash
        return request
    }
}