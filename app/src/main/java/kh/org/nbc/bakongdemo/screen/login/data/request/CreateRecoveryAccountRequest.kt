package kh.org.nbc.bakongdemo.screen.login.data.request

import com.google.gson.annotations.SerializedName

data class CreateRecoveryAccountRequest(
    @SerializedName("accountId")
    val accountId: String,
    @SerializedName("grantPermissionReducedHash")
    val grantPermissionReducedHash: String
)