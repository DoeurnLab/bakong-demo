package kh.org.nbc.bakongdemo.screen.login.data.response

import com.google.gson.annotations.SerializedName

data class RecoveryStatus(
    @SerializedName("createRecoveryAccount")
    val createRecoveryAccountStatus: String? = null,
    @SerializedName("grantPermissionToRecovery")
    val grantPermissionToRecovery: String? = null
) {
    fun isSuccess() = SUCCESS == createRecoveryAccountStatus && SUCCESS == grantPermissionToRecovery
    fun inProgress() = IN_PROGRESS == createRecoveryAccountStatus && IN_PROGRESS == grantPermissionToRecovery
}

private const val SUCCESS = "SUCCESS"
private const val IN_PROGRESS = "IN_PROGRESS"