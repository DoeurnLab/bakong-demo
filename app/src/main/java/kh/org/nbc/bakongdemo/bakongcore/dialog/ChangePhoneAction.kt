package kh.org.nbc.bakongdemo.bakongcore.dialog

import android.content.Intent
import androidx.fragment.app.DialogFragment
import kh.org.nbc.bakongdemo.bakongcore.alert.AlertAction
import kh.org.nbc.bakongdemo.screen.changephone.ChangePhoneActivity
import kotlinx.android.parcel.Parcelize

@Parcelize
class ChangePhoneAction : AlertAction() {
    override fun onAction(dialogFragment: DialogFragment) {
        val context = dialogFragment.context

        val intent = Intent(context, ChangePhoneActivity::class.java)
        intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK
        context!!.startActivity(intent)

    }
}