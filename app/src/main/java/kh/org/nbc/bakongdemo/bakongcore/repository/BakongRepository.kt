package kh.org.nbc.bakongdemo.bakongcore.repository

import io.reactivex.Single
import kh.org.nbc.bakongdemo.screen.qrrestore.utils.MessageSignature
import kh.org.nbc.bakongdemo.screen.registration.keygenerator.model.BakongKeyPair

interface BakongRepository {

    fun signMessage(message: String): Single<MessageSignature>
    fun encryptKey(passcode: String): Single<String>
    fun decryptKey(passcode: String, content: String): Single<BakongKeyPair>
}