package kh.org.nbc.bakongdemo.screen.deposit.data

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import kh.org.nbc.bakongdemo.bakongcore.rxschedulers.RxSchedulers
import kh.org.nbc.bakongdemo.base.BaseViewModel
import kh.org.nbc.bakongdemo.screen.deposit.DepositRepository
import javax.inject.Inject

class DepositViewModel @Inject constructor(
    private val rxSchedulers: RxSchedulers,
    private val repository: DepositRepository
) : BaseViewModel() {

    private val _depositResponse = MutableLiveData<DepositResponse>()
    val depositResponse: LiveData<DepositResponse> get() = _depositResponse


    fun checkAccountAndDeposit(accountNumber: String, participantCode: String, deposit: DepositPaymentData) {

        unsubscribeOnDestroy(
            repository.accountCheck(accountNumber, participantCode)
                .subscribeOn(rxSchedulers.io())
                .observeOn(rxSchedulers.main())
                .doOnSubscribe { _loading.postValue(true) }
                .subscribe({
                    deposit(deposit)
                    Log.d("CheckAccountResponse", "checkAccountAndDeposit: $it")
                }, { throwable ->
                    _loading.postValue(false)
                    showError(throwable)
                })
        )
    }

    fun deposit(deposit: DepositPaymentData) {

        unsubscribeOnDestroy(
            repository.deposit(deposit)
                .subscribeOn(rxSchedulers.io())
                .observeOn(rxSchedulers.main())
                .doFinally { _loading.postValue(false) }
                .subscribe({
                    _depositResponse.postValue(it)
                }, { throwable ->
                    showError(throwable)
                })
        )
    }


}