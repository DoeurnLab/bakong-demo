package kh.org.nbc.bakongdemo.screen.password.validate

interface TextValidator {
    fun validate(text: String): ValidationResult
}