package kh.org.nbc.bakongdemo.screen.phone.data;

import com.google.gson.annotations.SerializedName;

import java.util.Objects;

public class PhoneRegistrationRequest {

    @SerializedName("devID")
    private String mDeviceId;
    @SerializedName("versionOS")
    private String mVersionOs;
    @SerializedName("phone")
    private String mPhone;
    @SerializedName("publicKey")
    private String mPublicKey;

    public String getDeviceId() {
        return mDeviceId;
    }

    public void setDeviceId(String deviceId) {
        mDeviceId = deviceId;
    }

    public String getVersionOs() {
        return mVersionOs;
    }

    public void setVersionOs(String versionOs) {
        mVersionOs = versionOs;
    }

    public String getPhone() {
        return mPhone;
    }

    public void setPhone(String phone) {
        mPhone = phone;
    }

    public String getPublicKey() {
        return mPublicKey;
    }

    public void setPublicKey(String publicKey) {
        mPublicKey = publicKey;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PhoneRegistrationRequest that = (PhoneRegistrationRequest) o;
        return Objects.equals(mDeviceId, that.mDeviceId) &&
                Objects.equals(mVersionOs, that.mVersionOs) &&
                Objects.equals(mPhone, that.mPhone) &&
                Objects.equals(mPublicKey, that.mPublicKey);
    }

    @Override
    public int hashCode() {
        return Objects.hash(mDeviceId, mVersionOs, mPhone, mPublicKey);
    }

    @Override
    public String toString() {
        return "PhoneRegistrationRequest{" +
                "mDeviceId='" + mDeviceId + '\'' +
                ", mVersionOs='" + mVersionOs + '\'' +
                ", mPhone='" + mPhone + '\'' +
                ", mPublicKey='" + mPublicKey + '\'' +
                '}';
    }

}
