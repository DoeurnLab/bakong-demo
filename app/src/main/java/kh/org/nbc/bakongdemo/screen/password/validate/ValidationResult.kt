package kh.org.nbc.bakongdemo.screen.password.validate

enum class ValidationResult {
    LENGTH_LESS_ERROR,
    PATTERN_MATCH_ERROR,
    VALID
}