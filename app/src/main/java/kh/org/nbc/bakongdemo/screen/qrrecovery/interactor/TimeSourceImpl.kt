package kh.org.nbc.bakongdemo.screen.qrrecovery.interactor

import kh.org.nbc.bakongdemo.screen.qrrecovery.interactor.TimeSource
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class TimeSourceImpl @Inject constructor() :
    TimeSource {

    override fun currentTimeMillis(): Long {
        return System.currentTimeMillis()
    }
}
