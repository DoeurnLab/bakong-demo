package kh.org.nbc.bakongdemo.screen.phone.data;

import java.util.Objects;

public class RequestIdModel {

    private String mRequestId;

    public String getRequestId() {
        return mRequestId;
    }

    public void setRequestId(String requestId) {
        mRequestId = requestId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        RequestIdModel model = (RequestIdModel) o;
        return Objects.equals(mRequestId, model.mRequestId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(mRequestId);
    }

    @Override
    public String toString() {
        return "RequestIdModel{" +
                "mRequestId='" + mRequestId + '\'' +
                '}';
    }
}
