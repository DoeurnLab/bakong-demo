package kh.org.nbc.bakongdemo.screen.verification

import android.content.Intent
import android.os.Bundle
import android.os.CountDownTimer
import android.text.TextUtils
import android.view.View
import androidx.lifecycle.Observer
import kh.org.nbc.bakongdemo.R
import kh.org.nbc.bakongdemo.application.BakongApplication
import kh.org.nbc.bakongdemo.base.BaseActivity
import kh.org.nbc.bakongdemo.screen.username.UsernameActivity
import kh.org.nbc.bakongdemo.utils.hide
import kh.org.nbc.bakongdemo.utils.show
import kotlinx.android.synthetic.main.activity_verification.*
import kotlinx.android.synthetic.main.layout_toolbar.*
import javax.inject.Inject


open class VerificationActivity : BaseActivity() {

    @Inject
    lateinit var viewModel: VerificationViewModel
    private lateinit var mTimer: CountDownTimer
    private var requestId: String ? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        (application as BakongApplication).appComponent.inject(this)
        super.onCreate(savedInstanceState)

        setContentView( R.layout.activity_verification)

        edit_text_otp_number.requestFocus()

        viewModel.loginSuggestionName.observe(this, Observer {
            val intent = Intent(this, UsernameActivity::class.java)
            if (it.isNotEmpty()) {
                intent.putExtra(SUGGESTION_USERNAME, it[0])
            }
            startActivity(intent)
            finish()
        })

        viewModel.error.observe(this, Observer {
            onError(it)
        })

        viewModel.requestId.observe(this, Observer {
            this.requestId = it
        })

        text_view_title.text = "Verification"

        button_next.show()
        button_back.show()
        button_back.setOnClickListener {
            finish()
        }

        viewModel.loading.observe(this, Observer {
            if (it)
                progressBar.show()
            else progressBar.hide()
        })

        button_next.setOnClickListener {
            val code = edit_text_otp_number.text.toString()
            if (TextUtils.isEmpty(code)){
                edit_text_otp_number.error = resources.getString(R.string.code_is_required)
            }else {
                viewModel.verifyOtp(code)
            }
        }
        startTimer()
        button_request_code.setOnClickListener {
            viewModel.sendCode()
            startTimer()
        }
    }

    private fun startTimer() {
        enableButton(false)
        mTimer = object : CountDownTimer(TIMER, INTERVAL) {
            override fun onTick(millisUntilFinished: Long) {
                val timer = millisUntilFinished / 1000
                text_view_timer.text = "00:$timer"
            }

            override fun onFinish() {
                enableButton(true)
            }
        }
        mTimer.start()

    }

    private fun enableButton(enable: Boolean) {
        button_request_code.isEnabled = enable
        button_request_code.isClickable = enable
        if (enable) {
            view_timer.visibility = View.INVISIBLE
            button_request_code.alpha = 1f
        } else {
            button_request_code.alpha = 0.4f
            view_timer.visibility = View.VISIBLE
        }
    }

    companion object {
        const val SUGGESTION_USERNAME = "suggestion name"
        const val TIMER = 60000L
        const val INTERVAL = 1000L
    }

}