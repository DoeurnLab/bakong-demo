package kh.org.nbc.bakongdemo.di.module;


import dagger.Binds;
import dagger.Module;
import kh.org.nbc.bakongdemo.screen.send.SendRepository;
import kh.org.nbc.bakongdemo.screen.send.SendRepositoryImpl;

@Module
public abstract class SendModule {

    @Binds
    abstract SendRepository provideSendRepository(SendRepositoryImpl sendRepository);


}
