package kh.org.nbc.bakongdemo.bakongcore.error

const val SUCCESS = 0
const val USER_ERROR = 1
const val NO_RESPONSE_DATA = 2
const val UNATHORIZED = 3
const val ACCESS_DENIED = 4
const val NOT_AVAILABLE = 5
const val TOKEN_EXPIRED = 10
const val INTERNAL_SERVER_ERROR = 131
