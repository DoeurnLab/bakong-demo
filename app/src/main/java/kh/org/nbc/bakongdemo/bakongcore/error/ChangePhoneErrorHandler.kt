package kh.org.nbc.bakongdemo.bakongcore.error

import kh.org.nbc.bakongdemo.bakongcore.alert.Alert
import kh.org.nbc.bakongdemo.bakongcore.dialog.ChangePhoneAction
import kh.org.nbc.bakongdemo.bakongcore.exception.ChangePhoneException
import kh.org.nbc.bakongdemo.bakongcore.exception.WarningServerException
import kh.org.nbc.bakongdemo.utils.Status
import kh.org.nbc.bakongdemo.utils.getErrorMessage
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class ChangePhoneErrorHandler @Inject constructor() : ErrorHandler {

    companion object {
        const val CHANGE_PHONE_ERROR = 68
    }

    override fun canHandle(status: Status) = with(status) {
        return@with errorCode == CHANGE_PHONE_ERROR
    }

    override fun retrieveError(status: Status) = when (status.errorCode) {
        CHANGE_PHONE_ERROR -> {
            val alert = Alert(
                description = status.getErrorMessage(),
                okAction = ChangePhoneAction()
            )
            ChangePhoneException(
                status.code,
                status.errorCode,
                status.getErrorMessage(),
                alert
            )
        }
        else -> WarningServerException(
            status.code,
            status.errorCode,
            status.getErrorMessage()
        )
    }
}