package kh.org.nbc.bakongdemo.screen.qrrecovery.repository

import io.reactivex.Single

interface QrRecoveryRepository {

    fun createRecoveryQr(): Single<String>
    fun saveQrToDownload(qrPath: String): Single<String>
    fun restoreAccount(passcode: String, encryptedString: String): Single<Boolean>
}