package kh.org.nbc.bakongdemo.network.data.mapper

import java.util.*

abstract class Mapper<T, R> : OneWayMapper<T, R>() {

    abstract fun reverse(item: R): T

    fun reverseList(list: List<R>): List<T> {
        val convertedList = ArrayList<T>(list.size)
        for (item in list) {
            convertedList.add(reverse(item))
        }
        return convertedList
    }
}
