package kh.org.nbc.bakongdemo.screen.qrrecovery.repository

import android.content.Context
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.net.Uri
import com.google.zxing.*
import com.google.zxing.common.HybridBinarizer
import com.journeyapps.barcodescanner.BarcodeEncoder
import com.orhanobut.logger.Logger
import kh.org.nbc.bakongdemo.screen.qrrecovery.interactor.TimeSource
import kh.org.nbc.bakongdemo.screen.qrrecovery.exception.QrCreationException
import kh.org.nbc.bakongdemo.screen.qrrecovery.utils.decodeBitmapUri
import java.io.File
import java.io.FileOutputStream
import javax.inject.Inject

class QrRepositoryImpl @Inject constructor(
    private val context: Context, private val timeSource: TimeSource
) : QrRepository {

    companion object {
        private const val WIDTH = 500
        private const val HEIGHT = 500
        private const val QR_PATH = "qr-"
    }

    override fun encode(text: String): String {
        var bitmap: Bitmap? = null
        try {
            val multiFormatWriter = MultiFormatWriter()
            val bitMatrix = multiFormatWriter.encode(text, BarcodeFormat.QR_CODE,
                WIDTH,
                HEIGHT
            )
            val barcodeEncoder = BarcodeEncoder()
            bitmap = barcodeEncoder.createBitmap(bitMatrix)
        } catch (e: WriterException) {
            e.printStackTrace()
        }
        return save(bitmap)
    }

    private fun save(bitmap: Bitmap?): String {
        if (bitmap == null) throw QrCreationException()

        val file = File(context.cacheDir, QR_PATH + timeSource.currentTimeMillis() + ".jpg")
        file.createNewFile()

        val fileOutputStream = FileOutputStream(file)

        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, fileOutputStream)
        return file.absolutePath
    }

    override fun decode(path: String): String {
        val bitmap = BitmapFactory.decodeFile(path)
        return decodeBitmap(bitmap)
    }

    override fun decode(uri: Uri): String? {
        return runCatching {
            context.decodeBitmapUri(uri,
                WIDTH,
                HEIGHT
            )?.let { decodeBitmap(it) }
        }.onFailure { exception ->
            Logger.e(exception, exception.message ?: "Decoding error")
        }.getOrNull()
    }

    private fun decodeBitmap(bitmap: Bitmap): String {
        val height = bitmap.height
        val width = bitmap.width
        val pixels = IntArray(height * width)
        bitmap.getPixels(pixels, 0, width, 0, 0, width, height)
        bitmap.recycle()

        val binaryBitmap = BinaryBitmap(
            HybridBinarizer(
                RGBLuminanceSource(width, height, pixels)
            )
        )
        val result = tryDecode(binaryBitmap)
        return result.text
    }

    private fun tryDecode(binaryBitmap: BinaryBitmap): Result {
        val multiFormatReader = MultiFormatReader()
        return runCatching {
            multiFormatReader.decode(binaryBitmap)
        }.getOrElse { throwable ->
            Logger.e(throwable, throwable.message.orEmpty())
            val hints = mapOf(
                DecodeHintType.PURE_BARCODE to true,
                DecodeHintType.TRY_HARDER to true,
                DecodeHintType.POSSIBLE_FORMATS to listOf(BarcodeFormat.QR_CODE)
            )
            return multiFormatReader.decode(binaryBitmap, hints)
        }
    }
}