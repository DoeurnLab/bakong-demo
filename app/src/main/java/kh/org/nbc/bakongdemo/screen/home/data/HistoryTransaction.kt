package kh.org.nbc.bakongdemo.screen.home.data

import com.google.gson.annotations.SerializedName
import java.math.BigDecimal

data class HistoryTransaction(
    @SerializedName("fiTransactionId")
    val transactionId: Long = 0,
    @SerializedName("sourceAccountId")
    val sourceAccountId: String? = null,
    @SerializedName("destinationAccountId")
    val destinationAccountId: String? = null,
    @SerializedName("sourcePhoneNumber")
    val sourcePhoneNumber: String? = null,
    @SerializedName("destinationPhoneNumber")
    val destinationPhoneNumber: String? = null,
    @SerializedName("sourceFullName")
    val sourceFullName: String? = null,
    @SerializedName("destinationFullName")
    val destinationFullName: String? = null,
    @SerializedName("sourceName")
    val sourceName: String? = null,
    @SerializedName("destinationName")
    val destinationName: String? = null,
    @SerializedName("sourceBankParticipantCode")
    val sourceBankCode: String? = null,
    @SerializedName("sourceBankName")
    val sourceBankName: String? = null,
    @SerializedName("destinationBankParticipantCode")
    val destinationBankCode: String? = null,
    @SerializedName("destinationBankName")
    val destinationBankName: String? = null,
    @SerializedName("amount")
    val amount: BigDecimal,
    @SerializedName("currencyName")
    val currencyName: String? = null,
    @SerializedName("currencySymbol")
    val currencySymbol: String? = null,
    @SerializedName("transactionCreatedTime")
    val transactionCreatedTime: Double = 0.0,
    @SerializedName("sourceWalletType")
    val sourceWalletType: String? = null,
    @SerializedName("destinationWalletType")
    val destinationWalletType: String? = null,
    @SerializedName("precision")
    val precision: Int = 0,
    @SerializedName("status")
    val status: String,
    @SerializedName("transactionHash")
    val transactionHash: String? = null,
    @SerializedName("assetId")
    val assetId: String,
    @SerializedName("description")
    val description: String? = null,
    @SerializedName("errorMessage")
    val errorMessage: String? = null,
    @SerializedName("fastTransactionId")
    val fastTransactionId: String? = null,
    @SerializedName("transactionType")
    val transactionType: String,
    @SerializedName("sourceOrganizationName")
    val sourceOrganizationName: String? = null,
    @SerializedName("destinationOrganizationName")
    val destinationOrganizationName: String? = null,
    @SerializedName("receiverBankAccount")
    val receiverBankAccount: String? = null
)