package kh.org.nbc.bakongdemo.screen.send

import io.reactivex.Single
import kh.org.nbc.bakongdemo.screen.send.data.InitializePaymentPhone
import kh.org.nbc.bakongdemo.screen.send.data.PaymentData
import kh.org.nbc.bakongdemo.screen.send.data.SendPaymentResponse

interface SendRepository {

    fun initializePaymentPhone(phoneNumber: String): Single<InitializePaymentPhone>
    fun sendPayment(paymentData: PaymentData): Single<SendPaymentResponse>

}