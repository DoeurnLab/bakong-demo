package kh.org.nbc.bakongdemo.screen.login.data.response

import com.google.gson.annotations.SerializedName

data class Login(
    @SerializedName("accountId")
    val accountId: String,
    @SerializedName("firstName")
    val firstName: String,
    @SerializedName("lastName")
    val lastName: String,
    @SerializedName("expiresInSeconds")
    val expiresInSeconds: Long? = null,
    @SerializedName("token")
    var token: String? = null
)