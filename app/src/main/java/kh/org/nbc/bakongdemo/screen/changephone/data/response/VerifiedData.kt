package jp.co.soramitsu.feature_change_phone_impl.data.response

import com.google.gson.annotations.SerializedName

data class VerifiedData(
    @SerializedName("verified")
    val verified: Boolean
)