package kh.org.nbc.bakongdemo.screen.login.data.mapper

import kh.org.nbc.bakongdemo.screen.login.data.response.Login
import kh.org.nbc.bakongdemo.network.data.mapper.BaseServerMapper
import kh.org.nbc.bakongdemo.screen.login.data.LoginModel
import javax.inject.Inject

open class LoginToLoginModelMapper @Inject constructor() :
        BaseServerMapper<Login, LoginModel>() {

    override fun map(item: Login) = with(item) {
        LoginModel(accountId, firstName, lastName, expiresInSeconds, token)
    }
}
