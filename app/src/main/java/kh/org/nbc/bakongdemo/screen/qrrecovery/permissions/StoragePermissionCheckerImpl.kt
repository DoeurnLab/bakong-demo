package kh.org.nbc.bakongdemo.screen.qrrecovery.permissions

import android.Manifest.permission.WRITE_EXTERNAL_STORAGE
import android.content.Context
import android.content.pm.PackageManager.PERMISSION_GRANTED
import androidx.core.content.ContextCompat.checkSelfPermission
import javax.inject.Inject

class StoragePermissionCheckerImpl @Inject constructor(
    private val context: Context
) : StoragePermissionChecker {
    override fun hasPermission() = checkSelfPermission(context, WRITE_EXTERNAL_STORAGE) == PERMISSION_GRANTED
}