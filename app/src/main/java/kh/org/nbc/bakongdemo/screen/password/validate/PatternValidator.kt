package kh.org.nbc.bakongdemo.screen.password.validate

import java.util.regex.Pattern

class PatternValidator constructor(regex: String) :
    TextValidator {

    private val pattern: Pattern = Pattern.compile(regex)

    override fun validate(text: String) = if (pattern.matcher(text).matches()) ValidationResult.VALID else ValidationResult.PATTERN_MATCH_ERROR
}