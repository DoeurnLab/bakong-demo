package kh.org.nbc.bakongdemo.screen.registration.keygenerator

import io.reactivex.Single
import kh.org.nbc.bakongdemo.screen.registration.keygenerator.model.BakongKeyPair

interface KeysCreator {

    fun create(seed: ByteArray): Single<BakongKeyPair>
}
