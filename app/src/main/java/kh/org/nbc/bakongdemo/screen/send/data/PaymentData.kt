package kh.org.nbc.bakongdemo.screen.send.data

import android.os.Parcelable
import kh.org.nbc.bakongdemo.utils.Currency
import kh.org.nbc.bakongdemo.utils.Money
import kotlinx.android.parcel.Parcelize
import java.math.BigDecimal

@Parcelize
data class PaymentData @JvmOverloads constructor(
    var money: Money = Money(BigDecimal.ZERO, Currency.USD),
    var description: String? = "",
    var attachment: String? = "",
    var receiverId: String? = null,
    var time: Long = 0,
    var qrCodeData: String? = null
) : Parcelable