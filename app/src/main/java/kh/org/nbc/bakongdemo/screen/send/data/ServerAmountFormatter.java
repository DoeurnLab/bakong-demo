package kh.org.nbc.bakongdemo.screen.send.data;

import java.math.BigDecimal;
import java.util.Locale;

public class ServerAmountFormatter {
    private static final String SERVER_FORMAT = "%.2f";

    public static String formatAmount(BigDecimal amount) {
        return String.format(Locale.ENGLISH, SERVER_FORMAT, amount);
    }

}
