package kh.org.nbc.bakongdemo.screen.qrrecovery.utils

import android.content.Context
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.net.Uri
import com.orhanobut.logger.Logger
import java.io.InputStream

object ImageDecoder {

    fun decodeBitmapUri(context: Context, uri: Uri?, maxSize: Int): Bitmap? {
        return decodeBitmapUri(
            context,
            uri,
            maxSize,
            maxSize
        )
    }

    fun decodeBitmapUri(context: Context, uri: Uri?, reqWidth: Int, reqHeight: Int): Bitmap? {
        var stream = uri?.let {
            openStream(
                context,
                uri
            )
        } ?: return null

        // First decode with inJustDecodeBounds=true to check dimensions
        val options = BitmapFactory.Options()
        options.inJustDecodeBounds = true
        BitmapFactory.decodeStream(stream, null, options)

        // Calculate inSampleSize
        options.inSampleSize =
            calculateInSampleSize(
                options,
                reqWidth,
                reqHeight
            )

        // Decode bitmap with inSampleSize set
        options.inJustDecodeBounds = false

        stream = openStream(
            context,
            uri
        ) ?: return null

        return BitmapFactory.decodeStream(stream, null, options)
    }

    private fun openStream(context: Context, uri: Uri): InputStream? {
        return runCatching {
            context.contentResolver.openInputStream(uri)
        }.onFailure { throwable ->
            Logger.e(throwable, throwable.localizedMessage)
        }.getOrNull()
    }

    fun decodeBitmapByteArray(byteArray: ByteArray?, maxSize: Int): Bitmap? {
        return decodeBitmapByteArray(
            byteArray,
            maxSize,
            maxSize
        )
    }

    fun decodeBitmapByteArray(byteArray: ByteArray?, reqWidth: Int, reqHeight: Int): Bitmap? {
        if (byteArray == null) return null

        // First decode with inJustDecodeBounds=true to check dimensions
        val options = BitmapFactory.Options()
        options.inJustDecodeBounds = true
        BitmapFactory.decodeByteArray(byteArray, 0, byteArray.size, options)

        // Calculate inSampleSize
        options.inSampleSize =
            calculateInSampleSize(
                options,
                reqWidth,
                reqHeight
            )

        // Decode bitmap with inSampleSize set
        options.inJustDecodeBounds = false
        return BitmapFactory.decodeByteArray(byteArray, 0, byteArray.size, options)
    }

    private fun calculateInSampleSize(options: BitmapFactory.Options, reqWidth: Int, reqHeight: Int): Int {
        // Raw height and width of image
        val height = options.outHeight
        val width = options.outWidth
        var inSampleSize = 1

        if (height > reqHeight || width > reqWidth) {

            val halfHeight = height / 2
            val halfWidth = width / 2

            // Calculate the largest inSampleSize value that is a power of 2 and keeps both
            // height and width larger than the requested height and width.
            while (halfHeight / inSampleSize >= reqHeight && halfWidth / inSampleSize >= reqWidth) {
                inSampleSize *= 2
            }
        }

        return inSampleSize
    }
}

fun Context.decodeBitmapUri(uri: Uri?, reqWidth: Int, reqHeight: Int): Bitmap? =
    ImageDecoder.decodeBitmapUri(
        this,
        uri,
        reqWidth,
        reqHeight
    )

fun Context.decodeBitmapUri(uri: Uri?, maxSize: Int): Bitmap? =
    ImageDecoder.decodeBitmapUri(
        this,
        uri,
        maxSize
    )