package kh.org.nbc.bakongdemo.screen.registration.data

data class RegistrationModel(
    val isSuccessRegistration: Boolean = false,
    val loginSuggestions: List<String>,
    val bankDomain: String
)
