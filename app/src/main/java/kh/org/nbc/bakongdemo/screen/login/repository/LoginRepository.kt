package kh.org.nbc.bakongdemo.screen.login.repository

import io.reactivex.Completable
import io.reactivex.Single
import kh.org.nbc.bakongdemo.screen.login.data.LoginModel

interface LoginRepository {
    fun login(publicKey: String, deviceId: String): Single<LoginModel>
    fun status(): Single<Boolean>
    fun createRecovery(accountId: String): Completable
}
