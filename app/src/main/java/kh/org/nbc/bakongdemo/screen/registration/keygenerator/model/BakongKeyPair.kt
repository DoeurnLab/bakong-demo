package kh.org.nbc.bakongdemo.screen.registration.keygenerator.model

interface BakongKeyPair {

    fun getPublicKey(): String

    fun getPrivateKey(): String
}
