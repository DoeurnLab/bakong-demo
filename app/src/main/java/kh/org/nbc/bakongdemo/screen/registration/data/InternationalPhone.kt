package kh.org.nbc.bakongdemo.screen.registration.data

data class InternationalPhone(
    val countryCode: Int,
    val nationalNumber: Long
) {
    fun getE164() = "+$countryCode$nationalNumber"
    fun getPhoneNumber() = "$countryCode$nationalNumber"
    fun getNationalPhone() = "$nationalNumber"
    fun getCountryCode() = "$countryCode"
}