package kh.org.nbc.bakongdemo.screen.password.validate


class LessLengthValidator(private val length: Int) :
    TextValidator {

    override fun validate(text: String) = if (text.length >= length) ValidationResult.VALID else ValidationResult.LENGTH_LESS_ERROR
}