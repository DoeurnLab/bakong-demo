package kh.org.nbc.bakongdemo.di.module

import kh.org.nbc.bakongdemo.storage.SharedPreferencesStorage
import dagger.Binds
import dagger.Module
import kh.org.nbc.bakongdemo.storage.Storage

@Module
abstract class StorageModule {

    @Binds
    abstract fun provideStorage(storage: SharedPreferencesStorage): Storage

}