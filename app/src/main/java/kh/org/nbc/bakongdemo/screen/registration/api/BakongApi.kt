package kh.org.nbc.bakongdemo.screen.registration.api

import io.reactivex.Single
import kh.org.nbc.bakongdemo.screen.changephone.data.request.ChangePhoneRequest
import jp.co.soramitsu.feature_change_phone_impl.data.request.VerifyCodeRequest
import jp.co.soramitsu.feature_change_phone_impl.data.response.VerifiedData
import kh.org.nbc.bakongdemo.base.BaseResponse
import kh.org.nbc.bakongdemo.screen.changephone.data.response.ChangePhoneData
import kh.org.nbc.bakongdemo.screen.deposit.data.*
import kh.org.nbc.bakongdemo.screen.home.data.AccountAsset
import kh.org.nbc.bakongdemo.screen.home.data.TransactionData
import kh.org.nbc.bakongdemo.screen.home.data.TransactionRequest
import kh.org.nbc.bakongdemo.screen.login.data.request.CreateRecoveryAccountRequest
import kh.org.nbc.bakongdemo.screen.login.data.request.GrantRecoveryAccountRequest
import kh.org.nbc.bakongdemo.screen.login.data.request.LoginRequest
import kh.org.nbc.bakongdemo.screen.login.data.response.CreateRecoveryData
import kh.org.nbc.bakongdemo.screen.login.data.response.GrantRecoveryData
import kh.org.nbc.bakongdemo.screen.login.data.response.Login
import kh.org.nbc.bakongdemo.screen.login.data.response.RecoveryStatus
import kh.org.nbc.bakongdemo.screen.phone.data.PhoneRegistration
import kh.org.nbc.bakongdemo.screen.phone.data.PhoneRegistrationRequest
import kh.org.nbc.bakongdemo.screen.qrrestore.data.RestoreAccountRequest
import kh.org.nbc.bakongdemo.screen.qrrestore.utils.RestoreStatus
import kh.org.nbc.bakongdemo.screen.registration.data.AccountCheck
import kh.org.nbc.bakongdemo.screen.registration.data.Registration
import kh.org.nbc.bakongdemo.screen.registration.data.RegistrationRequest
import kh.org.nbc.bakongdemo.screen.registration.data.RestoreMessage
import kh.org.nbc.bakongdemo.screen.send.data.InitializePaymentPhone
import kh.org.nbc.bakongdemo.screen.send.data.InitializePaymentPhoneReq
import kh.org.nbc.bakongdemo.screen.send.data.SendPayment
import kh.org.nbc.bakongdemo.screen.send.data.SendPaymentResponse
import kh.org.nbc.bakongdemo.screen.username.data.AccountRegistration
import kh.org.nbc.bakongdemo.screen.username.data.AccountRequest
import kh.org.nbc.bakongdemo.screen.verification.data.PhoneVerification
import kh.org.nbc.bakongdemo.screen.verification.data.PhoneVerificationRequest
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.Query

interface BakongApi {

    @POST("/api/login")
    fun login(@Body request: LoginRequest): Single<BaseResponse<Login>>

    @POST("/paymentservice/api/bridge/v1/signup/phoneRegistration")
    fun phoneRegistration(@Body request: PhoneRegistrationRequest): Single<BaseResponse<PhoneRegistration>>

    @POST("/paymentservice/api/bridge/v1/signup/phoneVerification")
    fun phoneVerification(@Body request: PhoneVerificationRequest): Single<BaseResponse<PhoneVerification>>

    @POST("/paymentservice/api/bridge/v1/signup/registration")
    fun registration(@Body request: RegistrationRequest): Single<BaseResponse<Registration>>

    @POST("/paymentservice/api/bridge/v1/signup/accountRegistration")
    fun accountRegistration(@Body request: AccountRequest): Single<BaseResponse<AccountRegistration>>

    @POST("/paymentservice/api/bridge/v1/signup/recovery")
    fun restoreAccount(@Body request: RestoreAccountRequest): Single<BaseResponse<RestoreStatus>>

    @GET("/paymentservice/api/bridge/v1/signup/message")
    fun fetchRestoreMessage(): Single<BaseResponse<RestoreMessage>>

    @POST("/paymentservice/api/bridge/v1/signup/accountCheck")
    fun accountCheck(@Body request: AccountRequest): Single<BaseResponse<AccountCheck>>

    @POST("paymentservice/api/bridge/v1/signup/changePhone")
    fun changePhone(@Body changePhoneRequest: ChangePhoneRequest): Single<BaseResponse<ChangePhoneData>>

    @POST("paymentservice/api/bridge/v1/signup/changePhone/verify")
    fun verifyChangePhone(@Body verifyCodeRequest: VerifyCodeRequest): Single<BaseResponse<VerifiedData>>

    @POST("/paymentservice/api/v1/user/create-recovery-account")
    fun createRecoveryAccount(@Body request: CreateRecoveryAccountRequest): Single<BaseResponse<CreateRecoveryData>>

    @POST("/paymentservice/api/v1/user/grant-recovery-account")
    fun grantRecoveryAccount(@Body request: GrantRecoveryAccountRequest): Single<BaseResponse<GrantRecoveryData>>

    @GET("/paymentservice/api/v1/user/recovery-status")
    fun status(): Single<BaseResponse<RecoveryStatus>>

    @GET("/paymentservice/api/v1/payment/balance")
    fun getBalance(): Single<BaseResponse<AccountAsset>>

    @POST("/paymentservice/api/v1/payment/initializePaymentPhone")
    fun initializePaymentPhone(@Body initializePaymentPhoneReq: InitializePaymentPhoneReq): Single<BaseResponse<InitializePaymentPhone>>

    @POST("/paymentservice/api/v1/payment/confirmPayment")
    fun sendPayment(@Body sendPayment: SendPayment): Single<BaseResponse<SendPaymentResponse>>

    @GET("/paymentservice/api/v2/banks/deposit-list")
    fun getBankList(): Single<BaseResponse<List<BankListResponse>>>

    @GET("/paymentservice/api/v1/payment/deposit/account-info")
    fun checkAccount(@Query("accountNumber") accountNumber: String, @Query("participantCode")  participantCode: String): Single<BaseResponse<AccountCheckResponse>>

    @POST("/paymentservice/api/v1/payment/deposit")
    fun deposit(@Body deposit: Deposit): Single<BaseResponse<DepositResponse>>

    @POST("/paymentservice/api/v1/payment/transactionHistory")
    fun transactions(@Body request: TransactionRequest): Single<BaseResponse<TransactionData>>

}