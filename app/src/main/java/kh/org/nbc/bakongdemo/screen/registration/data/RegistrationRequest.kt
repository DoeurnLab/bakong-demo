package kh.org.nbc.bakongdemo.screen.registration.data

import com.google.gson.annotations.SerializedName

class RegistrationRequest(
    @SerializedName("devID")
    val deviceId: String?,
    @SerializedName("publicKey")
    val publicKey: String?,
    @SerializedName("identity")
    val identity: String?,
    @SerializedName("photo")
    val photo: String?,
    @SerializedName("firstName")
    val firstName: String?,
    @SerializedName("lastName")
    val lastName: String?,
    @SerializedName("sex")
    val sex: String?,
    @SerializedName("birthDate")
    val birthDate: String?,
    @SerializedName("email")
    val email: String?,
    @SerializedName("address")
    val address: String?,
    @SerializedName("nationality")
    val nationality: String?,
    @SerializedName("documentId")
    val documentId: String?
)