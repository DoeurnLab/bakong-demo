package kh.org.nbc.bakongdemo.bakongcore.error


import android.content.Context
import com.google.gson.Gson
import kh.org.nbc.bakongdemo.R
import kh.org.nbc.bakongdemo.bakongcore.exception.ErrorServerException
import kh.org.nbc.bakongdemo.bakongcore.exception.ServerException
import kh.org.nbc.bakongdemo.bakongcore.exception.WarningServerException
import kh.org.nbc.bakongdemo.base.BaseResponse
import kh.org.nbc.bakongdemo.utils.Status
import kh.org.nbc.bakongdemo.utils.getErrorMessage
import javax.inject.Inject
import javax.inject.Singleton

/**
 * Check response status at the network layer
 * <p>
 *     If response status == [0] -> request finish success
 *     If response status == [1] -> request fail by user input (account already exists, wrong sms code etc)
 *     If response status == [2, 3, 4, 5] -> request fail on server
 */
@Singleton
class ErrorCheckerImpl @Inject constructor(
    private val gson: Gson,
    private val context: Context,
    private val errorHandlers: Set<@JvmSuppressWildcards ErrorHandler>
) : ErrorChecker {


    override fun throwExceptionIfError(jsonString: String?) {
        val baseResponse = parseBaseResponse(jsonString)
        if (baseResponse != null) {
            val exception = extractExceptionIfError(baseResponse)
            if (exception != null) {
                throw exception
            }
        }
    }

    private fun parseBaseResponse(jsonString: String?): BaseResponse<*>? {
        return gson.fromJson(jsonString, BaseResponse::class.java)
    }

    private fun extractExceptionIfError(baseResponse: BaseResponse<*>): ServerException? {
        val errorServerException: ServerException?
        val status = baseResponse.status

        if (status.errorCode == INTERNAL_SERVER_ERROR) {
            errorServerException =
                ErrorServerException(
                    status.code,
                    status.errorCode,
                    status.getErrorMessage()
                )
        } else {
            errorServerException = when (status.code) {

                USER_ERROR, ACCESS_DENIED -> createHandledError(status)

                NOT_AVAILABLE -> ErrorServerException(
                    status.code,
                    status.errorCode,
                    context.getString(R.string.not_available)
                )

                SUCCESS -> null

                else -> ErrorServerException(
                    status.code,
                    status.errorCode,
                    context.getString(R.string.something_went_wrong)
                )
            }
        }

        return errorServerException
    }

    private fun createHandledError(status: Status): ServerException {
        val errorHandler = errorHandlers.firstOrNull { handler -> handler.canHandle(status) }
        return errorHandler?.retrieveError(status) ?: WarningServerException(
            status.code,
            status.errorCode,
            status.getErrorMessage()
        )
    }
}