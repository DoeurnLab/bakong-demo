package kh.org.nbc.bakongdemo.screen.registration.data

data class LoginStatusModel(
    val isLoginFree: Boolean = false
)

