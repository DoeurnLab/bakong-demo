package kh.org.nbc.bakongdemo.di.module;


import androidx.annotation.NonNull;

import com.google.gson.Gson;

import java.util.concurrent.TimeUnit;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import kh.org.nbc.bakongdemo.bakongcore.error.ErrorHandlerInterceptor;
import kh.org.nbc.bakongdemo.bakongcore.token.ServerAuthenticator;
import kh.org.nbc.bakongdemo.bakongcore.token.TokenInterceptor;
import kh.org.nbc.bakongdemo.network.data.NetworkApiCreator;
import kh.org.nbc.bakongdemo.network.data.NetworkApiCreatorImpl;
import kh.org.nbc.bakongdemo.network.converter.SessionConverterFactory;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;

@Module
public abstract class NetworkModule {

    @Provides
    static OkHttpClient provideOkHttpClient(@NonNull TokenInterceptor tokenInterceptor,
                                            @NonNull ErrorHandlerInterceptor errorHandlerInterceptor,
                                            @NonNull ServerAuthenticator serverAuthenticator) {
        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.level(HttpLoggingInterceptor.Level.BODY);

        return new OkHttpClient.Builder()
                .connectTimeout(60, TimeUnit.SECONDS)
                .writeTimeout(60, TimeUnit.SECONDS)
                .readTimeout(60, TimeUnit.SECONDS)
                .addNetworkInterceptor(tokenInterceptor)
                .authenticator(serverAuthenticator)
                .retryOnConnectionFailure(true)
                .addInterceptor(errorHandlerInterceptor)
                .addInterceptor(logging)
                .build();
    }

    @Provides
    static NetworkApiCreator provideApiCreator(@NonNull OkHttpClient okHttpClient, @NonNull SessionConverterFactory factory) {

        String baseUrl = "http://3.1.42.245:8090";

        Retrofit retrofit = new Retrofit.Builder()
                .client(okHttpClient)
                .baseUrl(baseUrl)
                .addConverterFactory(factory)
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build();

        return new NetworkApiCreatorImpl(retrofit);
    }

    @Provides
    @Singleton
    static Gson provideGson() {
        return new Gson();
    }



}
