package kh.org.nbc.bakongdemo.screen.login.repository

import io.reactivex.Completable
import io.reactivex.Single
import kh.org.nbc.bakongdemo.screen.login.interactor.RecoveryInteractor
import kh.org.nbc.bakongdemo.screen.login.data.mapper.LoginToLoginModelMapper
import kh.org.nbc.bakongdemo.screen.login.data.request.LoginRequest
import kh.org.nbc.bakongdemo.screen.login.data.LoginModel
import kh.org.nbc.bakongdemo.screen.registration.api.BakongApi
import javax.inject.Inject

class LoginRepositoryImpl @Inject constructor(
    private val api: BakongApi,
    private val recoveryInteractor: RecoveryInteractor,
    private val loginModelMapper: LoginToLoginModelMapper
) : LoginRepository {

    override fun login(publicKey: String, deviceId: String): Single<LoginModel> {
        val request = LoginRequest(deviceId, publicKey)
        return api.login(request)
            .map { loginModelMapper.mapResponse(it) }
    }

    override fun status(): Single<Boolean> {
        return recoveryInteractor.status()

    }

    override fun createRecovery(accountId: String): Completable {
        return recoveryInteractor.createRecovery(accountId)
    }


}




