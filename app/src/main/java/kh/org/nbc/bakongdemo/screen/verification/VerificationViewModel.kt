package kh.org.nbc.bakongdemo.screen.verification

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import kh.org.nbc.bakongdemo.base.BaseViewModel
import kh.org.nbc.bakongdemo.bakongcore.rxschedulers.RxSchedulers
import kh.org.nbc.bakongdemo.screen.registration.repository.RegistrationRepository
import kh.org.nbc.bakongdemo.storage.Storage
import javax.inject.Inject

class VerificationViewModel @Inject constructor(
    private val rxSchedulers: RxSchedulers,
    private val storage: Storage,
    private val registrationRepository: RegistrationRepository
) : BaseViewModel() {

    private val _loginSuggestionName = MutableLiveData<List<String>>()
    val loginSuggestionName: LiveData<List<String>> get() = _loginSuggestionName

    private val _requestId = MutableLiveData<String>()
    val requestId: LiveData<String> get() = _requestId

    fun sendCode() {
        unsubscribeOnDestroy(registrationRepository.registerPhone(storage.getPhoneNumber()!!)
            .subscribeOn(rxSchedulers.io())
            .observeOn(rxSchedulers.main())
            .doOnSubscribe {
                _loading.postValue(true)
            }
            .doFinally {
                _loading.postValue(false)
            }
            .subscribe({
                _requestId.postValue(it.requestId)
            }, {
                showError(it)
            })
        )
    }

    fun verifyOtp(code: String) {
        storage.setToken("")
        unsubscribeOnDestroy(registrationRepository.verifyPhone(
            code,
            storage.getPublicKey()!!
        )
            .subscribeOn(rxSchedulers.io())
            .observeOn(rxSchedulers.main())
            .doOnSubscribe { _loading.postValue(true) }
            .subscribe({
                registerUserInfo()
            }, {
                showError(it)
                _loading.postValue(false)
            })
        )
    }

    private fun registerUserInfo() {
        unsubscribeOnDestroy(registrationRepository.registerUserInfo()
            .subscribeOn(rxSchedulers.io())
            .observeOn(rxSchedulers.main())
            .doFinally { _loading.postValue(false) }
            .subscribe({
                if (it.isSuccessRegistration) {
                    _loginSuggestionName.postValue(it.loginSuggestions)
                }
            }, {
                showError(it)
            })
        )
    }
}