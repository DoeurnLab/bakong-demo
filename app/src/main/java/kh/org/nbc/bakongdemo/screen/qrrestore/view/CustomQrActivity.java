package kh.org.nbc.bakongdemo.screen.qrrestore.view;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.KeyEvent;
import android.widget.ImageView;

import androidx.annotation.Nullable;

import com.google.zxing.client.android.Intents;
import com.journeyapps.barcodescanner.CaptureManager;
import com.journeyapps.barcodescanner.DecoratedBarcodeView;

import javax.inject.Inject;

import kh.org.nbc.bakongdemo.R;
import kh.org.nbc.bakongdemo.application.BakongApplication;
import kh.org.nbc.bakongdemo.base.BaseActivity;
import kh.org.nbc.bakongdemo.screen.qrrestore.interactor.QrInteractor;
import kh.org.nbc.bakongdemo.utils.ActivityUtilsKt;

import static kh.org.nbc.bakongdemo.screen.qrrestore.view.QrRestoreActivity.SCAN_ERROR;

public class CustomQrActivity extends BaseActivity {

    private static final int SELECT_IMAGE_REQUEST_CODE = 1001;

    private CaptureManager mCaptureManager;
    private DecoratedBarcodeView mDecoratedBarcodeView;
    private ImageView mQrScanCloseImageView;
    private ImageView mQrOpenFromFileImageView;


    @Inject
    QrInteractor mQrInteractor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        ((BakongApplication) getApplication()).getAppComponent().inject(this);

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_custom_qr);
        mDecoratedBarcodeView = findViewById(R.id.zxing_barcode_scanner);
        mQrScanCloseImageView = findViewById(R.id.qrScanCloseImageView);
        mQrOpenFromFileImageView = findViewById(R.id.qrOpenFromFileImageView);

        mQrScanCloseImageView.setOnClickListener(v -> finish());

        mQrOpenFromFileImageView.setOnClickListener(v -> {
            Intent intent = new Intent();
            intent.setType("image/*");
            intent.setAction(Intent.ACTION_GET_CONTENT);
            startActivityForResult(intent, SELECT_IMAGE_REQUEST_CODE);
        });

        mCaptureManager = new CaptureManager(this, mDecoratedBarcodeView);
        mCaptureManager.initializeFromIntent(getIntent(), savedInstanceState);
        mCaptureManager.decode();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if (requestCode == SELECT_IMAGE_REQUEST_CODE) {
            if (resultCode == RESULT_OK && data != null && data.getData() != null) {
                tryDecode(data.getData());
            }
        } else {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        mCaptureManager.onResume();
        ActivityUtilsKt.setupStatusBarDarkTheme(this, mDecoratedBarcodeView);
    }

    @Override
    protected void onPause() {
        mCaptureManager.onPause();
        super.onPause();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mCaptureManager.onDestroy();
        ActivityUtilsKt.setupStatusBarWhiteTheme(this, mDecoratedBarcodeView);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        mCaptureManager.onSaveInstanceState(outState);
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        return mDecoratedBarcodeView.onKeyDown(keyCode, event) || super.onKeyDown(keyCode, event);
    }


    private void tryDecode(Uri uri) {
        String content = mQrInteractor.decode(uri);
        if (content != null && !content.isEmpty()) {
            Intent intent = new Intent(Intents.Scan.ACTION);
            intent.putExtra(Intents.Scan.RESULT, content);
            setResult(RESULT_OK, intent);
        } else {
            Intent intent = new Intent();
            intent.putExtra(SCAN_ERROR, true);
            setResult(RESULT_CANCELED, intent);
        }
        finish();
    }

}
