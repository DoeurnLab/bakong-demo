package kh.org.nbc.bakongdemo.screen.qrrecovery

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import kh.org.nbc.bakongdemo.base.BaseViewModel
import kh.org.nbc.bakongdemo.utils.SingleLiveEvent
import kh.org.nbc.bakongdemo.bakongcore.rxschedulers.RxSchedulers
import kh.org.nbc.bakongdemo.screen.qrrecovery.permissions.StoragePermissionChecker
import kh.org.nbc.bakongdemo.screen.qrrecovery.repository.QrRecoveryRepository
import javax.inject.Inject


class QrRecoveryViewModel @Inject constructor(
    private var mQrRecoveryRepository: QrRecoveryRepository,
    private var rxSchedulers: RxSchedulers,
    private val storagePermissionChecker: StoragePermissionChecker
) : BaseViewModel() {

    private val qrFilePath = MutableLiveData<String>()
    private val saveQrFile =
        SingleLiveEvent<String>()
    private val storagePermission =
        SingleLiveEvent<PermissionStatus>()
    private val _isPermissionGranted = MutableLiveData<Boolean> ()

    init {
        _isPermissionGranted.value = false
    }

    fun createRecoveryQr(){
        unsubscribeOnDestroy(mQrRecoveryRepository.createRecoveryQr()
            .subscribeOn(rxSchedulers.io())
            .observeOn(rxSchedulers.main())
            .subscribe(
                { file -> qrFilePath.value = file },
                { throwable -> showError(throwable) }
            ))
    }

    fun getQrFilePath(): LiveData<String> = qrFilePath

    fun getSaveQrFile(): LiveData<String> = saveQrFile

    fun storagePermission(): LiveData<PermissionStatus> = storagePermission

    val isPermissionGranted: LiveData<Boolean> get() = _isPermissionGranted

    fun saveQr() {
        if (storagePermissionChecker.hasPermission()) {
            qrFilePath.value?.let {
                saveToDownload(it)
                _isPermissionGranted.postValue(true)
            }
        } else {
            storagePermission.value = PermissionStatus.REQUEST_PERMISSION_STATUS
        }
    }

    private fun saveToDownload(path: String) {
        unsubscribeOnDestroy(mQrRecoveryRepository.saveQrToDownload(path)
            .subscribeOn(rxSchedulers.io())
            .observeOn(rxSchedulers.main())
            .subscribe(
                { file -> saveQrFile.value = file },
                { throwable -> showError(throwable) }
            ))
    }

    fun onPermissionHandle(granted: Boolean) {
        if (granted) {
            qrFilePath.value?.let { saveToDownload(it) }
        } else {
            storagePermission.value = PermissionStatus.DECLINE_PERMISSION_STATUS
        }
    }


    enum class PermissionStatus {
        REQUEST_PERMISSION_STATUS,
        DECLINE_PERMISSION_STATUS
    }

}