package kh.org.nbc.bakongdemo.screen.registration.repository

import io.reactivex.Single
import kh.org.nbc.bakongdemo.screen.changephone.data.response.ChangePhoneData
import kh.org.nbc.bakongdemo.screen.phone.data.RequestIdModel
import kh.org.nbc.bakongdemo.screen.qrrestore.utils.MessageSignature
import kh.org.nbc.bakongdemo.screen.registration.data.LoginStatusModel
import kh.org.nbc.bakongdemo.screen.registration.data.RegistrationModel
import kh.org.nbc.bakongdemo.screen.registration.data.RestoreMessage
import kh.org.nbc.bakongdemo.screen.username.data.AccountCreationModel
import kh.org.nbc.bakongdemo.screen.verification.data.VerificationModel

interface RegistrationRepository {
    fun registerPhone(phone: String): Single<RequestIdModel>
    fun verifyPhone(verificationCode: String, publicKey: String): Single<VerificationModel>
    fun registerUserInfo(): Single<RegistrationModel>
    fun createAccount(login: String): Single<AccountCreationModel?>
    fun restoreAccount(publicKey: String?, messageSignature: MessageSignature?, requestId: Long): Single<Boolean?>
    fun fetchRestoreMessage(): Single<RestoreMessage?>
    fun checkLogin(username: String): Single<LoginStatusModel>

    fun changePhone(
        phone: String
    ): Single<ChangePhoneData>

    fun verifyChangePhone(
        verificationCode: String
    ): Single<Boolean>

}