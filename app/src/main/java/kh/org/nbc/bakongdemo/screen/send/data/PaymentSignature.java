package kh.org.nbc.bakongdemo.screen.send.data;

import java.util.Arrays;
import java.util.Objects;

public class PaymentSignature {

    private byte[] mSignedBytes;
    private String mTransactionHash;

    public PaymentSignature(byte[] signedBytes, String transactionHash) {
        mSignedBytes = signedBytes;
        mTransactionHash = transactionHash;
    }

    public byte[] getSignedBytes() {
        return mSignedBytes;
    }

    public void setSignedBytes(byte[] signedBytes) {
        mSignedBytes = signedBytes;
    }

    public String getTransactionHash() {
        return mTransactionHash;
    }

    public void setTransactionHash(String transactionHash) {
        mTransactionHash = transactionHash;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PaymentSignature that = (PaymentSignature) o;
        return Arrays.equals(mSignedBytes, that.mSignedBytes) &&
                Objects.equals(mTransactionHash, that.mTransactionHash);
    }

    @Override
    public int hashCode() {
        int result = Objects.hash(mTransactionHash);
        result = 31 * result + Arrays.hashCode(mSignedBytes);
        return result;
    }

    @Override
    public String toString() {
        return "PaymentSignature{" +
                "mSignedBytes=" + Arrays.toString(mSignedBytes) +
                ", mTransactionHash='" + mTransactionHash + '\'' +
                '}';
    }
}
