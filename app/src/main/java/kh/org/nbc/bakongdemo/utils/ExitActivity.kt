package kh.org.nbc.bakongdemo.utils

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Bundle

class ExitActivity : Activity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        finishAndRemoveTask()
    }

    companion object {
        fun newIntent(context: Context): Intent {
            val intent = Intent(context, ExitActivity::class.java)
            intent.flags = intent.flags or Intent.FLAG_ACTIVITY_NO_ANIMATION
            return Intent.makeRestartActivityTask(intent.component)
        }
    }
}