package kh.org.nbc.bakongdemo.screen.home.data

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.airbnb.lottie.LottieAnimationView
import kh.org.nbc.bakongdemo.R
import kh.org.nbc.bakongdemo.utils.show
import kotlin.collections.ArrayList

class TransactionAdapter(
    val context: Context,
    private var transactions: ArrayList<HistoryTransaction?>
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        var holder: RecyclerView.ViewHolder? = null

        if (viewType == VIEW_ITEM) {
            val view: View =
                LayoutInflater.from(context).inflate(R.layout.item_transaction, parent, false)
            holder = ModelListVH(view)
        } else if (viewType == VIEW_LOADING) {
            val view: View =
                LayoutInflater.from(context).inflate(R.layout.item_progress, parent, false)
            holder = ProgressVH(view)
        }
        return holder!!

    }

    override fun getItemCount(): Int {
        return transactions.size
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (holder is ModelListVH) {
            val modelVH: ModelListVH = holder
            if (transactions[position] != null) {
                modelVH.onBind(transactions[position]!!, context)

            }

        } else if (holder is ProgressVH) {
            val progressVH: ProgressVH = holder
            progressVH.showProgressBar()
        }
    }

    override fun getItemViewType(position: Int): Int {
        return if (this.transactions[position] == null) VIEW_LOADING else VIEW_ITEM
    }

    fun addTransaction(transactions: ArrayList<HistoryTransaction>) {
        this.transactions.addAll(transactions)
        //notifyItemRangeInserted(this.transactions.size, this.transactions.size + transactions.size)
        notifyDataSetChanged()
    }

    fun isEmpty(): Boolean{
        return transactions.isEmpty()
    }

    fun addProgressBar() {
        if (transactions.isEmpty() || transactions[transactions.size - 1] != null) {
            transactions.add(null)
            notifyItemInserted(transactions.size - 1)
        }
    }

    fun removeProgressBar() {
        transactions.removeAt(transactions.size - 1)
        notifyItemRemoved(transactions.size - 1)
    }

    fun removeList() {
        val currentSize = transactions.size
        transactions.clear()
        notifyItemRangeRemoved(0, currentSize)
    }

    class ModelListVH(itemView: View) : RecyclerView.ViewHolder(itemView) {
        private val transactionName = itemView.findViewById<TextView>(R.id.text_tran_name)
        private val amount = itemView.findViewById<TextView>(R.id.text_amount)
        @SuppressLint("SetTextI18n")
        fun onBind(transaction: HistoryTransaction, context: Context) {
            transactionName.text = "${transaction.transactionType}/${transaction.sourceName}/${transaction.status.toLowerCase()}"
            var displayAmount : String = if (transaction.currencyName == "KHR"){
                "៛ ${transaction.amount}"
            }else "$ ${transaction.amount}"

            displayAmount = if (transaction.transactionType == "Receive"){
                "+ $displayAmount"
            }else {
                "- $displayAmount"
            }
            amount.text = displayAmount
        }
    }

    class ProgressVH(itemView: View) : RecyclerView.ViewHolder(itemView) {
        private val progressBar = itemView.findViewById<LottieAnimationView>(R.id.progressBar)

        fun showProgressBar() {
            progressBar.show()
        }

    }

    companion object {
        const val VIEW_ITEM = 1
        const val VIEW_LOADING = 0
//        val TAG = AssetListAdapter::class.simpleName
    }
}