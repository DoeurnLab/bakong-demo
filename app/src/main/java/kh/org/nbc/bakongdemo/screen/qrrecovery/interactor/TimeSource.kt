package kh.org.nbc.bakongdemo.screen.qrrecovery.interactor

interface TimeSource {

    fun currentTimeMillis(): Long
}
