package kh.org.nbc.bakongdemo.bakongcore.interactor

import io.reactivex.Single
import kh.org.nbc.bakongdemo.utils.GrantEditTransaction
import kh.org.nbc.bakongdemo.utils.GrantPermissionTransaction

interface IrohaInteractor {

    fun grantEditPermissionTransaction(accountId: String): Single<GrantEditTransaction>

    fun grantPermission(grantPermissionTransaction: GrantPermissionTransaction): Single<ByteArray>
}
