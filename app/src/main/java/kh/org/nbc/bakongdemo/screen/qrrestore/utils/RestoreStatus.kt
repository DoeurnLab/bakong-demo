package kh.org.nbc.bakongdemo.screen.qrrestore.utils

import com.google.gson.annotations.SerializedName

data class RestoreStatus(
    @SerializedName("accountRecovered")
    val accountRestore: Boolean
)