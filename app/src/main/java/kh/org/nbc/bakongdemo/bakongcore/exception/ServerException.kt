package kh.org.nbc.bakongdemo.bakongcore.exception

import kh.org.nbc.bakongdemo.bakongcore.alert.Alert
import java.io.IOException

open class ServerException(
    val code: Int,
    val errorCode: Int,
    message: String,
    val alert: Alert? = null
) : IOException(message)
