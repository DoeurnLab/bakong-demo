package kh.org.nbc.bakongdemo.screen.qrrecovery.repository

import android.net.Uri

interface QrRepository {
    fun encode(text: String): String
    fun decode(path: String): String
    fun decode(uri: Uri): String?
}