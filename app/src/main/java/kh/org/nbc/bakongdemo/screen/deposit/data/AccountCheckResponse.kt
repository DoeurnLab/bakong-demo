package kh.org.nbc.bakongdemo.screen.deposit.data

import com.google.gson.annotations.SerializedName

class AccountCheckResponse {

    @SerializedName("account")
    var account: String? = null
    @SerializedName("accountName")
    var accountName: String? = null
    @SerializedName("accountCurrency")
    var accountCurrency: String? = null
    @SerializedName("bankCode")
    var bankCode: String? = null
    @SerializedName("accountType")
    var accountType: String? = null
    override fun toString(): String {
        return "AccountCheckResponse(account=$account, accountName=$accountName, accountCurrency=$accountCurrency, bankCode=$bankCode, accountType=$accountType)"
    }


}