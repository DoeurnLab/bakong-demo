package kh.org.nbc.bakongdemo.screen.registration.data

import com.google.gson.annotations.SerializedName

data class AccountCheck(
    @SerializedName("free")
    val isFree: Boolean
)