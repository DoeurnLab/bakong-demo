package kh.org.nbc.bakongdemo.bakongcore.token;

import androidx.annotation.NonNull;

import java.io.IOException;

import javax.inject.Inject;

import kh.org.nbc.bakongdemo.storage.Storage;
import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

public class TokenInterceptor implements Interceptor {

    private static final String ACCESS_TOKEN_HEADER = "Authorization";

    private final Storage storage;

    @Inject
    public TokenInterceptor(@NonNull Storage storage) {
        this.storage = storage;
    }

    @Override
    public Response intercept(@NonNull Chain chain) throws IOException {
        Request.Builder requestBuilder = chain.request().newBuilder();
        if (chain.request().header(ACCESS_TOKEN_HEADER) == null) {
            String token = storage.getToken();
            if (token != null) {
                requestBuilder.addHeader(ACCESS_TOKEN_HEADER, token);
            }
        }
        return chain.proceed(requestBuilder.build());
    }
}
