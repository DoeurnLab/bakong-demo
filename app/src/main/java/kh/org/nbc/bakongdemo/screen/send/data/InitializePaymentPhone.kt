package kh.org.nbc.bakongdemo.screen.send.data

import com.google.gson.annotations.SerializedName

data class InitializePaymentPhone(
    @SerializedName("receiverFullName")
    val receiverFullName: String,
    @SerializedName("receiverAccountId")
    val receiverAccountId: String
)