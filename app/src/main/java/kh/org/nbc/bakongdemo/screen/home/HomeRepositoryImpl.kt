package kh.org.nbc.bakongdemo.screen.home

import io.reactivex.Single
import kh.org.nbc.bakongdemo.screen.home.data.AccountAsset
import kh.org.nbc.bakongdemo.screen.home.data.TransactionData
import kh.org.nbc.bakongdemo.screen.home.data.TransactionRequest
import kh.org.nbc.bakongdemo.screen.registration.api.BakongApi
import javax.inject.Inject

class HomeRepositoryImpl @Inject constructor(
    private val api: BakongApi
) : HomeRepository {
    override fun getBalance(): Single<AccountAsset> {
        return api.getBalance().map { it.data }
    }

    override fun getTransactions(request: TransactionRequest): Single<TransactionData> {
        return api.transactions(request).map { it.data }
    }
}