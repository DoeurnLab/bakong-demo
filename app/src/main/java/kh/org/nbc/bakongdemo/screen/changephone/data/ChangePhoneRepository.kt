package kh.org.nbc.bakongdemo.screen.changephone.data

import io.reactivex.Single
import kh.org.nbc.bakongdemo.screen.changephone.data.response.ChangePhoneData
import kh.org.nbc.bakongdemo.screen.qrrestore.utils.MessageSignature
import kh.org.nbc.bakongdemo.screen.registration.data.InternationalPhone
import kh.org.nbc.bakongdemo.screen.registration.data.RestoreMessage

interface ChangePhoneRepository {
    fun restoreMessage(): Single<RestoreMessage>

    fun changePhone(
        phone: InternationalPhone,
        requestId: Long,
        messageSignature: MessageSignature,
        publicKey: String
    ): Single<ChangePhoneData>

    fun verifyPhone(
        verificationCode: String,
        requestId: Long,
        publicKey: String
    ): Single<Boolean>
}