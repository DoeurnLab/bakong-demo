package kh.org.nbc.bakongdemo.bakongcore.exception

import kh.org.nbc.bakongdemo.bakongcore.exception.ServerException

class ErrorServerException(
    code: Int,
    errorCode: Int,
    message: String
) : ServerException(code, errorCode, message)
