package kh.org.nbc.bakongdemo.base;

import com.google.gson.annotations.SerializedName;

import java.util.Objects;

import kh.org.nbc.bakongdemo.utils.Status;

public class BaseResponse<T> {

    @SerializedName("status")
    private Status mStatus;
    @SerializedName("data")
    private T data;

    public Status getStatus() {
        return mStatus;
    }

    public void setStatus(Status status) {
        mStatus = status;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BaseResponse<?> that = (BaseResponse<?>) o;
        return Objects.equals(mStatus, that.mStatus) &&
                Objects.equals(data, that.data);
    }

    @Override
    public int hashCode() {
        return Objects.hash(mStatus, data);
    }

    @Override
    public String toString() {
        return "BaseResponse{" +
                "mStatus=" + mStatus +
                ", data=" + data +
                '}';
    }
}

