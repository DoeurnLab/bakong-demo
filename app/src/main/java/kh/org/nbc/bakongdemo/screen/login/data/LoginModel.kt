package kh.org.nbc.bakongdemo.screen.login.data

data class LoginModel(
    val accountId: String?,
    val firstName: String?,
    val lastName: String?,
    val tokenExpire: Long? = null,
    val token: String? = null
)
