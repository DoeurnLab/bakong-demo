package kh.org.nbc.bakongdemo.bakongcore.repository;

import androidx.annotation.NonNull;

import io.reactivex.Single;
import kh.org.nbc.bakongdemo.screen.send.data.PaymentPayload;
import kh.org.nbc.bakongdemo.screen.send.data.PaymentSignature;
import kh.org.nbc.bakongdemo.utils.GrantEditTransaction;
import kh.org.nbc.bakongdemo.utils.GrantPermissionTransaction;

public interface IrohaRepository {

    Single<GrantEditTransaction> createGrantEditSignatoryTransactionHash(@NonNull String accountId);

    Single<byte[]> grantPermission(@NonNull GrantPermissionTransaction grantPermissionTransaction);

    Single<PaymentSignature> signPayment(@NonNull PaymentPayload paymentPayload);


}
