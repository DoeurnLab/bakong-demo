package kh.org.nbc.bakongdemo.screen.home.data

import com.google.gson.annotations.SerializedName

class TransactionRequest{
    @SerializedName("page")
    var page: Int? = 1
    @SerializedName("size")
    var size: Int = 10
    @SerializedName("assetId")
    var assetId: String? = null
}


