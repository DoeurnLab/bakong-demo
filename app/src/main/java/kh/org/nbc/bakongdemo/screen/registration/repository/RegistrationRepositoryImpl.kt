package kh.org.nbc.bakongdemo.screen.registration.repository

import io.reactivex.Single
import kh.org.nbc.bakongdemo.screen.changephone.data.request.ChangePhoneRequest
import jp.co.soramitsu.feature_change_phone_impl.data.request.VerifyCodeRequest
import kh.org.nbc.bakongdemo.bakongcore.repository.BakongRepository
import kh.org.nbc.bakongdemo.screen.changephone.data.response.ChangePhoneData
import kh.org.nbc.bakongdemo.screen.phone.data.PhoneRegistrationRequest
import kh.org.nbc.bakongdemo.screen.phone.data.RequestIdModel
import kh.org.nbc.bakongdemo.screen.phone.data.mapper.PhoneRegistrationToRequestIdModelMapper
import kh.org.nbc.bakongdemo.screen.qrrestore.utils.MessageSignature
import kh.org.nbc.bakongdemo.screen.qrrestore.data.RestoreAccountRequest
import kh.org.nbc.bakongdemo.screen.registration.api.BakongApi
import kh.org.nbc.bakongdemo.screen.registration.data.*
import kh.org.nbc.bakongdemo.screen.registration.data.mapper.AccountCheckToLoginStatusModelMapper
import kh.org.nbc.bakongdemo.screen.registration.data.mapper.RegistrationToRegistrationModelMapper
import kh.org.nbc.bakongdemo.screen.username.data.AccountCreationModel
import kh.org.nbc.bakongdemo.screen.username.data.AccountRequest
import kh.org.nbc.bakongdemo.screen.username.data.mapper.AccountRegistrationToAccountCreationModelMapper
import kh.org.nbc.bakongdemo.screen.verification.data.PhoneVerificationRequest
import kh.org.nbc.bakongdemo.screen.verification.data.VerificationModel
import kh.org.nbc.bakongdemo.screen.verification.data.mapper.PhoneVerificationToVerificationModelMapper
import kh.org.nbc.bakongdemo.storage.Storage
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class RegistrationRepositoryImpl @Inject constructor(
    private val storage: Storage,
    private val mBakongApi: BakongApi,
    private val  mRequestIdModelMapper: PhoneRegistrationToRequestIdModelMapper,
    private val mVerificationModelMapper: PhoneVerificationToVerificationModelMapper,
    private val registrationToRegistrationModelMapper: RegistrationToRegistrationModelMapper,
    private val accountCheckToLoginStatusModelMapper: AccountCheckToLoginStatusModelMapper,
    private val mAccountCreationModelMapper: AccountRegistrationToAccountCreationModelMapper,
    private val bakongRepository: BakongRepository
) : RegistrationRepository {

    private lateinit var mRequestId: String
    private lateinit var mPhoneNumber : String
    override fun registerPhone(phone: String): Single<RequestIdModel> {

        val request =
            PhoneRegistrationRequest()
        request.deviceId = storage.getDeviceId()
        request.publicKey = storage.getPublicKey()
        request.versionOs = "Android"
        request.phone = phone

        return mBakongApi.phoneRegistration(request)
            .map(mRequestIdModelMapper::mapResponse)
            .map(this::saveRequestId)
    }

    override fun verifyPhone(
        verificationCode: String,
        publicKey: String
    ): Single<VerificationModel> {

        val request = PhoneVerificationRequest()
        request.verificationCode = verificationCode
        request.requestId = mRequestId
        request.publicKey = publicKey
        return mBakongApi.phoneVerification(request)
            .map(mVerificationModelMapper::mapResponse)
    }

    override fun registerUserInfo(): Single<RegistrationModel> {
        storage.setToken("")
        val request = createRegistrationRequest()
        return mBakongApi.registration(request)
            .map {
                storage.setDisplayName("${request.firstName} ${request.lastName}")
                registrationToRegistrationModelMapper.map(it.data)

            }
    }

    override fun createAccount(login: String): Single<AccountCreationModel?> {
        val request = AccountRequest()
        request.login = login
        request.deviceId = storage.getDeviceId()
        request.publicKey = storage.getPublicKey()
        return mBakongApi.accountRegistration(request)
            .map(mAccountCreationModelMapper::mapResponse)
    }


    override fun restoreAccount(
        publicKey: String?,
        messageSignature: MessageSignature?,
        requestId: Long
    ): Single<Boolean?> {
        val request =
            RestoreAccountRequest(
                publicKey!!,
                requestId,
                messageSignature!!.signature,
                storage.getDeviceId()!!
            )
        return mBakongApi.restoreAccount(request)
            .map{response -> response.data.accountRestore}
    }

    override fun fetchRestoreMessage(): Single<RestoreMessage?> {
        return mBakongApi.fetchRestoreMessage()
            .map{response -> response.data}
    }

    override fun checkLogin(username: String): Single<LoginStatusModel> {
        val request = AccountRequest()
        request.deviceId = storage.getDeviceId()
        request.login = username
        request.publicKey = storage.getPublicKey()

        return mBakongApi.accountCheck(request)
            .map {
                accountCheckToLoginStatusModelMapper.mapResponse(it)
            }
    }

    override fun changePhone(phone: String): Single<ChangePhoneData> {
        mPhoneNumber = phone
        return fetchRestoreMessage()
            .doOnSuccess { restoreMessage -> storage.setRequestId(restoreMessage!!.requestId.toString()) }
            .map(RestoreMessage::message)
            .flatMap(bakongRepository::signMessage)
            .flatMap(this::changePhoneRequest)
    }

    private fun changePhoneRequest(messageSignature: MessageSignature): Single<ChangePhoneData>{
        val changePhoneRequest = ChangePhoneRequest(
            deviceId = storage.getDeviceId()!!,
            phoneNumber =  mPhoneNumber,
            publicKey = storage.getPublicKey()!!,
            signature = messageSignature.signature,
            versionOS = "Android",
            requestId = storage.getRequestId()!!.toLong())

        return mBakongApi.changePhone(changePhoneRequest)
            .map { response -> response.data }

    }

    override fun verifyChangePhone(verificationCode: String): Single<Boolean> {
        val verifyCodeRequest = VerifyCodeRequest(
            publicKey = storage.getPublicKey()!!,
            requestId = storage.getRequestId()!!.toLong(),
            verificationCode = verificationCode)

        return mBakongApi.verifyChangePhone(verifyCodeRequest).map { response -> response.data.verified }
    }

    private fun saveRequestId(model: RequestIdModel): RequestIdModel? {
        mRequestId = model.requestId
        return model
    }

    private fun createRegistrationRequest(): RegistrationRequest {

        return RegistrationRequest(
            publicKey = storage.getPublicKey(),
            deviceId = storage.getDeviceId(),
            firstName = "Vandoeurn",
            lastName = "Pin",
            birthDate = "1994-01-04 00:00:00",
            sex = "Male",
            email = "vandoeurn@gmail.com",
            address = "Phnom Penh",
            nationality = "Khmer",
            documentId = "123123",
            identity = null,
            photo = null
        )
    }
}