/*
 * Copyright (C) 2019 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package kh.org.nbc.bakongdemo.storage

interface Storage {

    fun clearAll()

    fun setPrivateKey(key: String)
    fun getPrivateKey(): String?

    fun setPublicKey(key: String)
    fun getPublicKey(): String?

    fun setDeviceId(id: String)
    fun getDeviceId(): String?

    fun setRequestId(id: String)
    fun getRequestId(): String?

    fun setPhoneNumber(num: String)
    fun getPhoneNumber(): String?

    fun setPassword(password: String)
    fun getPassword(): String?

    fun setPinCode(pin: String)
    fun getPinCode(): String?

    fun setDisplayName(name: String)
    fun getDisplayName(): String?

    fun setToken(token: String?)
    fun getToken(): String?

    fun setAccountId(id: String)
    fun getAccountId(): String?

    fun setRegistered(isRegister: Boolean)
    fun isRegistered(): Boolean



}
