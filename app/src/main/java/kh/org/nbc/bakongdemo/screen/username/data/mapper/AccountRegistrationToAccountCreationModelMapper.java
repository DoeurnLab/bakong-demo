package kh.org.nbc.bakongdemo.screen.username.data.mapper;

import javax.inject.Inject;

import kh.org.nbc.bakongdemo.network.data.mapper.BaseServerMapper;
import kh.org.nbc.bakongdemo.screen.username.data.AccountCreationModel;
import kh.org.nbc.bakongdemo.screen.username.data.AccountRegistration;

public class AccountRegistrationToAccountCreationModelMapper
        extends BaseServerMapper<AccountRegistration, AccountCreationModel> {

    @Inject
    public AccountRegistrationToAccountCreationModelMapper() {
    }

    @Override
    public AccountCreationModel map(AccountRegistration item) {
        AccountCreationModel model = new AccountCreationModel();
        model.setAccountId(item.getAccountId());
        model.setAccountVerified(item.isAccountVerified());
        model.setHash(item.getTrxHash());
        return model;
    }
}
