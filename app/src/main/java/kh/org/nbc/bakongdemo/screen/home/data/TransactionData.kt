package kh.org.nbc.bakongdemo.screen.home.data

import com.google.gson.annotations.SerializedName

class TransactionData{
    @SerializedName("totalCount")
    var records: Int = 0
    @SerializedName("totalPages")
    var pages: Int = 0
    @SerializedName("transactions")
    var transactions: ArrayList<HistoryTransaction>? = null
}


