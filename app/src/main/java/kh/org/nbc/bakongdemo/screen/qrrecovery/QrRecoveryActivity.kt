package kh.org.nbc.bakongdemo.screen.qrrecovery

import android.Manifest
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Bundle
import androidx.lifecycle.Observer
import com.squareup.picasso.Picasso
import kh.org.nbc.bakongdemo.base.BaseActivity
import kh.org.nbc.bakongdemo.R
import kh.org.nbc.bakongdemo.application.BakongApplication
import kh.org.nbc.bakongdemo.bakongcore.alert.Alert
import kh.org.nbc.bakongdemo.bakongcore.alert.AlertDialogFragment
import kh.org.nbc.bakongdemo.utils.requestPermission
import kh.org.nbc.bakongdemo.bakongcore.rxschedulers.RxSchedulers
import kh.org.nbc.bakongdemo.screen.login.LoginActivity
import kh.org.nbc.bakongdemo.utils.setOnDebounceClickListener
import kh.org.nbc.bakongdemo.utils.toast
import kh.org.nbc.bakongdemo.screen.qrrecovery.QrRecoveryViewModel.*
import kh.org.nbc.bakongdemo.screen.qrrecovery.action.OkAction
import kh.org.nbc.bakongdemo.storage.Storage
import kh.org.nbc.bakongdemo.utils.show
import kotlinx.android.synthetic.main.activity_qr_recovery.*
import kotlinx.android.synthetic.main.layout_toolbar.button_back
import kotlinx.android.synthetic.main.layout_toolbar.text_view_title
import kotlinx.android.synthetic.main.layout_toolbar.button_next as button_done
import java.io.File
import javax.inject.Inject

class QrRecoveryActivity : BaseActivity() {

    @Inject
    lateinit var storage: Storage

    @Inject
    lateinit var viewModel: QrRecoveryViewModel

    @Inject
    lateinit var rxSchedulers: RxSchedulers

    override fun onCreate(savedInstanceState: Bundle?) {
        (application as BakongApplication).appComponent.inject(this)
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_qr_recovery)

        viewModel.getQrFilePath().observe(this, Observer { showQr(it) })
        viewModel.getSaveQrFile().observe(this, Observer { showFileSavedMessage(it) })
        viewModel.storagePermission().observe(
            this,
            Observer { handleStoragePermission(it) })

        viewModel.createRecoveryQr()
        initListeners()

        enableButton(false)

        button_done.show()
        button_done.text = resources.getString(R.string.done)
        text_view_title.text = resources.getString(R.string.qr)
        button_back.setOnClickListener { finish() }

    }

    private fun handleStoragePermission(permissionStatus: PermissionStatus) {
        when (permissionStatus) {
            PermissionStatus.REQUEST_PERMISSION_STATUS -> showStoragePermissionRequest()
            PermissionStatus.DECLINE_PERMISSION_STATUS -> showPermissionNotGranted()
        }
    }

    private fun initListeners() {
        button_save.setOnDebounceClickListener { viewModel.saveQr() }

        button_done.setOnDebounceClickListener {
            storage.setRegistered(true)
            val intent = Intent(this, LoginActivity::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK
            startActivity(intent)
        }
    }

    private fun showFileSavedMessage(path: String) {
        enableButton(true)
        showSimpleDialog(
            Alert(
                titleRes = R.string.recovery_qr_recovery_title,
                description = getString(R.string.recovery_qr_recovery_file_saved, path),
                okAction = OkAction()
            )
        )
    }

    private fun showStoragePermissionRequest() {
        requestPermission(
            Manifest.permission.WRITE_EXTERNAL_STORAGE,
            EXTERNAL_STORAGE_PERMISSION_CODE,
            getString(R.string.recovery_file_access_permision),
            getString(R.string.recovery_storage_access_permision_title),
            supportFragmentManager
        )

    }


    private fun enableButton(flag: Boolean) {
        button_done.isClickable = flag
        button_done.isEnabled = flag
        if (flag) {
            button_done.alpha = 1f
        } else
            button_done.alpha = 0.4f
    }

    private fun showPermissionNotGranted() {
        toast(R.string.recovery_qr_recovery_permission_not_granted_message)
    }

    private fun showSimpleDialog(alert: Alert) {
        val fragment = AlertDialogFragment.newInstance(alert)
        supportFragmentManager.let { fragment.show(supportFragmentManager, QR_FILE_DIALOG_TAG) }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        when (requestCode) {
            EXTERNAL_STORAGE_PERMISSION_CODE -> {
                val granted =
                    grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED
                viewModel.onPermissionHandle(granted)
            }
        }
    }


    private fun showQr(path: String) {
        Picasso.get()
            .load(File(path))
            .into(image_qr)
    }

    companion object {
        private const val EXTERNAL_STORAGE_PERMISSION_CODE = 112
        private const val QR_FILE_DIALOG_TAG = "QR_FILE_DIALOG_TAG"
    }


}