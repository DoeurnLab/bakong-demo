package kh.org.nbc.bakongdemo.bakongcore.token;

import androidx.annotation.NonNull;

import javax.annotation.Nullable;
import javax.inject.Inject;

import okhttp3.Authenticator;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.Route;

public class ServerAuthenticator implements Authenticator {

    @Inject
    public ServerAuthenticator(){

    }

    @Nullable
    @Override
    public Request authenticate(@NonNull Route route, @NonNull Response response) {
        return response.request();
    }
}
