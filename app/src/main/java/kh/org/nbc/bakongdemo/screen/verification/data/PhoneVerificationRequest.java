package kh.org.nbc.bakongdemo.screen.verification.data;

import com.google.gson.annotations.SerializedName;

import java.util.Objects;

public class PhoneVerificationRequest {

    @SerializedName("verificationCode")
    private String mVerificationCode;
    @SerializedName("requestId")
    private String mRequestId;
    @SerializedName("publicKey")
    private String mPublicKey;

    public String getVerificationCode() {
        return mVerificationCode;
    }

    public void setVerificationCode(String verificationCode) {
        mVerificationCode = verificationCode;
    }

    public String getRequestId() {
        return mRequestId;
    }

    public void setRequestId(String requestId) {
        mRequestId = requestId;
    }

    public String getPublicKey() {
        return mPublicKey;
    }

    public void setPublicKey(String publicKey) {
        mPublicKey = publicKey;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PhoneVerificationRequest that = (PhoneVerificationRequest) o;
        return Objects.equals(mVerificationCode, that.mVerificationCode) &&
                Objects.equals(mRequestId, that.mRequestId) &&
                Objects.equals(mPublicKey, that.mPublicKey);
    }

    @Override
    public int hashCode() {
        return Objects.hash(mVerificationCode, mRequestId, mPublicKey);
    }

    @Override
    public String toString() {
        return "PhoneVerificationRequest{" +
                "mVerificationCode='" + mVerificationCode + '\'' +
                ", mRequestId='" + mRequestId + '\'' +
                ", mPublicKey='" + mPublicKey + '\'' +
                '}';
    }
}
