package kh.org.nbc.bakongdemo.di.module;

import javax.inject.Singleton;

import dagger.Binds;
import dagger.Module;
import dagger.multibindings.IntoSet;
import kh.org.nbc.bakongdemo.bakongcore.interactor.IrohaInteractor;
import kh.org.nbc.bakongdemo.bakongcore.interactor.IrohaInteractorImpl;
import kh.org.nbc.bakongdemo.bakongcore.repository.IrohaRepository;
import kh.org.nbc.bakongdemo.bakongcore.repository.IrohaRepositoryImpl;
import kh.org.nbc.bakongdemo.bakongcore.error.ChangePhoneErrorHandler;
import kh.org.nbc.bakongdemo.bakongcore.error.ErrorChecker;
import kh.org.nbc.bakongdemo.bakongcore.error.ErrorCheckerImpl;
import kh.org.nbc.bakongdemo.bakongcore.error.ErrorHandler;
import kh.org.nbc.bakongdemo.screen.login.repository.LoginRepository;
import kh.org.nbc.bakongdemo.screen.login.repository.LoginRepositoryImpl;
import kh.org.nbc.bakongdemo.screen.login.repository.RecoveryRepositoryImpl;
import kh.org.nbc.bakongdemo.screen.login.interactor.RecoveryInteractor;
import kh.org.nbc.bakongdemo.screen.login.interactor.RecoveryInteractorImpl;
import kh.org.nbc.bakongdemo.screen.login.repository.RecoveryRepository;

@Module
public abstract class LoginModule {

    @Binds
    abstract LoginRepository provideLoginRepository(LoginRepositoryImpl loginRepository);

    @Binds
    abstract RecoveryRepository provideRecoveryRepository(RecoveryRepositoryImpl recoveryDataSource);
    @Binds
    abstract RecoveryInteractor provideRecoveryInteractor(RecoveryInteractorImpl recoveryInteractor);

    @Binds
    abstract IrohaInteractor provideRecoveryIrohaInteractor(IrohaInteractorImpl irohaInteractor);

    @Binds
    abstract IrohaRepository provideRecoveryIrohaRepository(IrohaRepositoryImpl irohaRepository);

    @Singleton
    @Binds
    abstract ErrorChecker provideErrorChecker(ErrorCheckerImpl errorChecker);

    @Singleton
    @Binds
    @IntoSet
    abstract ErrorHandler provideChangePhoneErrorHandler(ChangePhoneErrorHandler changePhoneErrorHandler);
}
