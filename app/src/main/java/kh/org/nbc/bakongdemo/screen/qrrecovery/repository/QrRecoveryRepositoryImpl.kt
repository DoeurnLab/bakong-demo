package kh.org.nbc.bakongdemo.screen.qrrecovery.repository

import androidx.core.util.Pair
import io.reactivex.Single
import io.reactivex.functions.BiFunction
import kh.org.nbc.bakongdemo.bakongcore.interactor.FileInteractor
import kh.org.nbc.bakongdemo.bakongcore.repository.BakongRepository
import kh.org.nbc.bakongdemo.screen.qrrecovery.interactor.TimeSource
import kh.org.nbc.bakongdemo.screen.qrrestore.utils.MessageSignature
import kh.org.nbc.bakongdemo.screen.registration.repository.RegistrationRepository
import kh.org.nbc.bakongdemo.screen.registration.data.RestoreMessage
import kh.org.nbc.bakongdemo.screen.registration.keygenerator.model.BakongKeyPair
import kh.org.nbc.bakongdemo.storage.Storage
import javax.inject.Inject

class QrRecoveryRepositoryImpl @Inject constructor(

    private var storage: Storage,

    private var qrRepository: QrRepository,
    private val fileInteractor: FileInteractor,
    private val mRegistrationRepository: RegistrationRepository,
    private val timeSource: TimeSource,
    private val bakongRepository: BakongRepository
) : QrRecoveryRepository {

    private var mRequestId: Long = 0


    override fun createRecoveryQr(): Single<String> {
        return bakongRepository.encryptKey(storage.getPassword()!!)
            .map { qrRepository.encode(it) }
    }

    override fun saveQrToDownload(qrPath: String): Single<String> {
        val filePath =
            "${fileInteractor.downloadDir()}/qr-recovery-${timeSource.currentTimeMillis()}.jpg"
        return fileInteractor.copy(qrPath, filePath)
            .toSingleDefault(filePath)
    }

    override fun restoreAccount(passcode: String, encryptedString: String): Single<Boolean> {
        return bakongRepository.decryptKey(passcode, encryptedString)
            .doOnSuccess  (this::saveKey)
            .map(BakongKeyPair::getPublicKey)
            .zipWith(signRestoreMessage(), BiFunction { publicKey: String, signRestoreMessage: MessageSignature -> Pair(publicKey,signRestoreMessage) })
            .flatMap{ pair-> mRegistrationRepository.restoreAccount(pair.first, pair.second, mRequestId)}
    }

    private fun saveKey(keyPair: BakongKeyPair){
        storage.setPrivateKey(keyPair.getPrivateKey())
        storage.setPublicKey(keyPair.getPublicKey())
    }

    private fun signRestoreMessage(): Single<MessageSignature> {
        return mRegistrationRepository.fetchRestoreMessage()
            .doOnSuccess { restoreMessage -> mRequestId = restoreMessage!!.requestId }
            .map(RestoreMessage::message)
            .flatMap(bakongRepository::signMessage)
    }










}
