package kh.org.nbc.bakongdemo.base

import androidx.appcompat.app.AppCompatActivity
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import kh.org.nbc.bakongdemo.bakongcore.alert.Alert
import kh.org.nbc.bakongdemo.bakongcore.dialog.DialogHelper

abstract class BaseActivity : AppCompatActivity() {

    private val compositeDisposable = CompositeDisposable()
    private val dialogHelper =
        DialogHelper()

    override fun onDestroy() {
        compositeDisposable.clear()
        super.onDestroy()
    }


    protected fun unsubscribeOnDestroy(disposable: Disposable) {
        compositeDisposable.add(disposable)
    }

    fun onError(alert: Alert) {
        dialogHelper.showError(this, alert)
    }


}
