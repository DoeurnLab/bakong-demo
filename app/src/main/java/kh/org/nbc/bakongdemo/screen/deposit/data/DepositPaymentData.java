package kh.org.nbc.bakongdemo.screen.deposit.data;

import java.util.Objects;

import kh.org.nbc.bakongdemo.utils.Money;

public class DepositPaymentData {
    private String mAccountNumber;
    private String mBankId;
    private String mDescription;
    private Money mMoney;
    private long mTime;

    public String getAccountNumber() {
        return mAccountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        mAccountNumber = accountNumber;
    }

    public String getBankId() {
        return mBankId;
    }

    public void setBankId(String bankId) {
        mBankId = bankId;
    }

    public String getDescription() {
        return mDescription;
    }

    public void setDescription(String description) {
        mDescription = description;
    }

    public Money getMoney() {
        return mMoney;
    }

    public void setMoney(Money money) {
        this.mMoney = money;
    }

    public long getTime() {
        return mTime;
    }

    public void setTime(long time) {
        mTime = time;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        DepositPaymentData that = (DepositPaymentData) o;
        return mTime == that.mTime &&
                Objects.equals(mAccountNumber, that.mAccountNumber) &&
                Objects.equals(mBankId, that.mBankId) &&
                Objects.equals(mDescription, that.mDescription) &&
                Objects.equals(mMoney, that.mMoney);
    }

    @Override
    public int hashCode() {
        return Objects.hash(mAccountNumber, mBankId, mDescription, mMoney, mTime);
    }

    @Override
    public String toString() {
        return "DepositPaymentData{" +
                "mAccountNumber='" + mAccountNumber + '\'' +
                ", mBankId='" + mBankId + '\'' +
                ", mDescription='" + mDescription + '\'' +
                ", mMoney=" + mMoney +
                ", mTime=" + mTime +
                '}';
    }
}
