package kh.org.nbc.bakongdemo.screen.registration

import android.content.Intent
import android.os.Bundle
import android.util.Log
import io.reactivex.Completable
import io.reactivex.Single
import kh.org.nbc.bakongdemo.R
import kh.org.nbc.bakongdemo.application.BakongApplication
import kh.org.nbc.bakongdemo.screen.registration.keygenerator.model.BakongKeyPair
import kh.org.nbc.bakongdemo.base.BaseActivity
import kh.org.nbc.bakongdemo.screen.registration.keygenerator.*
import kh.org.nbc.bakongdemo.bakongcore.rxschedulers.RxSchedulers
import kh.org.nbc.bakongdemo.screen.login.LoginActivity
import kh.org.nbc.bakongdemo.screen.phone.PhoneRegisterActivity
import kh.org.nbc.bakongdemo.screen.qrrestore.view.QrRestoreActivity
import kh.org.nbc.bakongdemo.storage.Storage
import kotlinx.android.synthetic.main.activity_registration.*
import java.util.*
import javax.inject.Inject

class RegistrationActivity : BaseActivity() {

    @Inject
    lateinit var mMnemonicInteractor: MnemonicInteractor
    private lateinit var passPhrase: String

    @Inject
    lateinit var mKeysCreator: KeysCreator

    @Inject
    lateinit var storage: Storage

    @Inject
    lateinit var rxSchedulers: RxSchedulers

    private lateinit var userType: UserType

    private enum class UserType {
        NEW_USER,
        RESTORE
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        (application as BakongApplication).appComponent.inject(this)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_registration)
        storage.setToken("")

        button_register.setOnClickListener {
            userType = UserType.NEW_USER
            generateKey()
        }
        button_restore.setOnClickListener {
            userType = UserType.RESTORE
            generateKey()
        }

        if (storage.isRegistered()) {
            val intent = Intent(this, LoginActivity::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK
            startActivity(intent)
        }

    }

    private fun generateKey() {
        unsubscribeOnDestroy(
            getMnenomic()
                .subscribeOn(rxSchedulers.io())
                .observeOn(rxSchedulers.main())
                .subscribe({
                    //success
                    if (userType == UserType.NEW_USER) {
                        startActivity(Intent(this, PhoneRegisterActivity::class.java))
                    } else {
                        startActivity(Intent(this, QrRestoreActivity::class.java))
                    }


                }, {
                    Log.d(TAG, it.message ?: "Generate key error")
                })
        )
    }

    private fun getMnenomic(): Single<BakongKeyPair> {
        return mMnemonicInteractor.createMnemonic()
            .doOnSuccess(this::saveMnemonic)
            .flatMap(this::createKeys)
    }

    private fun saveMnemonic(passPhrase: String) {
        this.passPhrase = passPhrase
    }

    private fun createKeys(passphrase: String): Single<BakongKeyPair>? {
        return Single.just(passphrase)
            .map(mMnemonicInteractor::deriveSeedFromMnemonic)
            .flatMap(mKeysCreator::create)
            .flatMap {
                saveKeys(it).toSingleDefault(it)
            }
    }

    private fun saveKeys(bakongKeys: BakongKeyPair): Completable {
        return Completable.fromAction {
            storage.setPublicKey(bakongKeys.getPublicKey())
            storage.setPrivateKey(bakongKeys.getPrivateKey())
            storage.setDeviceId(UUID.randomUUID().toString())
        }
    }

    companion object {
        val TAG: String = RegistrationActivity::class.java.simpleName

    }


}




