package kh.org.nbc.bakongdemo.screen.changephone.data.response

import com.google.gson.annotations.SerializedName

data class ChangePhoneData(
    @SerializedName("requestId")
    val requestId: Long
)