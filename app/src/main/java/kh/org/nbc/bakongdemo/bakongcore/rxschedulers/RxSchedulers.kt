package kh.org.nbc.bakongdemo.bakongcore.rxschedulers

import io.reactivex.Scheduler

interface RxSchedulers {

    fun main(): Scheduler

    fun io(): Scheduler

    fun computation(): Scheduler

    fun newThread(): Scheduler
}
