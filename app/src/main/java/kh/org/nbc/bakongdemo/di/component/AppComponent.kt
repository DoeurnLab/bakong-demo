package kh.org.nbc.bakongdemo.di.component

import android.content.Context
import dagger.BindsInstance
import dagger.Component
import kh.org.nbc.bakongdemo.di.module.*
import kh.org.nbc.bakongdemo.screen.changephone.ChangePhoneActivity
import kh.org.nbc.bakongdemo.screen.changephone.VerificationChangePhoneActivity
import kh.org.nbc.bakongdemo.screen.deposit.BankListActivity
import kh.org.nbc.bakongdemo.screen.deposit.DepositActivity
import kh.org.nbc.bakongdemo.screen.home.HomeActivity
import kh.org.nbc.bakongdemo.screen.login.LoginActivity
import kh.org.nbc.bakongdemo.screen.password.PasswordActivity
import kh.org.nbc.bakongdemo.screen.phone.PhoneRegisterActivity
import kh.org.nbc.bakongdemo.screen.qrrecovery.QrRecoveryActivity
import kh.org.nbc.bakongdemo.screen.qrrestore.view.CustomQrActivity
import kh.org.nbc.bakongdemo.screen.qrrestore.view.QrRestoreActivity
import kh.org.nbc.bakongdemo.screen.registration.RegistrationActivity
import kh.org.nbc.bakongdemo.screen.selectphone.SelectPhoneActivity
import kh.org.nbc.bakongdemo.screen.send.SendActivity
import kh.org.nbc.bakongdemo.screen.username.UsernameActivity
import kh.org.nbc.bakongdemo.screen.verification.VerificationActivity
import javax.inject.Singleton

@Singleton
@Component(modules = [
    StorageModule::class,
    ApplicationModule::class,
    NetworkModule::class,
    RegistrationModule::class,
    LoginModule::class,
    HomeModule::class,
    SendModule::class,
    ChangePhoneModule::class,
    DepositModule::class,
    QrRestoreModule::class])
interface AppComponent {

    //Factory to create instance of the AppComponent
    @Component.Factory
    interface Factory{
        fun create(@BindsInstance context: Context): AppComponent
    }
    fun inject(activity: PhoneRegisterActivity?)
    fun inject(activity: RegistrationActivity?)
    fun inject(activity: VerificationActivity?)
    fun inject(activity: UsernameActivity)
    fun inject(activity: PasswordActivity)
    fun inject(activity: QrRecoveryActivity)
    fun inject(activity: LoginActivity)
    fun inject(activity: QrRestoreActivity)
    fun inject(activity: CustomQrActivity)
    fun inject(activity: HomeActivity)
    fun inject(activity: ChangePhoneActivity)
    fun inject(activity: VerificationChangePhoneActivity)
    fun inject(activity: SelectPhoneActivity)
    fun inject(activity: SendActivity)
    fun inject(activity: BankListActivity)
    fun inject(activity: DepositActivity)

}