package kh.org.nbc.bakongdemo.screen.qrrecovery.permissions

interface StoragePermissionChecker {
    fun hasPermission(): Boolean
}