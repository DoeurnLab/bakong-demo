package kh.org.nbc.bakongdemo.bakongcore.error

import kh.org.nbc.bakongdemo.bakongcore.exception.ServerException
import kh.org.nbc.bakongdemo.utils.Status


interface ErrorHandler {
    fun canHandle(status: Status): Boolean

    fun retrieveError(status: Status): ServerException
}