package kh.org.nbc.bakongdemo.screen.deposit

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import kh.org.nbc.bakongdemo.bakongcore.rxschedulers.RxSchedulers
import kh.org.nbc.bakongdemo.base.BaseViewModel
import kh.org.nbc.bakongdemo.screen.deposit.data.BankListResponse
import javax.inject.Inject

class BankListViewModel @Inject constructor(
    private val rxSchedulers: RxSchedulers,
    private val repository: DepositRepository
): BaseViewModel() {


    private val _bankListResponse = MutableLiveData<List<BankListResponse>>()
    val bankListResponse: LiveData<List<BankListResponse>> get() = _bankListResponse

    fun getBankList(){
        unsubscribeOnDestroy(
            repository.getDepositBankList()
                .subscribeOn(rxSchedulers.io())
                .observeOn(rxSchedulers.main())
                .doOnSubscribe { _loading.postValue(true) }
                .doFinally { _loading.postValue(false) }
                .subscribe({
                    _bankListResponse.postValue(it!!)
                }, { throwable ->
                    showError(throwable)
                })
        )
    }

}