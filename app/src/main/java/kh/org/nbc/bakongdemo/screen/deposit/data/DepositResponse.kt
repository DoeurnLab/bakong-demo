package kh.org.nbc.bakongdemo.screen.deposit.data

import com.google.gson.annotations.SerializedName

class DepositResponse {

    @SerializedName("receiverAccountId")
    var receiverAccountId: String? = null
    @SerializedName("assetId")
    var assetId: String? = null
    @SerializedName("description")
    var description: String? = null
    @SerializedName("amount")
    var amount: String? = null
    @SerializedName("transactionDate")
    var transactionDate: String? = null
    @SerializedName("trxHash")
    var trxHash: String? = null
    override fun toString(): String {
        return "DepositResponse(receiverAccountId=$receiverAccountId, assetId=$assetId, description=$description, amount=$amount, transactionDate=$transactionDate, trxHash=$trxHash)"
    }


}