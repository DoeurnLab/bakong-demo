package kh.org.nbc.bakongdemo.screen.changephone.data.request

import com.google.gson.annotations.SerializedName

data class ChangePhoneRequest(
    @SerializedName("deviceId")
    val deviceId: String,
    @SerializedName("phoneNumber")
    val phoneNumber: String,
    @SerializedName("publicKey")
    val publicKey: String,
    @SerializedName("requestId")
    val requestId: Long,
    @SerializedName("signature")
    val signature: ByteArray,
    @SerializedName("versionOS")
    val versionOS: String
)