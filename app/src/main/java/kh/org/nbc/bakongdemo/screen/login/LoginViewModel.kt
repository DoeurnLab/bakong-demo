package kh.org.nbc.bakongdemo.screen.login

import androidx.lifecycle.MutableLiveData
import kh.org.nbc.bakongdemo.base.BaseViewModel
import kh.org.nbc.bakongdemo.bakongcore.rxschedulers.RxSchedulers
import kh.org.nbc.bakongdemo.screen.login.repository.LoginRepository
import kh.org.nbc.bakongdemo.storage.Storage
import javax.inject.Inject

class LoginViewModel @Inject constructor(
    private val storage: Storage,
    private val rxSchedulers: RxSchedulers,
    private val loginRepository: LoginRepository
) : BaseViewModel() {

    val loginSuccess = MutableLiveData<Boolean>()

    fun login() {
        storage.setToken("")
        unsubscribeOnDestroy(
            loginRepository.login(storage.getPublicKey()!!, storage.getDeviceId()!!)
                .subscribeOn(rxSchedulers.io())
                .observeOn(rxSchedulers.main())
                .doOnSubscribe { _loading.postValue(true) }
                .subscribe(
                    {
                        storage.setToken(it.token)
                        storage.setAccountId(it.accountId!!)
                        checkAccountRecovery()
                    },
                    { throwable ->
                        showError(throwable)
                        _loading.postValue(false)
                    })
        )
    }
    private fun checkAccountRecovery() {
        unsubscribeOnDestroy(
            loginRepository.status()
                .subscribeOn(rxSchedulers.io())
                .observeOn(rxSchedulers.main())
                .doFinally { _loading.postValue(false) }
                .subscribe(
                    {created->
                        if (!created){
                            loginRepository.createRecovery(storage.getAccountId()!!)
                                .subscribeOn(rxSchedulers.io())
                                .subscribe ({
                                    loginSuccess.postValue(true)
                                },{
                                    showError(it)
                                    loginSuccess.postValue(true)
                                })
                        }else{
                            loginSuccess.postValue(true)
                        }
                    },
                    { throwable ->
                        showError(throwable)
                        _loading.postValue(false)
                    })
        )
    }

    companion object{
        const val TAG = "LoginViewModel"
    }
}