package kh.org.nbc.bakongdemo.screen.changephone

import android.content.Intent
import android.os.Bundle
import android.text.TextUtils
import androidx.lifecycle.Observer
import kh.org.nbc.bakongdemo.R
import kh.org.nbc.bakongdemo.application.BakongApplication
import kh.org.nbc.bakongdemo.base.BaseActivity
import kh.org.nbc.bakongdemo.utils.hide
import kh.org.nbc.bakongdemo.utils.show
import kotlinx.android.synthetic.main.activity_change_phone.*
import kotlinx.android.synthetic.main.layout_toolbar.*
import kotlinx.android.synthetic.main.layout_toolbar.button_next as button_update
import javax.inject.Inject

class ChangePhoneActivity : BaseActivity() {

    @Inject
    lateinit var viewModel: ChangePhoneViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        (application as BakongApplication).appComponent.inject(this)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_change_phone)

        viewModel.loading.observe(this, Observer {
            if (it){
                progressBar.show()
            }else{
                progressBar.hide()
            }
        })

        viewModel.success.observe(this, Observer {
            if (it){
                val intent = Intent(this, VerificationChangePhoneActivity::class.java)
                startActivity(intent)
            }
        })
        viewModel.error.observe(this, Observer {
            onError(it)
        })


        text_view_title.text = resources.getString(R.string.change_phone)
        button_update.text = resources.getString(R.string.update)
        button_update.show()
        button_update.setOnClickListener {
            if (!TextUtils.isEmpty(edit_text_phone_number.text)){
                val phone : String = edit_text_phone_number.text.toString()
                if(phone.startsWith("855")){
                    viewModel.changePhone(phone)
                }else{
                    viewModel.changePhone("855$phone")
                }
            }

        }
    }
}