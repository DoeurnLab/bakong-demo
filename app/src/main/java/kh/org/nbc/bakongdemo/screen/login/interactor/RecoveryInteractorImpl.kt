package kh.org.nbc.bakongdemo.screen.login.interactor

import io.reactivex.Completable
import io.reactivex.Single
import kh.org.nbc.bakongdemo.utils.GrantPermissionTransaction
import kh.org.nbc.bakongdemo.bakongcore.interactor.IrohaInteractor
import kh.org.nbc.bakongdemo.screen.login.repository.RecoveryRepository
import javax.inject.Inject

class RecoveryInteractorImpl @Inject constructor(
    private val irohaInteractor: IrohaInteractor,
    private val recoveryRepository: RecoveryRepository
) : RecoveryInteractor {
    override fun createRecovery(accountId: String ): Completable {

        return irohaInteractor.grantEditPermissionTransaction(accountId)
            .map { grandEditTransaction ->
                accountId to grandEditTransaction
            }

            .flatMap { (accountId, grandEditTransaction) ->
                recoveryRepository.createRecoveryAccount(
                    accountId,
                    grandEditTransaction.transactionHash
                )
                    .map { data -> data to grandEditTransaction }
            }
            .flatMap { (data, grandPermission) ->
                irohaInteractor.grantPermission(
                    GrantPermissionTransaction(
                        grandPermission.transaction,
                        data.grantPermissionRecoveryToBankReducedHash,
                        data.addSignatoryChangeQuorumReducedHash
                    )
                )
            }
            .flatMap { transaction -> recoveryRepository.grantRecoveryAccount(transaction) }
            .ignoreElement()

    }


    override fun status(): Single<Boolean> = recoveryRepository.status()


}