package kh.org.nbc.bakongdemo.screen.login.data.response

import com.google.gson.annotations.SerializedName

data class GrantRecoveryData(
    @SerializedName("grantPermissionToRecoveryHash")
    val grantPermissionToRecoveryHash: String,
    @SerializedName("grantPermissionToBankHash")
    val grantPermissionToBankHash: String,
    @SerializedName("addSignatorySetQuorumHash")
    val addSignatorySetQuorumHash: String
)