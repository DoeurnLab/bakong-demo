package kh.org.nbc.bakongdemo.screen.deposit.data

import com.google.gson.annotations.SerializedName

class Deposit {
    @SerializedName("receiverAccount")
    var receiverAccount: String? = null
    @SerializedName("bankAccountId")
    var bankAccountId: String? = null
    @SerializedName("description")
    var description: String? = null
    @SerializedName("assetId")
    var assetId: String? = null
    @SerializedName("amount")
    var amount: String? = null
    @SerializedName("signByte")
    var signByte: ByteArray? = null
    @SerializedName("trxHash")
    var trxHash: String? = null
    @SerializedName("qrCode")
    var qrCode: String? = null
}