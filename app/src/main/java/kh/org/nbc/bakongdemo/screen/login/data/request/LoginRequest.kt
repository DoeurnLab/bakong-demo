package kh.org.nbc.bakongdemo.screen.login.data.request

import com.google.gson.annotations.SerializedName

data class LoginRequest(
    @SerializedName("devId")
    var deviceId: String? = null,
    @SerializedName("publicKey")
    var publicKey: String? = null
)
