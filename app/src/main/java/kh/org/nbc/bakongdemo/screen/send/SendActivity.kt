package kh.org.nbc.bakongdemo.screen.send

import android.content.DialogInterface
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.appcompat.app.AlertDialog
import androidx.core.content.ContextCompat
import androidx.lifecycle.Observer
import kh.org.nbc.bakongdemo.R
import kh.org.nbc.bakongdemo.application.BakongApplication
import kh.org.nbc.bakongdemo.base.BaseActivity
import kh.org.nbc.bakongdemo.screen.selectphone.SelectPhoneActivity.Companion.FULL_NAME
import kh.org.nbc.bakongdemo.screen.selectphone.SelectPhoneActivity.Companion.REC_ID
import kh.org.nbc.bakongdemo.storage.Storage
import kh.org.nbc.bakongdemo.utils.Currency
import kh.org.nbc.bakongdemo.utils.hide
import kh.org.nbc.bakongdemo.utils.show
import kotlinx.android.synthetic.main.activity_send.*
import kotlinx.android.synthetic.main.layout_toolbar.*
import javax.inject.Inject

class SendActivity : BaseActivity() {

    private var rielActive: Boolean? = null
    private var receiveId : String ? = null
    private var fullName : String ? = null

    @Inject
    lateinit var viewModel: SendViewModel
    @Inject
    lateinit var storage: Storage
    override fun onCreate(savedInstanceState: Bundle?) {
        (application as BakongApplication).appComponent.inject(this)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_send)


        text_view_title.text = resources.getString(R.string.set_amount)
        button_back.show()
        button_back.setOnClickListener {
            finish()
        }

        receiveId = intent.getStringExtra(REC_ID)
        fullName = intent.getStringExtra(FULL_NAME)

        text_receive_id.text = receiveId
        text_receive_full_name.text = fullName

        val mApplication = (application as BakongApplication)

        text_riel_account.text = mApplication.rielAccount.toString()
        text_usd_account.text = mApplication.usdAccount.toString()

        view_usd.setOnClickListener {
            accountActive(false)
        }
        view_riel.setOnClickListener {
            accountActive(true)
        }
        accountActive(true)

        viewModel.loading.observe(this, Observer {
            if (it){
                showLoading()
            }else hideLoading()
        })

        viewModel.sendPaymentResponse.observe(this, Observer {
            AlertDialog.Builder(this)
                .setCancelable(false)
                .setTitle("Success")
                .setMessage(it.amount +" was sent successfully to ${it.receiverAccountId}")
                .setPositiveButton(R.string.ok){dialog: DialogInterface?, _: Int ->
                    dialog!!.dismiss()
                    finish()
                }.show()
        })
        button_send.setOnClickListener {
            if (rielActive ==true) viewModel.send(receiveId!!, edt_amount.text.toString(), Currency.KHR)
            else viewModel.send(receiveId!!, edt_amount.text.toString(), Currency.USD)
        }
        viewModel.error.observe(this, Observer {
            onError(it)
        })
    }

    private fun showLoading(){
        progressBar.show()
    }
    private fun hideLoading(){
        progressBar.hide()
    }

    private fun accountActive(isRielActive: Boolean){
        rielActive = isRielActive
        if (isRielActive){
            text_usd_account.setTextColor(getColor(R.color.very_light_grey))
            text_riel_account.setTextColor(getColor(R.color.deep_red))

            image_usd.setColorFilter(ContextCompat.getColor(this, R.color.very_light_grey))
            image_riel.setColorFilter(ContextCompat.getColor(this, R.color.deep_red))
            text_currency.text = "៛"

        }else{
            text_usd_account.setTextColor(getColor(R.color.pumpkin))
            text_riel_account.setTextColor(getColor(R.color.very_light_grey))

            image_usd.setColorFilter(ContextCompat.getColor(this, R.color.pumpkin))
            image_riel.setColorFilter(ContextCompat.getColor(this, R.color.very_light_grey))
            text_currency.text = "$"
        }
    }
}