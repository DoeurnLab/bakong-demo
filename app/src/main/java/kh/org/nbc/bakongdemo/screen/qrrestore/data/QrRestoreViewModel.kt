package kh.org.nbc.bakongdemo.screen.qrrestore.data

import androidx.lifecycle.MutableLiveData
import kh.org.nbc.bakongdemo.base.BaseViewModel
import kh.org.nbc.bakongdemo.bakongcore.rxschedulers.RxSchedulers
import kh.org.nbc.bakongdemo.screen.qrrecovery.repository.QrRecoveryRepository
import javax.crypto.BadPaddingException
import javax.inject.Inject

class QrRestoreViewModel @Inject constructor(
    private val rxSchedulers: RxSchedulers,
    private val qrRecoveryRepository: QrRecoveryRepository
): BaseViewModel() {

    val restoreSuccess = MutableLiveData<Boolean>()
    val stateError = MutableLiveData<Boolean>()


    fun restoreAccount(password: String, encryptedString: String){
        unsubscribeOnDestroy(qrRecoveryRepository.restoreAccount(password, encryptedString)
            .subscribeOn(rxSchedulers.io())
            .observeOn(rxSchedulers.main())
            .doOnSubscribe {

                _loading.postValue(true)

            }
            .doFinally {
                _loading.postValue(false)

            }
            .subscribe ({success ->
                if (success){
                    restoreSuccess.postValue(true)
                }else{
                    restoreSuccess.postValue(false)
                }

            },{
                if (it is BadPaddingException) stateError.postValue(true) else showError(it)
            })
        )
    }

}