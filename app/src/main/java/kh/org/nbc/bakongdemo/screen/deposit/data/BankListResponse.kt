package kh.org.nbc.bakongdemo.screen.deposit.data

import com.google.gson.annotations.SerializedName

class BankListResponse {
    @SerializedName("bin")
    var bin: String? =  null
    @SerializedName("participantCode")
    var participantCode: String? =  null
    @SerializedName("accountId")
    var accountId: String? =  null
    @SerializedName("logoUuid")
    var logoUuid: String? =  null

    override fun toString(): String {
        return "BankListResponse(bin=$bin, participantCode=$participantCode, accountId=$accountId, logoUuid=$logoUuid)"
    }

}