package kh.org.nbc.bakongdemo.screen.registration.data.mapper

import kh.org.nbc.bakongdemo.network.data.mapper.BaseServerMapper
import kh.org.nbc.bakongdemo.screen.registration.data.AccountCheck
import kh.org.nbc.bakongdemo.screen.registration.data.LoginStatusModel
import javax.inject.Inject

class AccountCheckToLoginStatusModelMapper @Inject constructor() : BaseServerMapper<AccountCheck, LoginStatusModel>() {

    override fun map(item: AccountCheck): LoginStatusModel {
        return LoginStatusModel(item.isFree)
    }
}