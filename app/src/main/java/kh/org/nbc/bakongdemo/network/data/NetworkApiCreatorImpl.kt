package kh.org.nbc.bakongdemo.network.data

import retrofit2.Retrofit

class NetworkApiCreatorImpl constructor(private var retrofit: Retrofit):
    NetworkApiCreator {

    override fun <T : Any?> create(service: Class<T>?): T {
        return retrofit.create(service)
    }
}
