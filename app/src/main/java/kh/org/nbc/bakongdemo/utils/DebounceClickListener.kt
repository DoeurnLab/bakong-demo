package kh.org.nbc.bakongdemo.utils

import android.view.View

abstract class DebounceClickListener(private val minimumIntervalMillis: Int) : View.OnClickListener {
    private var lastClickedTime = 0L

    abstract fun onDebouncedClick(v: View)

    override fun onClick(clickedView: View) {
        val currentTimestamp = System.currentTimeMillis()
        if (currentTimestamp - lastClickedTime > minimumIntervalMillis) {
            lastClickedTime = currentTimestamp
            onDebouncedClick(clickedView)
        }
    }
}

fun View.setOnDebounceClickListener(timeout: Int = 500, listener: (View) -> Unit) {
    setOnClickListener(
        object : DebounceClickListener(timeout) {
            override fun onDebouncedClick(v: View) {
                listener.invoke(v)
            }
        }
    )
}