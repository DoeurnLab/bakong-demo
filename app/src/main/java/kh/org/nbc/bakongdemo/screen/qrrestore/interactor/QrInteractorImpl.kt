package kh.org.nbc.bakongdemo.screen.qrrestore.interactor

import android.net.Uri
import kh.org.nbc.bakongdemo.screen.qrrecovery.repository.QrRepository
import kh.org.nbc.bakongdemo.screen.qrrestore.interactor.QrInteractor
import javax.inject.Inject

class QrInteractorImpl @Inject constructor(private val qrRepository: QrRepository) :
    QrInteractor {

    override fun encode(text: String): String {
        return qrRepository.encode(text)
    }

    override fun decode(path: String): String {
        return qrRepository.decode(path)
    }

    override fun decode(uri: Uri): String? {
        return qrRepository.decode(uri)
    }
}