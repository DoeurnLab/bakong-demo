package kh.org.nbc.bakongdemo.screen.login.data.response

import com.google.gson.annotations.SerializedName

data class CreateRecoveryData(
    @SerializedName("grantPermissionRecoveryToBankReducedHash")
    val grantPermissionRecoveryToBankReducedHash: String,
    @SerializedName("addSignatoryChangeQuorumReducedHash")
    val addSignatoryChangeQuorumReducedHash: String,
    @SerializedName("createRecoveryAccountTrxHash")
    val createRecoveryAccountTrxHash: String
)