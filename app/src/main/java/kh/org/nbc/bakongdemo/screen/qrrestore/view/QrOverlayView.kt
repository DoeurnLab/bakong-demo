package kh.org.nbc.bakongdemo.screen.qrrestore.view

import android.content.Context
import android.graphics.Bitmap
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.graphics.PorterDuff
import android.graphics.PorterDuffXfermode
import android.graphics.RectF
import android.util.AttributeSet
import android.widget.LinearLayout
import androidx.core.content.ContextCompat
import kh.org.nbc.bakongdemo.R as coreUI

class QrOverlayView : LinearLayout {
    private var bitmap: Bitmap? = null

    private val coef = 330
    private val radius = 30F

    constructor(context: Context) : super(context)

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs)

    constructor(context: Context, attrs: AttributeSet, defStyleAttr: Int) : super(context, attrs, defStyleAttr)

    override fun dispatchDraw(canvas: Canvas) {
        super.dispatchDraw(canvas)

        if (bitmap == null) {
            createWindowFrame()
        }

        canvas.drawBitmap(bitmap!!, 0.0F, 0.0F, null)
    }

    private fun createWindowFrame() {
        bitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888)
        val paint = Paint(Paint.ANTI_ALIAS_FLAG)
        val osCanvas = Canvas(bitmap!!)

        val outerRectangle = RectF(0f, 0f, width.toFloat(), height.toFloat())

        paint.color = ContextCompat.getColor(context, coreUI.color.black)
        paint.alpha = 199
        osCanvas.drawRect(outerRectangle, paint)

        paint.color = Color.TRANSPARENT
        paint.xfermode = PorterDuffXfermode(PorterDuff.Mode.SRC_OUT)

        val rect = RectF(width / 2F - coef,
                height / 2F - coef,
                width / 2F + coef,
                height / 2F + coef)

        osCanvas.drawRoundRect(
                rect, radius, radius, paint)

        // TODO add corner lines
//        paint.color = Color.GRAY
//        paint.setStrokeWidth(2F)
//        paint.style = Paint.Style.STROKE

//        val path = Path()
// //        path.moveTo(50F, 50F)
// //        path.lineTo(50F, 500F)
// //        path.lineTo(200F, 500F)
//        path.moveTo(rect.left, rect.left)
//        //  path.lineTo(50F, height / 2F - coef)
//        path.lineTo(rect.right , rect.right)
//
//        val radius = 50.0f
//
//        val cornerPathEffect = CornerPathEffect(radius)
//
//        paint.pathEffect = cornerPathEffect
//        osCanvas.drawPath(path, paint)
    }

    override fun isInEditMode(): Boolean {
        return true
    }

    override fun onLayout(changed: Boolean, l: Int, t: Int, r: Int, b: Int) {
        super.onLayout(changed, l, t, r, b)
        bitmap = null
    }
}
