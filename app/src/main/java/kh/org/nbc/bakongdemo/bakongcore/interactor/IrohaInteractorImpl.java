package kh.org.nbc.bakongdemo.bakongcore.interactor;

import androidx.annotation.NonNull;



import javax.inject.Inject;

import io.reactivex.Single;
import kh.org.nbc.bakongdemo.bakongcore.repository.IrohaRepository;
import kh.org.nbc.bakongdemo.utils.GrantEditTransaction;
import kh.org.nbc.bakongdemo.utils.GrantPermissionTransaction;

public class IrohaInteractorImpl implements IrohaInteractor {

    private final IrohaRepository mIrohaRepository;

    @Inject
    IrohaInteractorImpl(@NonNull IrohaRepository irohaRepository) {
        mIrohaRepository = irohaRepository;
    }

    @NonNull
    @Override
    public Single<GrantEditTransaction> grantEditPermissionTransaction(@NonNull String accountId) {
        return mIrohaRepository.createGrantEditSignatoryTransactionHash(accountId);
    }

    @NonNull
    @Override
    public Single<byte[]> grantPermission(@NonNull GrantPermissionTransaction grantPermissionTransaction) {
        return mIrohaRepository.grantPermission(grantPermissionTransaction);
    }

}
