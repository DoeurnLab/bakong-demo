package kh.org.nbc.bakongdemo.di.module;

import dagger.Binds;
import dagger.Module;
import kh.org.nbc.bakongdemo.screen.changephone.data.ChangePhoneRepository;
import kh.org.nbc.bakongdemo.screen.changephone.data.ChangePhoneRepositoryImpl;

@Module
public abstract class ChangePhoneModule {


    @Binds
    abstract ChangePhoneRepository provideChangePhoneRepository(ChangePhoneRepositoryImpl changePhoneRepository);
}
