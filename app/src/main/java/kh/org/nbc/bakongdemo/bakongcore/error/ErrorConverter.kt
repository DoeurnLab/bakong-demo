package kh.org.nbc.bakongdemo.bakongcore.error

import kh.org.nbc.bakongdemo.bakongcore.alert.AlertType
import kh.org.nbc.bakongdemo.R
import kh.org.nbc.bakongdemo.bakongcore.alert.Alert
import kh.org.nbc.bakongdemo.bakongcore.exception.ServerException
import kh.org.nbc.bakongdemo.network.data.mapper.OneWayMapper
import java.net.SocketTimeoutException
import java.net.UnknownHostException

class ErrorConverter : OneWayMapper<Throwable, Alert>() {

    companion object {
        private const val UNEXPECTED_ERROR = "Unexpected error"
    }

    override fun map(item: Throwable): Alert {
        return when {
            item is ServerException -> item.alert ?: Alert(
                description = item.message
                    ?: UNEXPECTED_ERROR, exception = item
            )
            isInternetConnectionError(item) -> Alert(
                descriptionRes = R.string.no_internet_connection,
                exception = item
            )
            else -> Alert(
                descriptionRes = R.string.something_went_wrong,
                type = AlertType.TOAST,
                exception = item
            )
        }
    }

    private fun isInternetConnectionError(throwable: Throwable) =
        throwable is UnknownHostException ||
                throwable is SocketTimeoutException
}