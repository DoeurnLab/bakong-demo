package kh.org.nbc.bakongdemo.utils

data class GrantEditTransaction(
    val transactionHash: String,
    val transaction: ByteArray
) {
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as GrantEditTransaction

        if (transactionHash != other.transactionHash) return false
        if (!transaction.contentEquals(other.transaction)) return false

        return true
    }

    override fun hashCode(): Int {
        var result = transactionHash.hashCode()
        result = 31 * result + transaction.contentHashCode()
        return result
    }
}