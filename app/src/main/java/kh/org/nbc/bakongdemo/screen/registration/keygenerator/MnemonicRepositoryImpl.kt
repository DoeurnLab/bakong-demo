package kh.org.nbc.bakongdemo.screen.registration.keygenerator

import io.github.novacrypto.bip39.MnemonicGenerator
import io.github.novacrypto.bip39.wordlists.English
import io.reactivex.Single
import org.spongycastle.crypto.generators.SCrypt
import java.security.SecureRandom
import java.text.Normalizer
import javax.inject.Inject

class MnemonicRepositoryImpl @Inject constructor():
    MnemonicRepository {
    override fun createMnemonic(): Single<String> {
        val sb = StringBuilder()
        MnemonicGenerator(English.INSTANCE)
            .createMnemonic(SecureRandom().generateSeed(20)) { sb.append(it) }
        return Single.fromCallable {
            sb.toString()
        }
    }

    override fun entropyFromMnemonic(mnemonic: String): ByteArray {
        return Normalizer.normalize(mnemonic, Normalizer.Form.NFKD).toByteArray(charset("UTF-8"))
    }

    override fun deriveSeedFromMnemonic(
        mnemonic: String,
        password: String,
        project: String,
        purpose: String,
        keyLength: Int
    ): ByteArray {
        val salt = StringBuilder()
                .append(project)
                .append("|")
                .append(purpose)
                .append("|")
                .append(password)
                .toString()

        return SCrypt.generate(entropyFromMnemonic(mnemonic), salt.toByteArray(charset("UTF-8")), 16384, 8, 1, keyLength)
    }
}