package kh.org.nbc.bakongdemo.utils

import android.text.InputType
import java.io.Serializable

enum class Currency(
    val scale: Int,
    val sign: String,
    val isoCode: String
) : Serializable {
    USD(2, "$", "840"),
    KHR(0, "៛", "116");
}

fun String.toIsoCurrency() = Currency.values().first { it.isoCode == this }

fun Currency.getType() = when (this) {
    Currency.USD -> InputType.TYPE_CLASS_NUMBER or InputType.TYPE_NUMBER_FLAG_DECIMAL
    Currency.KHR -> InputType.TYPE_CLASS_NUMBER
}
