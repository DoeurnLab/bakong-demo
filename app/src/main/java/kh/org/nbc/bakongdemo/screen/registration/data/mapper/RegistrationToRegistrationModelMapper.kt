package kh.org.nbc.bakongdemo.screen.registration.data.mapper

import kh.org.nbc.bakongdemo.network.data.mapper.BaseServerMapper
import kh.org.nbc.bakongdemo.screen.registration.data.Registration
import kh.org.nbc.bakongdemo.screen.registration.data.RegistrationModel
import javax.inject.Inject

class RegistrationToRegistrationModelMapper @Inject constructor() :
    BaseServerMapper<Registration, RegistrationModel>() {

    override fun map(item: Registration): RegistrationModel {
        return RegistrationModel(item.isSuccess, item.loginOptions ?: emptyList(), item.bankDomain)
    }
}