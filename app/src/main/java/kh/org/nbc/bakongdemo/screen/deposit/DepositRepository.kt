package kh.org.nbc.bakongdemo.screen.deposit

import io.reactivex.Single
import kh.org.nbc.bakongdemo.screen.deposit.data.*

interface DepositRepository {
    fun getDepositBankList(): Single<List<BankListResponse>>
    fun accountCheck(accountNumber: String, participantCode: String): Single<AccountCheckResponse>
    fun deposit(depositPaymentData: DepositPaymentData): Single<DepositResponse>
}