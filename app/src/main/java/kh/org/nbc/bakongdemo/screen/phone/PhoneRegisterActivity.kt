package kh.org.nbc.bakongdemo.screen.phone

import android.content.Intent
import android.os.Bundle
import android.text.TextUtils
import androidx.lifecycle.Observer
import kh.org.nbc.bakongdemo.application.BakongApplication
import kh.org.nbc.bakongdemo.base.BaseActivity
import kh.org.nbc.bakongdemo.R
import kh.org.nbc.bakongdemo.screen.verification.VerificationActivity
import kh.org.nbc.bakongdemo.utils.gone
import kh.org.nbc.bakongdemo.utils.show
import kotlinx.android.synthetic.main.activity_phone_register.*
import kotlinx.android.synthetic.main.layout_toolbar.*
import javax.inject.Inject

class PhoneRegisterActivity : BaseActivity() {

    @Inject
    lateinit var viewModel: PhoneRegisterViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        (application as BakongApplication).appComponent.inject(this)
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_phone_register)

        viewModel.loading.observe(this, Observer {
            if (it) {
                progressBar.show()
            } else {
                progressBar.gone()
            }
        })

        viewModel.requestIdModel.observe(this, Observer {
            startActivity(Intent(this, VerificationActivity::class.java))
            finish()
        })

        viewModel.error.observe(this, Observer {
            onError(it)
        })

        text_view_title.text = resources.getString(R.string.phone_number_register)
        button_back.show()
        button_next.show()

        button_back.setOnClickListener {
            finish()
        }
        button_next.setOnClickListener {
            if (TextUtils.isEmpty(edit_text_phone_number.text.toString())) {
                edit_text_phone_number.error = resources.getString(R.string.phone_number_is_requred)
            }else{
                viewModel.registerPhoneNumber(edit_text_phone_number.text.toString())
            }

        }

    }

}


