package kh.org.nbc.bakongdemo.screen.changephone

import androidx.lifecycle.MutableLiveData
import kh.org.nbc.bakongdemo.base.BaseViewModel
import kh.org.nbc.bakongdemo.bakongcore.rxschedulers.RxSchedulers
import kh.org.nbc.bakongdemo.screen.registration.repository.RegistrationRepository
import kh.org.nbc.bakongdemo.storage.Storage
import javax.inject.Inject

class ChangePhoneViewModel @Inject constructor(
    private val rxSchedulers: RxSchedulers,
    private val storage: Storage,
    private val registrationRepository: RegistrationRepository
)  : BaseViewModel() {

    var success = MutableLiveData<Boolean>()

    fun changePhone(phone: String){
        storage.setPhoneNumber(phone)
        unsubscribeOnDestroy(registrationRepository.changePhone(phone)
            .subscribeOn(rxSchedulers.io())
            .observeOn(rxSchedulers.main())
            .doOnSubscribe {
                _loading.postValue(true)
            }
            .doFinally {
                _loading.postValue(false)
            }
            .subscribe ({
                storage.setRequestId(it.requestId.toString())
                success.postValue(true)
            },{
                showError(it)
            })
        )
    }

    fun verify(code: String){
        unsubscribeOnDestroy(registrationRepository.verifyChangePhone(code)
            .subscribeOn(rxSchedulers.io())
            .observeOn(rxSchedulers.main())
            .doOnSubscribe {
                _loading.postValue(true)
            }
            .doFinally {
                _loading.postValue(false)
            }
            .subscribe ({
                success.postValue(it)
            },{
                showError(it)
            })
        )
    }

    fun onResendCode() {
        unsubscribeOnDestroy(registrationRepository.registerPhone(storage.getPhoneNumber()!!)
            .subscribeOn(rxSchedulers.io())
            .observeOn(rxSchedulers.main())
            .doOnSubscribe {
                _loading.value = true
            }
            .doFinally {
                _loading.value = false
            }
            .subscribe({
            }, {
                showError(it)
            })
        )
    }


}