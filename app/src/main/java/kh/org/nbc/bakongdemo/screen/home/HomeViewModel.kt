package kh.org.nbc.bakongdemo.screen.home

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import kh.org.nbc.bakongdemo.bakongcore.rxschedulers.RxSchedulers
import kh.org.nbc.bakongdemo.base.BaseViewModel
import kh.org.nbc.bakongdemo.screen.home.data.TransactionData
import kh.org.nbc.bakongdemo.screen.home.data.TransactionRequest
import kh.org.nbc.bakongdemo.utils.Currency
import kh.org.nbc.bakongdemo.utils.Money
import java.math.BigDecimal
import javax.inject.Inject

class HomeViewModel @Inject constructor(
    private val rxSchedulers: RxSchedulers,
    private val homeRepository: HomeRepository
) : BaseViewModel() {

    private val _accountRiel = MutableLiveData<String>()
    val accountRiel: LiveData<String> get() = _accountRiel

    private val _accountUsd = MutableLiveData<String>()
    val accountUsd: LiveData<String> get() = _accountUsd

    private val _transactionData = MutableLiveData<TransactionData>()
    val transactionData: LiveData<TransactionData> get() = _transactionData

    fun getBalance() {
        unsubscribeOnDestroy(
            homeRepository.getBalance()
                .subscribeOn(rxSchedulers.io())
                .observeOn(rxSchedulers.main())
                .doOnSubscribe { _loading.postValue(true) }
                .doFinally { _loading.postValue(false) }
                .subscribe({
                    val asset = it.asset
                    val riel = Money(parseAmount(asset.amountKhr), Currency.KHR)
                    val usd = Money(parseAmount(asset.amountUsd), Currency.USD)
                    _accountRiel.postValue("${riel.amount}")
                    _accountUsd.postValue("${usd.amount}")
                }, { throwable ->
                    showError(throwable)
                })
        )
    }


    fun getTransaction(request: TransactionRequest){
        unsubscribeOnDestroy(
            homeRepository.getTransactions(request)
                .subscribeOn(rxSchedulers.io())
                .observeOn(rxSchedulers.main())
                /*.doOnSubscribe { _loading.postValue(true) }
                .doFinally { _loading.postValue(false) }*/
                .subscribe({
                    _transactionData.postValue(it)
                }, { throwable ->
                    showError(throwable)
                })
        )
    }

    private fun parseAmount(amount: String?): BigDecimal {
        return BigDecimal.valueOf(
            if (amount.isNullOrEmpty()) 0.0 else java.lang.Double.parseDouble(amount)
        )
    }
}