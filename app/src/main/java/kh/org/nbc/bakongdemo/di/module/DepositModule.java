package kh.org.nbc.bakongdemo.di.module;


import dagger.Binds;
import dagger.Module;
import kh.org.nbc.bakongdemo.screen.deposit.DepositRepository;
import kh.org.nbc.bakongdemo.screen.deposit.DepositRepositoryImpl;

@Module
public abstract class DepositModule {

    @Binds
    abstract DepositRepository provideDepositRepository(DepositRepositoryImpl sendRepository);


}
