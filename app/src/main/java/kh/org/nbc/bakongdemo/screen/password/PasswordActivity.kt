package kh.org.nbc.bakongdemo.screen.password

import android.content.Intent
import android.os.Bundle
import kh.org.nbc.bakongdemo.base.BaseActivity
import kh.org.nbc.bakongdemo.R
import kh.org.nbc.bakongdemo.application.BakongApplication
import kh.org.nbc.bakongdemo.screen.password.validate.PasscodeValidator
import kh.org.nbc.bakongdemo.screen.password.validate.PasswordView
import kh.org.nbc.bakongdemo.screen.password.validate.ValidationResult
import kh.org.nbc.bakongdemo.screen.qrrecovery.QrRecoveryActivity
import kh.org.nbc.bakongdemo.storage.Storage
import kh.org.nbc.bakongdemo.utils.show
import kotlinx.android.synthetic.main.activity_password.*
import kotlinx.android.synthetic.main.layout_toolbar.text_view_title
import kotlinx.android.synthetic.main.layout_toolbar.button_back
import javax.inject.Inject

class PasswordActivity : BaseActivity(),
    PasswordView {

    @Inject
    lateinit var storage: Storage

    @Inject
    lateinit var passcodeValidator: PasscodeValidator

    override fun onCreate(savedInstanceState: Bundle?) {

        (application as BakongApplication).appComponent.inject(this)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_password)


        button_next_screen.setOnClickListener {
            onPasswordFilled(
                edit_text_password.text.toString(),
                edit_text_confirm_password.text.toString()
            )
        }
        text_view_title.text = resources.getString(R.string.password)
        button_back.show()
        button_back.setOnClickListener {
            finish()
        }
    }

    private fun onPasswordFilled(password: String, confirmPassword: String) {
        val validationResult = passcodeValidator.validate(password)
        if (validationResult == ValidationResult.LENGTH_LESS_ERROR) {
            showPasswordLengthError()
            enableButton(true)
        } else if (validationResult == ValidationResult.PATTERN_MATCH_ERROR) {
            showStrongPolicyError()
            enableButton(true)
        } else if (!password.contentEquals(confirmPassword)) {
            showPasswordDidNotMatchError()
            enableButton(true)
        } else {
            storage.setPassword(password)
            startActivity(Intent(this, QrRecoveryActivity::class.java))
            finish()
        }
    }

    override fun showPasswordLengthError() {
        edit_text_password.error =
            getString(R.string.registration_password_Password_length_error)
    }

    override fun showPasswordDidNotMatchError() {
        edit_text_confirm_password.error =
            getString(R.string.registration_password_did_not_match)
    }

    override fun showStrongPolicyError() {
        edit_text_password.error =
            getString(R.string.registration_password_Password_strong_policy_error)
    }

    override fun enableButton(flag: Boolean) {
        button_next_screen.isClickable = flag
        button_next_screen.isEnabled = flag
    }


}