package kh.org.nbc.bakongdemo.screen.username.data;

import com.google.gson.annotations.SerializedName;

import java.util.Objects;

public class AccountRequest {

    @SerializedName("devId")
    private String mDeviceId;
    @SerializedName("publicKey")
    private String mPublicKey;
    @SerializedName("alias")
    private String mLogin;

    public String getDeviceId() {
        return mDeviceId;
    }

    public void setDeviceId(String deviceId) {
        mDeviceId = deviceId;
    }

    public String getPublicKey() {
        return mPublicKey;
    }

    public void setPublicKey(String publicKey) {
        mPublicKey = publicKey;
    }

    public String getLogin() {
        return mLogin;
    }

    public void setLogin(String login) {
        mLogin = login;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        AccountRequest that = (AccountRequest) o;
        return Objects.equals(mDeviceId, that.mDeviceId) &&
                Objects.equals(mPublicKey, that.mPublicKey) &&
                Objects.equals(mLogin, that.mLogin);
    }

    @Override
    public int hashCode() {
        return Objects.hash(mDeviceId, mPublicKey, mLogin);
    }

    @Override
    public String toString() {
        return "AccountRequest{" +
                "mDeviceId='" + mDeviceId + '\'' +
                ", mPublicKey='" + mPublicKey + '\'' +
                ", mLogin='" + mLogin + '\'' +
                '}';
    }
}
