package kh.org.nbc.bakongdemo.screen.qrrestore.interactor

import android.app.Activity
import androidx.fragment.app.Fragment

interface QrLauncher {
    companion object {
        const val SCAN_ERROR = "scan_error"
    }

    fun launch(activity: Activity)

    fun launch(fragment: Fragment)
}