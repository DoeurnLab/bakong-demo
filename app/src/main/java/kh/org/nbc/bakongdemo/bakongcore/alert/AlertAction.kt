package kh.org.nbc.bakongdemo.bakongcore.alert

import android.os.Parcelable
import androidx.fragment.app.DialogFragment

abstract class AlertAction(val descriptionRes: Int? = null) : Parcelable {

    abstract fun onAction(dialogFragment: DialogFragment)
}