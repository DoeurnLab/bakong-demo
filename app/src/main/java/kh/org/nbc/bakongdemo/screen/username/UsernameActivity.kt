package kh.org.nbc.bakongdemo.screen.username

import android.content.Intent
import android.os.Bundle
import android.text.TextUtils
import androidx.lifecycle.Observer
import com.jakewharton.rxbinding.widget.RxTextView
import kh.org.nbc.bakongdemo.R
import kh.org.nbc.bakongdemo.application.BakongApplication
import kh.org.nbc.bakongdemo.base.BaseActivity
import kh.org.nbc.bakongdemo.screen.password.PasswordActivity
import kh.org.nbc.bakongdemo.screen.verification.VerificationActivity
import kh.org.nbc.bakongdemo.utils.hide
import kh.org.nbc.bakongdemo.utils.show
import kotlinx.android.synthetic.main.activity_username.*
import kotlinx.android.synthetic.main.activity_verification.progressBar
import kotlinx.android.synthetic.main.layout_toolbar.*
import kotlinx.android.synthetic.main.layout_toolbar.button_back
import kotlinx.android.synthetic.main.layout_toolbar.text_view_title
import java.util.concurrent.TimeUnit
import javax.inject.Inject

class UsernameActivity : BaseActivity() {

    @Inject
    lateinit var viewModel: UsernameViewModel

    private var userInputted = false

    private var username: String? = null
    private var usernameAvailable = false


    override fun onCreate(savedInstanceState: Bundle?) {

        (application as BakongApplication).appComponent.inject(this)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_username)

        username = intent.getStringExtra(VerificationActivity.SUGGESTION_USERNAME)
        if (!TextUtils.isEmpty(username)) {
            edit_text_username.setText(username)
            usernameAvailable = true
        }

        text_view_title.text = resources.getString(R.string.username_creation)
        button_back.show()
        button_next.show()
        button_next.setOnClickListener {
            if (!TextUtils.isEmpty(username) && usernameAvailable)
                viewModel.createUsername(username!!)
        }
        button_back.setOnClickListener {
            finish()
        }

        viewModel.accountCreationModel.observe(this, Observer {
            startActivity(Intent(this, PasswordActivity::class.java))
            finish()
        })

        viewModel.usernameAvailable.observe(this, Observer {
            if (it) {
                edit_text_username.setTextColor(getColor(R.color.kelley_green))
                username = edit_text_username.text.toString()
            } else {
                edit_text_username.setTextColor(getColor(R.color.low_red))
            }
            usernameAvailable = it
        })

        RxTextView.textChanges(edit_text_username)
            .debounce(1000, TimeUnit.MILLISECONDS)
            .subscribe { username ->
                if (userInputted) {
                    if (!TextUtils.isEmpty(username)) {
                        viewModel.checkUsername(username.toString())
                        usernameAvailable = false
                    }
                } else {
                    userInputted = true
                }
            }

        RxTextView.beforeTextChangeEvents(edit_text_username)
            .subscribe {
                edit_text_username.setTextColor(getColor(R.color.black))
            }

        viewModel.error.observe(this, Observer {
            onError(it)
        })

        viewModel.loading.observe(this, Observer {
            if (it)
                progressBar.show()
            else
                progressBar.hide()
        })

    }

}