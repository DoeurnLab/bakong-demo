package kh.org.nbc.bakongdemo.bakongcore.repository

import androidx.core.util.Pair
import io.reactivex.Single
import jp.co.soramitsu.crypto.ed25519.Ed25519Sha3
import jp.co.soramitsu.crypto.ed25519.EdDSAPrivateKey
import jp.co.soramitsu.crypto.ed25519.EdDSAPublicKey
import jp.co.soramitsu.crypto.ed25519.spec.EdDSAPublicKeySpec
import jp.co.soramitsu.iroha.java.Utils
import kh.org.nbc.bakongdemo.screen.qrrecovery.interactor.AesCbcCipher
import kh.org.nbc.bakongdemo.screen.qrrestore.utils.MessageSignature
import kh.org.nbc.bakongdemo.screen.registration.keygenerator.model.BakongKeyPair
import kh.org.nbc.bakongdemo.screen.registration.keygenerator.model.BakongKeyPairIroha
import kh.org.nbc.bakongdemo.storage.Storage
import org.spongycastle.util.encoders.DecoderException
import org.spongycastle.util.encoders.Hex
import java.security.KeyPair
import java.security.SecureRandom
import javax.crypto.BadPaddingException
import javax.inject.Inject

class BakongRepositoryImpl @Inject constructor(
    private val mEd25519Sha3: Ed25519Sha3,
    private var storage: Storage,
    private var mAesCbcCipher: AesCbcCipher,
    private var mSecureRandom: SecureRandom
) : BakongRepository {

    private val keysInternal: BakongKeyPair?
        get() {
            var keyPair: BakongKeyPair? = null
            val publicKey = storage.getPublicKey()
            val privateKey = storage.getPrivateKey()
            if (!publicKey.isNullOrEmpty() && !privateKey.isNullOrEmpty()) {
                keyPair = BakongKeyPairIroha(publicKey, privateKey)
            }
            return keyPair
        }

    override fun signMessage(message: String): Single<MessageSignature> {
        return Single.fromCallable { signMessageInternal(message) }
    }

    private fun signMessageInternal(message: String): MessageSignature {
        val bakongKeyPair = keysInternal
        val keyPair =
            Utils.parseHexKeypair(bakongKeyPair!!.getPublicKey(), bakongKeyPair.getPrivateKey())
        val messageBytes = message.toByteArray()
        val signature = mEd25519Sha3.rawSign(messageBytes, keyPair)
        return MessageSignature(
            message,
            messageBytes,
            signature
        )
    }


    private fun encryptKeyInternal(password: String): String {
        val salt = salt
        val encryptedKey = mAesCbcCipher.encrypt(salt, password, keyPair.private.encoded)
        return String.format("%s|%s", Hex.toHexString(salt), Hex.toHexString(encryptedKey))
    }

    private val salt: ByteArray
        get() {
            val salt = ByteArray(16)
            mSecureRandom.nextBytes(salt)
            return salt
        }

    private val keyPair: KeyPair
        get() {
            val bakongKeyPair = keysInternal
            return Utils.parseHexKeypair(
                bakongKeyPair!!.getPublicKey(),
                bakongKeyPair.getPrivateKey()
            )
        }


    override fun encryptKey(passcode: String): Single<String> {
        return Single.fromCallable { encryptKeyInternal(passcode) }
    }

    override fun decryptKey(passcode: String, content: String): Single<BakongKeyPair> {
        return Single
            .fromCallable { decryptKeyInternal(passcode, content) }
            .onErrorResumeNext { error ->
                // on ui layer we know only about java.crypto package. Here invalid
                // qr scan exception mapped to java.crypto.DecoderException.
                // This transformation leads to showing more natural error for user.
                // Check QrRestorePresenter for details
                if (error is DecoderException) {
                    Single.error(BadPaddingException())
                } else {
                    Single.error(error)
                }
            }
    }

    private fun decryptKeyInternal(passcode: String, content: String): BakongKeyPair {
        val saltAndEncryptedKey = parseSaltAndEncryptedKey(content)

        val encodedPrivateKey = mAesCbcCipher.decrypt(
            saltAndEncryptedKey.first!!,
            passcode,
            saltAndEncryptedKey.second!!
        )

        return getBakongKeyPairFromPrivateKey(encodedPrivateKey)
    }

    private fun parseSaltAndEncryptedKey(content: String): Pair<ByteArray, ByteArray> {
        val saltAndEncryptedKey =
            content.split("[|]".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
        val salt = Hex.decode(saltAndEncryptedKey[0])
        val encryptedKey = Hex.decode(saltAndEncryptedKey[1])
        return Pair.create(salt, encryptedKey)
    }

    private fun getBakongKeyPairFromPrivateKey(encodedPrivateKey: ByteArray): BakongKeyPair {
        val privateKey = Ed25519Sha3.privateKeyFromBytes(encodedPrivateKey) as EdDSAPrivateKey

        val publicKeySpec = EdDSAPublicKeySpec(privateKey.a, privateKey.params)

        val publicKey = EdDSAPublicKey(publicKeySpec)

        return BakongKeyPairIroha(
            Hex.toHexString(publicKey.encoded),
            Hex.toHexString(privateKey.encoded)
        )
    }


}