package kh.org.nbc.bakongdemo.di.module;

import dagger.Binds;
import dagger.Module;
import kh.org.nbc.bakongdemo.bakongcore.repository.BakongRepository;
import kh.org.nbc.bakongdemo.bakongcore.repository.BakongRepositoryImpl;
import kh.org.nbc.bakongdemo.screen.qrrestore.interactor.QrInteractor;
import kh.org.nbc.bakongdemo.screen.qrrestore.interactor.QrInteractorImpl;
import kh.org.nbc.bakongdemo.screen.qrrestore.interactor.QrLauncher;
import kh.org.nbc.bakongdemo.screen.qrrestore.interactor.QrLauncherImpl;

@Module
public abstract class QrRestoreModule  {

    @Binds
    abstract QrLauncher provideQrLauncher(QrLauncherImpl QrLauncher);

    @Binds
    abstract QrInteractor provideQrInteractor(QrInteractorImpl QrInteractor);

    @Binds
    abstract BakongRepository provideBakongRepository(BakongRepositoryImpl bakongRepository);


}
