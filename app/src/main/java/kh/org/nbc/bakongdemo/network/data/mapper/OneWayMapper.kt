package kh.org.nbc.bakongdemo.network.data.mapper

import java.util.*

abstract class OneWayMapper<T, R> {

    abstract fun map(item: T): R

    fun mapList(list: List<T>): List<R> {
        val convertedList = ArrayList<R>(list.size)
        for (item in list) {
            convertedList.add(map(item))
        }
        return convertedList
    }
}
