package kh.org.nbc.bakongdemo.screen.send.data

import com.google.gson.annotations.SerializedName

class SendPayment {
    @SerializedName("amount")
    var amount: String? = null

    @SerializedName("assetId")
    var assetId: String? = null

    @SerializedName("receiverAccountId")
    var receiverAccountId: String? = null

    @SerializedName("description")
    var description: String? = null

    @SerializedName("signByte")
    var signByte: ByteArray? = null

    @SerializedName("trxHash")
    var trxHash: String? = null

    @SerializedName("qrCode")
    var qrCode: String? = null

}