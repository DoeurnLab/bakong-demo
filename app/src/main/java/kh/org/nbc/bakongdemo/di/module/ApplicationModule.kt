package kh.org.nbc.bakongdemo.di.module

import dagger.Binds
import dagger.Module
import kh.org.nbc.bakongdemo.bakongcore.rxschedulers.RxSchedulers
import kh.org.nbc.bakongdemo.bakongcore.rxschedulers.RxSchedulersImpl
import javax.inject.Singleton


@Module
interface ApplicationModule {

    @Singleton
    @Binds
    fun provideRxScheduler(rxSchedulersImpl: RxSchedulersImpl): RxSchedulers

}