package kh.org.nbc.bakongdemo.screen.selectphone

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import kh.org.nbc.bakongdemo.bakongcore.rxschedulers.RxSchedulers
import kh.org.nbc.bakongdemo.base.BaseViewModel
import kh.org.nbc.bakongdemo.screen.send.SendRepository
import kh.org.nbc.bakongdemo.screen.send.data.InitializePaymentPhone
import javax.inject.Inject

class SelectPhoneViewModel @Inject constructor(
    private val rxSchedulers: RxSchedulers,
    private val repository: SendRepository
): BaseViewModel() {

    private val _initializePaymentPhone = MutableLiveData<InitializePaymentPhone>()
    val initializePaymentPhone: LiveData<InitializePaymentPhone> get() = _initializePaymentPhone

    fun initializePaymentPhone(phoneNumber: String){
        unsubscribeOnDestroy(
            repository.initializePaymentPhone(phoneNumber)
                .subscribeOn(rxSchedulers.io())
                .observeOn(rxSchedulers.main())
                .doOnSubscribe { _loading.postValue(true) }
                .doFinally { _loading.postValue(false) }
                .subscribe({
                    _initializePaymentPhone.postValue(it)
                }, { throwable ->
                    showError(throwable)
                })
        )
    }
}