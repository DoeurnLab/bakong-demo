package kh.org.nbc.bakongdemo.screen.registration.keygenerator

import io.reactivex.Single
import javax.inject.Inject


class MnemonicInteractorImpl @Inject constructor(
    private val mnemonicRepository: MnemonicRepository
) : MnemonicInteractor {

    companion object {
        private const val PASSWORD = ""
        private const val PROJECT = "Bakong"
        private const val PURPOSE = "iroha keypair"
        private const val KEY_LENGTH = 32
    }

    override fun createMnemonic(): Single<String> {
        return mnemonicRepository.createMnemonic()
    }

    override fun entropyFromMnemonic(mnemonic: String): ByteArray {
        return mnemonicRepository.entropyFromMnemonic(mnemonic)
    }

    override fun deriveSeedFromMnemonic(mnemonic: String): ByteArray {
        return mnemonicRepository.deriveSeedFromMnemonic(mnemonic,
            PASSWORD,
            PROJECT,
            PURPOSE,
            KEY_LENGTH
        )
    }
}