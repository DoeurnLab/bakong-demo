package kh.org.nbc.bakongdemo.screen.qrrestore.interactor

import android.app.Activity
import androidx.fragment.app.Fragment
import com.google.zxing.integration.android.IntentIntegrator
import kh.org.nbc.bakongdemo.screen.qrrestore.view.CustomQrActivity
import javax.inject.Inject

class QrLauncherImpl @Inject constructor() :
    QrLauncher {
    override fun launch(activity: Activity) {
        IntentIntegrator(activity).configure().initiateScan()
    }

    override fun launch(fragment: Fragment) {
        IntentIntegrator.forSupportFragment(fragment).configure().initiateScan()
    }

    private fun IntentIntegrator.configure(): IntentIntegrator = setOrientationLocked(false)
        .setBarcodeImageEnabled(false)
        .setCaptureActivity(CustomQrActivity::class.java)
        .setDesiredBarcodeFormats(IntentIntegrator.QR_CODE_TYPES)
}