package kh.org.nbc.bakongdemo.utils

data class GrantPermissionTransaction(
    val transaction: ByteArray,
    val grandPermissionHash: String,
    val addSignatoryHash: String
) {
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as GrantPermissionTransaction

        if (!transaction.contentEquals(other.transaction)) return false
        if (grandPermissionHash != other.grandPermissionHash) return false
        if (addSignatoryHash != other.addSignatoryHash) return false

        return true
    }

    override fun hashCode(): Int {
        var result = transaction.contentHashCode()
        result = 31 * result + grandPermissionHash.hashCode()
        result = 31 * result + addSignatoryHash.hashCode()
        return result
    }
}