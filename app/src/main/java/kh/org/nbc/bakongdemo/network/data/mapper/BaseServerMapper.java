package kh.org.nbc.bakongdemo.network.data.mapper;

import androidx.annotation.NonNull;

import kh.org.nbc.bakongdemo.base.BaseResponse;

public abstract class BaseServerMapper<T, R> extends Mapper<T, R> {

    public R mapResponse(@NonNull BaseResponse<T> item) {
        return map(item.getData());
    }

    @Override
    public T reverse(@NonNull R item) {
        throw new UnsupportedOperationException();
    }

}
