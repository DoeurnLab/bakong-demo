package kh.org.nbc.bakongdemo.base

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.orhanobut.logger.Logger
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import kh.org.nbc.bakongdemo.bakongcore.alert.Alert
import kh.org.nbc.bakongdemo.bakongcore.error.ErrorConverter

open class BaseViewModel: ViewModel(){

    private val _error = MutableLiveData<Alert>()
    val error: LiveData<Alert> = _error
    private val compositeDisposable = CompositeDisposable()
    private val errorConverter =
        ErrorConverter()

    override fun onCleared() {
        compositeDisposable.clear()
        super.onCleared()
    }
    protected val _loading = MutableLiveData<Boolean>()
    val loading : LiveData<Boolean> get() = _loading

    protected fun unsubscribeOnDestroy(disposable: Disposable) {
        compositeDisposable.add(disposable)
    }

    protected fun showError(throwable: Throwable) {
        val alert = errorConverter.map(throwable)
        _error.postValue(alert)
        Logger.e(throwable, throwable.localizedMessage ?: "")
    }


}