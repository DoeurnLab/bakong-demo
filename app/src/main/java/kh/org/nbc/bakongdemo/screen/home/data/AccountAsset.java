package kh.org.nbc.bakongdemo.screen.home.data;

import com.google.gson.annotations.SerializedName;

import java.util.Objects;

public class AccountAsset {

    @SerializedName("accountAsset")
    private Asset mAsset;

    public Asset getAsset() {
        return mAsset;
    }

    public void setAsset(Asset asset) {
        mAsset = asset;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        AccountAsset that = (AccountAsset) o;
        return Objects.equals(mAsset, that.mAsset);
    }

    @Override
    public int hashCode() {
        return Objects.hash(mAsset);
    }

    @Override
    public String toString() {
        return "AccountAsset{" +
                "mAsset=" + mAsset +
                '}';
    }

    public static class Asset {
        @SerializedName("inqCurAccUsd")
        private String mUsdCurrencyId;
        @SerializedName("inqCurAccKhr")
        private String mKhrCurrencyId;
        @SerializedName("inqCurBalUsd")
        private String mAmountUsd;
        @SerializedName("inqCurBalKhr")
        private String mAmountKhr;

        public String getUsdCurrencyId() {
            return mUsdCurrencyId;
        }

        public void setUsdCurrencyId(String usdCurrencyId) {
            mUsdCurrencyId = usdCurrencyId;
        }

        public String getKhrCurrencyId() {
            return mKhrCurrencyId;
        }

        public void setKhrCurrencyId(String khrCurrencyId) {
            mKhrCurrencyId = khrCurrencyId;
        }

        public String getAmountUsd() {
            return mAmountUsd;
        }

        public void setAmountUsd(String amountUsd) {
            mAmountUsd = amountUsd;
        }

        public String getAmountKhr() {
            return mAmountKhr;
        }

        public void setAmountKhr(String amountKhr) {
            mAmountKhr = amountKhr;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            Asset that = (Asset) o;
            return Objects.equals(mUsdCurrencyId, that.mUsdCurrencyId) &&
                    Objects.equals(mKhrCurrencyId, that.mKhrCurrencyId) &&
                    Objects.equals(mAmountUsd, that.mAmountUsd) &&
                    Objects.equals(mAmountKhr, that.mAmountKhr);
        }

        @Override
        public int hashCode() {
            return Objects.hash(mUsdCurrencyId, mKhrCurrencyId, mAmountUsd, mAmountKhr);
        }

        @Override
        public String toString() {
            return "AccountAsset{" +
                    "mUsdCurrencyId='" + mUsdCurrencyId + '\'' +
                    ", mKhrCurrencyId='" + mKhrCurrencyId + '\'' +
                    ", mAmountUsd='" + mAmountUsd + '\'' +
                    ", mAmountKhr='" + mAmountKhr + '\'' +
                    '}';
        }
    }
}
