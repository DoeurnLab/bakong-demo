package kh.org.nbc.bakongdemo.utils

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize
import java.math.BigDecimal

@Parcelize
data class Money(
    val amount: BigDecimal = BigDecimal.ZERO,
    val currency: Currency = Currency.USD
) : Parcelable

fun Money.formatted() = when (currency) {
    Currency.USD -> String.format("%s%.2f", currency.sign, amount)
    Currency.KHR -> String.format("%s%.0f", currency.sign, amount)
}


