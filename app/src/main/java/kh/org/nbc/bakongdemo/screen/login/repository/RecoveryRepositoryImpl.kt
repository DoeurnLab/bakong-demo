package kh.org.nbc.bakongdemo.screen.login.repository

import io.reactivex.Single
import kh.org.nbc.bakongdemo.screen.login.data.request.CreateRecoveryAccountRequest
import kh.org.nbc.bakongdemo.screen.login.data.request.GrantRecoveryAccountRequest
import kh.org.nbc.bakongdemo.screen.login.data.response.CreateRecoveryData
import kh.org.nbc.bakongdemo.screen.login.data.response.GrantRecoveryData
import kh.org.nbc.bakongdemo.screen.registration.api.BakongApi
import javax.inject.Inject

class RecoveryRepositoryImpl @Inject constructor(
    private val api: BakongApi
) : RecoveryRepository {

    override fun createRecoveryAccount(
        accountId: String,
        hash: String
    ): Single<CreateRecoveryData> {
        return api.createRecoveryAccount(
            CreateRecoveryAccountRequest(
                accountId,
                hash
            )
        )
            .map { response -> response.data }
    }

    override fun grantRecoveryAccount(transaction: ByteArray): Single<GrantRecoveryData> {
        return api.grantRecoveryAccount(
            GrantRecoveryAccountRequest(
                transaction
            )
        )
            .map { response ->
                response.data
            }
    }

    override fun status(): Single<Boolean> {
        return api.status()
            .map { response ->
                response.data?.run {
                    isSuccess() || inProgress()
                } ?: false
            }
    }
}
