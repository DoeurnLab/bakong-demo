package kh.org.nbc.bakongdemo.screen.qrrecovery.interactor

import org.spongycastle.crypto.generators.SCrypt
import java.security.InvalidAlgorithmParameterException
import java.security.InvalidKeyException
import java.security.NoSuchAlgorithmException
import javax.crypto.BadPaddingException
import javax.crypto.Cipher
import javax.crypto.IllegalBlockSizeException
import javax.crypto.NoSuchPaddingException
import javax.crypto.spec.IvParameterSpec
import javax.crypto.spec.SecretKeySpec
import javax.inject.Inject

/**
 * Encrypts and decrypts content using AES algorithm in CBC mode
 * <h1>Encrypt</h1>
 * <p>
 *     `key = scrypt(pwd, salt)`, choose the other scrypt parameters so that it would run for ~300ms
 *     on a common smartphone (needs some testing, try `n: 8192`, `r: 8`, `p: 1`).
 *     The resulting `key` length must be 256 bit.
 *     `enc_data = AES256(priv, key)`. AES is in CBC mode with 256bit key, `iv = salt`.
 * </p>
 * <h1>Decrypt</h1>
 * <p>
 *      `key = scrypt(pwd, salt)`
 *      `priv = AES256(enc_data, key)`, where `priv` - user's private key.
 * </p>
 */
class AesCbcCipherImpl @Inject constructor() :
    AesCbcCipher {

    private val cipher = Cipher.getInstance("AES/CBC/PKCS5Padding")

    override fun encrypt(salt: ByteArray, passphrase: String, content: ByteArray): ByteArray {
        return aesProcess(salt, passphrase.toByteArray(), content, Cipher.ENCRYPT_MODE)
    }

    override fun decrypt(salt: ByteArray, passphrase: String, content: ByteArray): ByteArray {
        return aesProcess(salt, passphrase.toByteArray(), content, Cipher.DECRYPT_MODE)
    }

    @Throws(NoSuchPaddingException::class,
            NoSuchAlgorithmException::class,
            InvalidAlgorithmParameterException::class,
            InvalidKeyException::class,
            BadPaddingException::class,
            IllegalBlockSizeException::class)
    private fun aesProcess(salt: ByteArray, pass: ByteArray, content: ByteArray, mode: Int): ByteArray {
        val key = SCrypt.generate(pass, salt, 8192, 8, 1, 32)

        val secretKey = SecretKeySpec(key, "AES")
        val iv = IvParameterSpec(salt)

        cipher.init(mode, secretKey, iv)
        return cipher.doFinal(content)
    }
}