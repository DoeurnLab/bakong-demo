package kh.org.nbc.bakongdemo.screen.phone.data.mapper;

import javax.inject.Inject;

import kh.org.nbc.bakongdemo.network.data.mapper.BaseServerMapper;
import kh.org.nbc.bakongdemo.screen.phone.data.PhoneRegistration;
import kh.org.nbc.bakongdemo.screen.phone.data.RequestIdModel;


public class PhoneRegistrationToRequestIdModelMapper
        extends BaseServerMapper<PhoneRegistration, RequestIdModel> {

    @Inject
    public PhoneRegistrationToRequestIdModelMapper() {
    }

    @Override
    public RequestIdModel map(PhoneRegistration item) {
        RequestIdModel model = new RequestIdModel();
        model.setRequestId(item.getRequestId());
        return model;
    }
}
