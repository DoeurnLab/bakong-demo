package kh.org.nbc.bakongdemo.bakongcore.error

interface ErrorChecker {
    fun throwExceptionIfError(jsonString: String?)
}