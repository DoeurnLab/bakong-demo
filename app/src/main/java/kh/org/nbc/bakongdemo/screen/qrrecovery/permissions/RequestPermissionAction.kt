package kh.org.nbc.bakongdemo.screen.qrrecovery.permissions

import android.app.Activity
import androidx.fragment.app.DialogFragment
import kh.org.nbc.bakongdemo.bakongcore.alert.AlertAction
import kotlinx.android.parcel.Parcelize

@Parcelize
class RequestPermissionAction(
    private val permission: String,
    private val permissionRequestKey: Int
) : AlertAction() {

    override fun onAction(dialogFragment: DialogFragment) {
        (dialogFragment.context as Activity).requestPermissions( arrayOf(permission),
            permissionRequestKey)
    }
}