package jp.co.soramitsu.feature_change_phone_impl.data.request

import com.google.gson.annotations.SerializedName

data class VerifyCodeRequest(
    @SerializedName("publicKey")
    val publicKey: String,
    @SerializedName("requestId")
    val requestId: Long,
    @SerializedName("verificationCode")
    val verificationCode: String
)