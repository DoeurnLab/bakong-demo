package kh.org.nbc.bakongdemo.screen.username

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import kh.org.nbc.bakongdemo.base.BaseViewModel
import kh.org.nbc.bakongdemo.bakongcore.rxschedulers.RxSchedulers
import kh.org.nbc.bakongdemo.screen.registration.repository.RegistrationRepository
import kh.org.nbc.bakongdemo.screen.username.data.AccountCreationModel
import javax.inject.Inject

class UsernameViewModel @Inject constructor(
    private val rxSchedulers: RxSchedulers,
    private val registrationRepository: RegistrationRepository
): BaseViewModel() {

    private val _usernameAvailable = MutableLiveData<Boolean>()
    val usernameAvailable: LiveData<Boolean> get() = _usernameAvailable

    private val _accountCreationModel = MutableLiveData<AccountCreationModel>()
    val accountCreationModel: LiveData<AccountCreationModel> get() = _accountCreationModel

    fun checkUsername(username: String){
        unsubscribeOnDestroy(registrationRepository.checkLogin(username)
            .subscribeOn(rxSchedulers.io())
            .observeOn(rxSchedulers.main())
            .doOnSubscribe{
                _loading.postValue(true)
            }
            .doFinally {
                _loading.postValue(false)
            }
            .subscribe ({
                _usernameAvailable.postValue(it.isLoginFree)

            },{
                showError(it)
            })
        )
    }

    fun createUsername(username: String){
            unsubscribeOnDestroy(registrationRepository.createAccount(username)
                .subscribeOn(rxSchedulers.io())
                .observeOn(rxSchedulers.main())
                .doOnSubscribe{
                    _loading.postValue(true)
                }
                .doFinally {
                    _loading.postValue(false)
                }
                .subscribe ({
                    _accountCreationModel.postValue(it)
                },{
                    showError(it)
                })
            )


    }
}