package kh.org.nbc.bakongdemo.screen.send.data

import com.google.gson.annotations.SerializedName

class InitializePaymentPhoneReq {
    @SerializedName("phoneNumber")
    var phoneNumber: String? = null
}