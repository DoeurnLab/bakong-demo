package kh.org.nbc.bakongdemo.screen.phone

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import kh.org.nbc.bakongdemo.base.BaseViewModel
import kh.org.nbc.bakongdemo.bakongcore.rxschedulers.RxSchedulers
import kh.org.nbc.bakongdemo.screen.phone.data.RequestIdModel
import kh.org.nbc.bakongdemo.screen.registration.repository.RegistrationRepository
import kh.org.nbc.bakongdemo.storage.Storage
import javax.inject.Inject

class PhoneRegisterViewModel @Inject constructor(
    private val rxSchedulers: RxSchedulers,
    private val storage: Storage,
    private val registrationRepository: RegistrationRepository
) : BaseViewModel() {

    private val _requestIdModel = MutableLiveData<RequestIdModel>()
    val requestIdModel: LiveData<RequestIdModel> get() = _requestIdModel

    fun registerPhoneNumber(phoneNumber: String) {
        unsubscribeOnDestroy(registrationRepository.registerPhone(if (phoneNumber.startsWith("855")) phoneNumber else "855$phoneNumber")
            .subscribeOn(rxSchedulers.io())
            .observeOn(rxSchedulers.main())
            .doOnSubscribe {
                _loading.postValue(true)
            }
            .doFinally {
                _loading.postValue(false)
            }
            .subscribe({
                _requestIdModel.postValue(it)
                storage.setPhoneNumber(phoneNumber)

            }, {
                showError(it)
            })
        )
    }
}
