package kh.org.nbc.bakongdemo.screen.login.interactor

import io.reactivex.Completable
import io.reactivex.Single

interface RecoveryInteractor {
    fun createRecovery(accountId: String): Completable

    fun status(): Single<Boolean>
}