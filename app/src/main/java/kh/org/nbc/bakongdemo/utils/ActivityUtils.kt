package kh.org.nbc.bakongdemo.utils

import android.app.Activity
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Color
import android.os.Build
import android.view.View
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.fragment.app.FragmentManager
import kh.org.nbc.bakongdemo.bakongcore.alert.Alert
import kh.org.nbc.bakongdemo.bakongcore.alert.AlertDialogFragment
import kh.org.nbc.bakongdemo.screen.qrrecovery.permissions.RequestPermissionAction

fun Activity.toast(
    messageResId: String,
    duration: Int = Toast.LENGTH_SHORT
) {
    Toast.makeText(this, messageResId, duration).show()
}

fun Activity.toast(
    messageResId: Int,
    duration: Int = Toast.LENGTH_SHORT
) {
    Toast.makeText(this, messageResId, duration).show()
}

fun Activity.requestPermission(
    permission: String,
    permissionRequestKey: Int,
    explanationMessage: String,
    alertTitle: String,
    fragmentManager: FragmentManager
): Boolean {
    var permissionRequested = false

    if (ContextCompat.checkSelfPermission(this, permission)
        != PackageManager.PERMISSION_GRANTED
    ) {

        val fragment = AlertDialogFragment.newInstance(
            Alert(
                description = explanationMessage,
                title = alertTitle,
                okAction = RequestPermissionAction(permission, permissionRequestKey)
            )
        )
        fragment.isCancelable = false
        fragment.show(fragmentManager, explanationMessage)

        permissionRequested = true
    }

    return permissionRequested
}




fun Activity.exitApp() {
    val intent = ExitActivity.newIntent(this)
    startActivity(intent)
    finish()
}

fun Activity.recreateWithoutAnimation() {
    val restartIntent = Intent(this, this.javaClass)

    val extras = intent.extras
    if (extras != null) {
        restartIntent.putExtras(extras)
    }

    startActivity(restartIntent)
    overridePendingTransition(0, 0)

    finish()
}

fun Activity.setupStatusBarDarkTheme(view: View) {
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
        var flags = view.systemUiVisibility
        flags = flags xor View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR
        view.systemUiVisibility = flags
        window.statusBarColor = Color.BLACK
    }
}

fun Activity.setupStatusBarWhiteTheme(view: View) {
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
        var flags = view.systemUiVisibility
        flags = flags or View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR
        view.systemUiVisibility = flags
        window.statusBarColor = Color.WHITE
    }
}

