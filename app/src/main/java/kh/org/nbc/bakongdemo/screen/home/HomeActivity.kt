package kh.org.nbc.bakongdemo.screen.home

import android.app.AlertDialog
import android.content.Intent
import android.os.Bundle
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import kh.org.nbc.bakongdemo.base.BaseActivity
import kh.org.nbc.bakongdemo.R
import kh.org.nbc.bakongdemo.application.BakongApplication
import kh.org.nbc.bakongdemo.screen.deposit.BankListActivity
import kh.org.nbc.bakongdemo.screen.home.data.HistoryTransaction
import kh.org.nbc.bakongdemo.screen.home.data.TransactionAdapter
import kh.org.nbc.bakongdemo.screen.home.data.TransactionRequest
import kh.org.nbc.bakongdemo.screen.login.LoginActivity
import kh.org.nbc.bakongdemo.screen.selectphone.SelectPhoneActivity
import kh.org.nbc.bakongdemo.utils.hide
import kh.org.nbc.bakongdemo.utils.show
import kotlinx.android.synthetic.main.activity_home.*
import javax.inject.Inject

class HomeActivity : BaseActivity(), SwipeRefreshLayout.OnRefreshListener {


    private var isLoaded = false
    private lateinit var adapter: TransactionAdapter
    private lateinit var transactions: ArrayList<HistoryTransaction?>
    private var currentPage = 0
    private var totalPage: Int = 0
    private var isRefresh = false

    @Inject
    lateinit var viewModel: HomeViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        (application as BakongApplication).appComponent.inject(this)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)

        viewModel.loading.observe(this, Observer {
            if (it)
                showLoading()
            else hideLoading()
        })

        viewModel.accountRiel.observe(this, Observer {
            text_view_riel_account.text = it
            (application as BakongApplication).rielAccount = it.toDouble()
        })

        viewModel.accountUsd.observe(this, Observer {
            text_view_usd_account.text = it
            (application as BakongApplication).usdAccount = it.toDouble()
        })

        viewModel.transactionData.observe(this, Observer {

            if (isRefresh) {
                removeList()
                isRefresh = false
                swipe_refresh.isRefreshing = false
            } else {
                onLoaded()
            }
            if (it != null) {
                adapter.addTransaction(it.transactions!!)
                if (adapter.isEmpty()){
                    text_no_event.show()
                }else text_no_event.hide()
            }
            totalPage = it.pages

        })
        transactions = ArrayList()
        val layoutManager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
        rv_transaction.layoutManager = layoutManager
        adapter = TransactionAdapter(this, transactions)
        rv_transaction.adapter = adapter

        rv_transaction.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
                val totalItemCount: Int = layoutManager.itemCount
                val lastVisibleItem: Int = layoutManager.findLastVisibleItemPosition()
                val isRequestData = totalItemCount <= (lastVisibleItem + VISIBLE_THRESHOLD)
                if (!isLoaded && totalPage() && isRequestData) {
                    isLoaded = true
                    getTransactions()
                }
            }
        })

        text_logout.setOnClickListener {
            AlertDialog.Builder(this)
                .setCancelable(false)
                .setTitle(resources.getString(R.string.logout))
                .setMessage(resources.getString(R.string.logout_message))
                .setNegativeButton(resources.getString(R.string.cancel)) { dialog, _ ->
                    dialog.dismiss()
                }
                .setPositiveButton(resources.getString(R.string.ok)) { dialog, _ ->
                    dialog.dismiss()
                    val intent = Intent(this, LoginActivity::class.java)
                    intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK
                    startActivity(intent)
                }
                .show()
        }
        swipe_refresh.setOnRefreshListener(this)

        view_send.setOnClickListener {
            startActivity(Intent(this, SelectPhoneActivity::class.java))
        }
        view_deposit.setOnClickListener {
            startActivity(Intent(this, BankListActivity::class.java))
        }
        onRefresh()
    }

    private fun onLoaded() {
        isLoaded = false
        adapter.removeProgressBar()
    }

    private fun showLoading() {
        progressBar.show()
    }

    private fun hideLoading() {
        progressBar.hide()
        if (swipe_refresh.isRefreshing)
            swipe_refresh.isRefreshing = false
    }

    private fun initPage() {
        currentPage = 0
        totalPage = 0
    }

    override fun onRefresh() {
        isRefresh = true
        initPage()
        viewModel.getBalance()
        getTransactions()
    }

    private fun getTransactions() {
        val request = TransactionRequest()
        request.page = currentPage++
        viewModel.getTransaction(request)
        adapter.addProgressBar()
    }

    fun totalPage(): Boolean {
        return totalPage != 0 && totalPage >= currentPage
    }

    private fun removeList() {
        adapter.removeList()
        adapter.notifyDataSetChanged()
    }

    companion object {
        const val VISIBLE_THRESHOLD = 5
    }


}