package kh.org.nbc.bakongdemo.screen.verification.data.mapper;

import javax.inject.Inject;

import kh.org.nbc.bakongdemo.network.data.mapper.BaseServerMapper;
import kh.org.nbc.bakongdemo.screen.verification.data.PhoneVerification;
import kh.org.nbc.bakongdemo.screen.verification.data.VerificationModel;


public class PhoneVerificationToVerificationModelMapper
        extends BaseServerMapper<PhoneVerification, VerificationModel> {

    @Inject
    public PhoneVerificationToVerificationModelMapper() {
    }

    @Override
    public VerificationModel map(PhoneVerification item) {
        VerificationModel model = new VerificationModel();
        model.setVerified(item.isVerified());
        return model;
    }
}
