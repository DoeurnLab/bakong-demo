package kh.org.nbc.bakongdemo.screen.password.validate

interface PasswordView {


        fun showPasswordLengthError()

        fun showPasswordDidNotMatchError()

        fun showStrongPolicyError()

        fun enableButton(flag: Boolean)

}