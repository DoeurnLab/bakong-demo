package kh.org.nbc.bakongdemo.screen.qrrestore.data

import com.google.gson.annotations.SerializedName

data class RestoreAccountRequest(
    @SerializedName("publicKey")
    val publicKey: String,
    @SerializedName("requestId")
    var requestId: Long,
    @SerializedName("signature")
    var signature: ByteArray,
    @SerializedName("deviceId")
    var deviceId: String
)