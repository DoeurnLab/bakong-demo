package kh.org.nbc.bakongdemo.screen.deposit.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import kh.org.nbc.bakongdemo.R
import kh.org.nbc.bakongdemo.screen.deposit.data.BankListResponse

class BankListAdapter(val context: Context, private var bankList: List<BankListResponse?>, var onBankSelected: (bank: BankListResponse)->Unit) : RecyclerView.Adapter<BankListAdapter.BankListVH>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BankListVH {
        val view = LayoutInflater.from(context).inflate(R.layout.bank_list_item, parent, false)
        return BankListVH(view)
    }

    override fun getItemCount(): Int {
        return bankList.size
    }

    override fun onBindViewHolder(holder: BankListVH, position: Int) {
        val bank = bankList[position]
        holder.textTitle.text = bank!!.bin
        holder.itemView.setOnClickListener {
            onBankSelected(bankList[position]!!)
        }
    }

    class BankListVH(itemView: View): RecyclerView.ViewHolder(itemView){
        val textTitle: TextView = itemView.findViewById(R.id.text_view_title)
    }

    companion object {
        val TAG = BankListAdapter::class.simpleName
    }


}