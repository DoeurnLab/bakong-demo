package kh.org.nbc.bakongdemo.screen.send

import io.reactivex.Single
import kh.org.nbc.bakongdemo.bakongcore.repository.IrohaRepository
import kh.org.nbc.bakongdemo.screen.registration.api.BakongApi
import kh.org.nbc.bakongdemo.screen.send.data.*
import kh.org.nbc.bakongdemo.storage.Storage
import javax.inject.Inject

class SendRepositoryImpl @Inject constructor(
    private val api: BakongApi,
    private val irohaRepository: IrohaRepository,
    private val storage: Storage
): SendRepository {

    override fun initializePaymentPhone(phoneNumber: String): Single<InitializePaymentPhone> {
        val phone = InitializePaymentPhoneReq()
        phone.phoneNumber = phoneNumber
        return api.initializePaymentPhone(phone).map { it.data }
    }

    override fun sendPayment(paymentData: PaymentData): Single<SendPaymentResponse> {

        return Single.just(paymentData)
            .map { data ->
                val payload = PaymentPayload()
                payload.amount = data.money.amount
                payload.assetId = ServerCurrencyToAssetIdMapper.mapCurrencyToAssetId(
                    data.money.currency
                )
                payload.senderAccountId = storage.getAccountId()
                payload.receiverAccountId = data.receiverId
                payload.description = data.receiverId
                payload.time = System.currentTimeMillis()
                payload
            }
            .flatMap(irohaRepository::signPayment)
            .map { signature -> createConfirmRequest(paymentData, signature) }
            .flatMap(api::sendPayment)
            .map { it.data }
    }

    private fun createConfirmRequest(
        data: PaymentData,
        signature: PaymentSignature
    ): SendPayment? {
        val request = SendPayment()
        request.amount = ServerAmountFormatter.formatAmount(data.money.amount)
        request.assetId = ServerCurrencyToAssetIdMapper.mapCurrencyToAssetId(data.money.currency)
        request.description = data.description
        request.receiverAccountId = data.receiverId
        request.trxHash = signature.transactionHash
        request.signByte = signature.signedBytes
        request.qrCode = data.qrCodeData
        return request
    }

}