package kh.org.nbc.bakongdemo.network.converter;

import androidx.annotation.NonNull;

import java.io.IOException;

import okhttp3.ResponseBody;
import retrofit2.Converter;

public class SessionResponseConverter<T> implements Converter<ResponseBody, T> {


    private final Converter<ResponseBody, T> mConverter;

    public SessionResponseConverter(@NonNull Converter<ResponseBody, T> converter) {
        mConverter = converter;
    }

    @Override
    public T convert(@NonNull ResponseBody value) throws IOException {
        return mConverter.convert(value);
    }

}
