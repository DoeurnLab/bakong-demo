package kh.org.nbc.bakongdemo.screen.registration.keygenerator;

import androidx.annotation.NonNull;

import java.security.KeyPair;
import java.util.Locale;

import javax.inject.Inject;
import javax.xml.bind.DatatypeConverter;

import io.reactivex.Single;
import jp.co.soramitsu.crypto.ed25519.Ed25519Sha3;
import kh.org.nbc.bakongdemo.screen.registration.keygenerator.model.BakongKeyPair;
import kh.org.nbc.bakongdemo.screen.registration.keygenerator.model.BakongKeyPairIroha;

public class KeysCreatorImpl implements KeysCreator {

    private final Ed25519Sha3 mEd25519Sha3;

    @Inject
    public KeysCreatorImpl(@NonNull Ed25519Sha3 ed25519Sha3) {
        mEd25519Sha3 = ed25519Sha3;
    }

    @Override
    public Single<BakongKeyPair> create(byte[] seed) {
        return Single.fromCallable(() -> {
            KeyPair irohaKeypair = mEd25519Sha3.generateKeypair(seed);
            String publicKey = DatatypeConverter.printHexBinary(irohaKeypair.getPublic().getEncoded());
            String privateKey = DatatypeConverter.printHexBinary(irohaKeypair.getPrivate().getEncoded());
            return new BakongKeyPairIroha(publicKey.toLowerCase(Locale.ENGLISH),
                    privateKey.toLowerCase(Locale.ENGLISH));
        });
    }
}
