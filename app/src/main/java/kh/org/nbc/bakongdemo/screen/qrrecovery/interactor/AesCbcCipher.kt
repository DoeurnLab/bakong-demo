package kh.org.nbc.bakongdemo.screen.qrrecovery.interactor

interface AesCbcCipher {
    fun encrypt(salt: ByteArray, passphrase: String, content: ByteArray): ByteArray
    fun decrypt(salt: ByteArray, passphrase: String, content: ByteArray): ByteArray
}