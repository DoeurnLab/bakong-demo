package kh.org.nbc.bakongdemo.bakongcore.interactor

import io.reactivex.Completable
import java.io.InputStream
import java.io.OutputStream

interface FileInteractor {
    fun downloadDir(): String
    fun copy(originalPath: String, destinationPath: String): Completable

    fun copy(inputStream: InputStream, out: OutputStream): Boolean
}