package kh.org.nbc.bakongdemo.screen.send

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import kh.org.nbc.bakongdemo.bakongcore.rxschedulers.RxSchedulers
import kh.org.nbc.bakongdemo.base.BaseViewModel
import kh.org.nbc.bakongdemo.screen.send.data.PaymentData
import kh.org.nbc.bakongdemo.screen.send.data.SendPaymentResponse
import kh.org.nbc.bakongdemo.utils.Currency
import kh.org.nbc.bakongdemo.utils.Money
import javax.inject.Inject

class SendViewModel @Inject constructor(
    private val rxSchedulers: RxSchedulers,
    private val repository: SendRepository
): BaseViewModel() {

    private val _sendPaymentResponse = MutableLiveData<SendPaymentResponse>()
    val sendPaymentResponse: LiveData<SendPaymentResponse> get() = _sendPaymentResponse

    fun send(receiverId: String, amount: String, currency: Currency?){

        val paymentData = PaymentData()
        paymentData.receiverId = receiverId
        paymentData.money = Money(amount.toBigDecimal(), currency?: Currency.USD)
        paymentData.description = "Vandoeurn testing"
        paymentData.qrCodeData = null

        unsubscribeOnDestroy(

            repository.sendPayment(paymentData)
                .subscribeOn(rxSchedulers.io())
                .observeOn(rxSchedulers.main())
                .doOnSubscribe { _loading.postValue(true) }
                .doFinally { _loading.postValue(false) }
                .subscribe({
                    _sendPaymentResponse.postValue(it)
                }, { throwable ->
                    showError(throwable)
                })

        )
    }
}