package kh.org.nbc.bakongdemo.screen.password.validate

import javax.inject.Inject

class PasscodeValidator @Inject constructor() :
    TextValidator {

    private val validators = listOf(
        LessLengthValidator(8),
        PatternValidator(
            "^(?=.*[\\!\\\"\\#\\\$\\%\\&\\'\\(\\)\\*\\+\\,\\-\\.\\/\\:\\;\\<\\>\\=\\?\\@\\[\\]\\{\\}\\\\\\^\\_\\`\\~])" +
                    "(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])[^\\s]{8,}\$"
        )
    )

    override fun validate(text: String): ValidationResult {
        for (validator in validators) {
            val validationResult = validator.validate(text)
            if (validationResult != ValidationResult.VALID) {
                return validationResult
            }
        }
        return ValidationResult.VALID
    }
}