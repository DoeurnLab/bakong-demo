package kh.org.nbc.bakongdemo.screen.verification.data;

import java.util.Objects;

public class VerificationModel {

    private boolean mVerified;

    public boolean isVerified() {
        return mVerified;
    }

    public void setVerified(boolean verified) {
        mVerified = verified;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        VerificationModel that = (VerificationModel) o;
        return mVerified == that.mVerified;
    }

    @Override
    public int hashCode() {
        return Objects.hash(mVerified);
    }

    @Override
    public String toString() {
        return "VerificationModel{" +
                "mVerified=" + mVerified +
                '}';
    }
}
