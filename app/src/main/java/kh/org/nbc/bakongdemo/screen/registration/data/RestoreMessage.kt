package kh.org.nbc.bakongdemo.screen.registration.data

import com.google.gson.annotations.SerializedName

data class RestoreMessage(
    @SerializedName("message")
    val message: String,
    @SerializedName("requestId")
    val requestId: Long


)