/*
 * Copyright (C) 2019 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package kh.org.nbc.bakongdemo.storage

import android.content.Context
import java.util.*
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class SharedPreferencesStorage @Inject constructor(context: Context) : Storage {

    enum class KEY{
        PRIVATE_KEY,
        PUBLIC_KEY,
        DEVICE_ID,
        REQUEST_ID,
        PHONE,
        PASSWORD,
        PIN_CODE,
        DISPLAY_NAME,
        TOKEN,
        ACCOUNT_ID,
        REGISTERED
    }

    private val sharedPreferences = context.getSharedPreferences("SharedPreferencesStorage", Context.MODE_PRIVATE)

    override fun clearAll() {
        with(sharedPreferences.edit()){
            clear().commit()
        }
    }

    override fun setPrivateKey(key: String) {
        with(sharedPreferences.edit()){
            putString(KEY.PRIVATE_KEY.toString(), key)
                .apply()
        }
    }

    override fun getPrivateKey(): String? {
        return sharedPreferences.getString(KEY.PRIVATE_KEY.toString(), "")
    }

    override fun setPublicKey(key: String) {
        with(sharedPreferences.edit()){
            putString(KEY.PUBLIC_KEY.toString(), key)
                .apply()
        }    }

    override fun getPublicKey(): String? {
        return sharedPreferences.getString(KEY.PUBLIC_KEY.toString(), "")
    }

    override fun setDeviceId(id: String) {
        with(sharedPreferences.edit()){
            putString(KEY.DEVICE_ID.toString(), id)
                .apply()
        }
    }

    override fun getDeviceId(): String? {
        return sharedPreferences.getString(KEY.DEVICE_ID.toString(), "")
    }

    private fun initDeviceId(): String {
        val deviceId = UUID.randomUUID().toString()
        setDeviceId(deviceId)
        return deviceId
    }

    override fun setRequestId(id: String) {
        with(sharedPreferences.edit()){
            putString(KEY.REQUEST_ID.toString(), id)
                .apply()
        }
    }

    override fun getRequestId(): String? {
        return sharedPreferences.getString(KEY.REQUEST_ID.toString(), "")
    }

    override fun setPhoneNumber(num: String) {
        with(sharedPreferences.edit()){
            putString(KEY.PHONE.toString(), num)
                .apply()
        }
    }

    override fun getPhoneNumber(): String? {
        return sharedPreferences.getString(KEY.PHONE.toString(), "")
    }

    override fun setPassword(password: String) {
        with(sharedPreferences.edit()){
            putString(KEY.PASSWORD.toString(), password)
                .apply()
        }
    }

    override fun getPassword(): String? {
        return sharedPreferences.getString(KEY.PASSWORD.toString(), "")
    }

    override fun setPinCode(pin: String) {
        with(sharedPreferences.edit()){
            putString(KEY.PIN_CODE.toString(), pin)
                .apply()
        }
    }

    override fun getPinCode(): String? {
        return sharedPreferences.getString(KEY.PIN_CODE.toString(), "")
    }

    override fun setDisplayName(name: String) {
        with(sharedPreferences.edit()){
            putString(KEY.DISPLAY_NAME.toString(), name)
                .apply()
        }
    }

    override fun getDisplayName(): String? {
        return sharedPreferences.getString(KEY.DISPLAY_NAME.toString(), "")
    }

    override fun setToken(token: String?) {
        with(sharedPreferences.edit()){
            putString(KEY.TOKEN.toString(), "Bearer $token")
                .apply()
        }
    }

    override fun getToken(): String? {
        return sharedPreferences.getString(KEY.TOKEN.toString(), "")
    }

    override fun setAccountId(id: String) {
        with(sharedPreferences.edit()){
            putString(KEY.ACCOUNT_ID.toString(), id)
                .apply()
        }
    }

    override fun getAccountId(): String? {
        return sharedPreferences.getString(KEY.ACCOUNT_ID.toString(), "")

    }

    override fun setRegistered(isRegister: Boolean) {
        with(sharedPreferences.edit()){
            putBoolean(KEY.REGISTERED.toString(), isRegister)
                .apply()
        }
    }

    override fun isRegistered(): Boolean {
        return sharedPreferences.getBoolean(KEY.REGISTERED.toString(), false)
    }



}
