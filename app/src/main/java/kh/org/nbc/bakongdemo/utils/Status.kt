package kh.org.nbc.bakongdemo.utils

import com.google.gson.annotations.SerializedName

data class Status(
    @SerializedName("code")
    var code: Int = 0,
    @SerializedName("errorCode")
    var errorCode: Int = 0,
    @SerializedName("error")
    var error: String? = null,
    @SerializedName("warning")
    var warning: String? = null
)

fun Status.getErrorMessage(): String {
    val message = if (!error.isNullOrBlank()) error else warning
    return message ?: "Unexpected error"
}
