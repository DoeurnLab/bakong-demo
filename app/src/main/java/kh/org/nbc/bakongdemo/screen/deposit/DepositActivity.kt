package kh.org.nbc.bakongdemo.screen.deposit

import android.app.Activity
import android.app.AlertDialog
import android.os.Bundle
import android.text.TextUtils
import android.util.Log
import androidx.core.content.ContextCompat
import androidx.lifecycle.Observer
import kh.org.nbc.bakongdemo.R
import kh.org.nbc.bakongdemo.application.BakongApplication
import kh.org.nbc.bakongdemo.base.BaseActivity
import kh.org.nbc.bakongdemo.screen.deposit.BankListActivity.Companion.BANK_ACCOUNT_ID
import kh.org.nbc.bakongdemo.screen.deposit.BankListActivity.Companion.PARTICIPANT_CODE
import kh.org.nbc.bakongdemo.screen.deposit.data.DepositPaymentData
import kh.org.nbc.bakongdemo.screen.deposit.data.DepositViewModel
import kh.org.nbc.bakongdemo.storage.Storage
import kh.org.nbc.bakongdemo.utils.Currency
import kh.org.nbc.bakongdemo.utils.Money
import kh.org.nbc.bakongdemo.utils.hide
import kh.org.nbc.bakongdemo.utils.show
import kotlinx.android.synthetic.main.activity_deposit.*
import kotlinx.android.synthetic.main.activity_deposit.image_riel
import kotlinx.android.synthetic.main.activity_deposit.image_usd
import kotlinx.android.synthetic.main.activity_deposit.text_riel_account
import kotlinx.android.synthetic.main.activity_deposit.text_usd_account
import kotlinx.android.synthetic.main.activity_send.view_riel
import kotlinx.android.synthetic.main.activity_send.view_usd
import kotlinx.android.synthetic.main.layout_progress_bar.*
import kotlinx.android.synthetic.main.layout_toolbar.*
import javax.inject.Inject

class DepositActivity : BaseActivity() {

    private var rielActive: Boolean? = null
    private var participantCode: String? = null
    private var accountNumber: String? = null
    private var deposit: DepositPaymentData? = null
    private var userInputAmount: Double? = null
    private var bankAccountId: String? = null
    private var rielValidAmount: Double = 0.0
    private var usdValidAmount: Double = 0.0

    @Inject
    lateinit var viewModel: DepositViewModel
    @Inject
    lateinit var storage: Storage

    override fun onCreate(savedInstanceState: Bundle?) {
        (application as BakongApplication).appComponent.inject(this)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_deposit)

        val mApplication = (application as BakongApplication)

        rielValidAmount = mApplication.rielAccount?:0.0
        usdValidAmount = mApplication.usdAccount?:0.0

        text_riel_account.text = "$rielValidAmount"
        text_usd_account.text =  "$usdValidAmount"

        accountActive(true)

        view_usd.setOnClickListener {
            accountActive(false)
        }
        view_riel.setOnClickListener {
            accountActive(true)
        }
        viewModel.loading.observe(this, Observer {
            if (it){
                showLoading()
            }else hideLoading()
        })

        viewModel.error.observe(this, Observer {
            onError(it)
        })

        button_back.show()
        button_back.setOnClickListener {
            finish()
        }
        text_view_title.text = resources.getString(R.string.set_amount)

        button_deposit.setOnClickListener {
            when {
                TextUtils.isEmpty(edt_account_number.text) -> {
                    edt_account_number.error = "Account number is required!"
                }
                TextUtils.isEmpty(edt_amount.text) -> {
                    edt_amount.error = "Amount is required!"
                }
                edt_amount.text.toString().toDouble() <= 0 ->{
                    edt_amount.error = "Invalid amount!"
                    return@setOnClickListener
                }
                else -> {


                    participantCode = intent.getStringExtra(PARTICIPANT_CODE)
                    bankAccountId = intent.getStringExtra(BANK_ACCOUNT_ID)
                    accountNumber = edt_account_number.text.toString()
                    userInputAmount = edt_amount.text.toString().toDouble()
                    Log.d("Data", "participantCode: $participantCode")
                    Log.d("Data", "accountNumber: $accountNumber")
                    Log.d("Data", "bankAccountId: $bankAccountId")
                    Log.d("Data", "Amount: $userInputAmount")

                    if (rielActive ==true && userInputAmount!! > rielValidAmount){
                        edt_amount.error = "Invalid amount!"
                        return@setOnClickListener
                    }else {
                        if (rielActive == false && userInputAmount!! > usdValidAmount){
                            edt_amount.error = "Invalid amount!"
                            return@setOnClickListener
                        }
                    }

                    deposit = DepositPaymentData()
                    deposit!!.accountNumber = accountNumber
                    deposit!!.money = Money(userInputAmount!!.toDouble().toBigDecimal(), if (rielActive==true) Currency.KHR else Currency.USD)
                    deposit!!.description = "Vandoeurn Test"
                    deposit!!.bankId = bankAccountId
                    viewModel.checkAccountAndDeposit(accountNumber!!, participantCode!!, deposit!!)
                }
            }
        }

        viewModel.depositResponse.observe(this, Observer {
            //Log.d("Deposit Response", "onCreate: $it")
            AlertDialog.Builder(this)
                .setCancelable(false)
                .setTitle(resources.getString(R.string.success_deposit))
                .setMessage(resources.getString(R.string.deposit_successfully))
                .setPositiveButton(resources.getString(R.string.ok)) { dialog, _ ->
                    dialog.dismiss()
                    setResult(Activity.RESULT_OK)
                    finish()
                }
                .show()
        })
    }

    private fun accountActive(isRielActive: Boolean) {
        rielActive = isRielActive
        if (isRielActive) {
            text_usd_account.setTextColor(getColor(R.color.very_light_grey))
            text_riel_account.setTextColor(getColor(R.color.deep_red))

            image_usd.setColorFilter(ContextCompat.getColor(this, R.color.very_light_grey))
            image_riel.setColorFilter(ContextCompat.getColor(this, R.color.deep_red))

        } else {
            text_usd_account.setTextColor(getColor(R.color.pumpkin))
            text_riel_account.setTextColor(getColor(R.color.very_light_grey))

            image_usd.setColorFilter(ContextCompat.getColor(this, R.color.pumpkin))
            image_riel.setColorFilter(ContextCompat.getColor(this, R.color.very_light_grey))
        }
    }
    private fun showLoading(){
        progressBar.show()
    }
    private fun hideLoading(){
        progressBar.hide()
    }
}