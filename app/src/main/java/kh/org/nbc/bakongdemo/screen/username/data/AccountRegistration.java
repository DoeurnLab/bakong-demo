package kh.org.nbc.bakongdemo.screen.username.data;

import com.google.gson.annotations.SerializedName;

import java.util.Objects;

public class AccountRegistration {

    @SerializedName("accountId")
    private String mAccountId;
    @SerializedName("accountVerified")
    private boolean mAccountVerified;
    @SerializedName("trxHash")
    private String mTrxHash;

    public String getAccountId() {
        return mAccountId;
    }

    public void setAccountId(String accountId) {
        mAccountId = accountId;
    }

    public boolean isAccountVerified() {
        return mAccountVerified;
    }

    public void setAccountVerified(boolean accountVerified) {
        mAccountVerified = accountVerified;
    }

    public String getTrxHash() {
        return mTrxHash;
    }

    public void setTrxHash(String trxHash) {
        mTrxHash = trxHash;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        AccountRegistration that = (AccountRegistration) o;
        return mAccountVerified == that.mAccountVerified &&
                Objects.equals(mAccountId, that.mAccountId) &&
                Objects.equals(mTrxHash, that.mTrxHash);
    }

    @Override
    public int hashCode() {
        return Objects.hash(mAccountId, mAccountVerified, mTrxHash);
    }

    @Override
    public String toString() {
        return "AccountRegistration{" +
                "mAccountId='" + mAccountId + '\'' +
                ", mAccountVerified=" + mAccountVerified +
                ", mTrxHash='" + mTrxHash + '\'' +
                '}';
    }
}
