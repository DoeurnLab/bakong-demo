package kh.org.nbc.bakongdemo.di.module;

import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;

import javax.inject.Singleton;

import dagger.Binds;
import dagger.Module;
import dagger.Provides;
import jp.co.soramitsu.crypto.ed25519.Ed25519Sha3;
import kh.org.nbc.bakongdemo.bakongcore.interactor.FileInteractor;
import kh.org.nbc.bakongdemo.bakongcore.interactor.FileInteractorImpl;
import kh.org.nbc.bakongdemo.network.data.NetworkApiCreator;
import kh.org.nbc.bakongdemo.screen.qrrecovery.repository.QrRecoveryRepository;
import kh.org.nbc.bakongdemo.screen.qrrecovery.repository.QrRecoveryRepositoryImpl;
import kh.org.nbc.bakongdemo.screen.qrrecovery.repository.QrRepository;
import kh.org.nbc.bakongdemo.screen.qrrecovery.repository.QrRepositoryImpl;
import kh.org.nbc.bakongdemo.screen.qrrecovery.interactor.TimeSource;
import kh.org.nbc.bakongdemo.screen.qrrecovery.interactor.TimeSourceImpl;
import kh.org.nbc.bakongdemo.screen.qrrecovery.interactor.AesCbcCipher;
import kh.org.nbc.bakongdemo.screen.qrrecovery.interactor.AesCbcCipherImpl;
import kh.org.nbc.bakongdemo.screen.qrrecovery.permissions.StoragePermissionChecker;
import kh.org.nbc.bakongdemo.screen.qrrecovery.permissions.StoragePermissionCheckerImpl;
import kh.org.nbc.bakongdemo.screen.registration.repository.RegistrationRepository;
import kh.org.nbc.bakongdemo.screen.registration.repository.RegistrationRepositoryImpl;
import kh.org.nbc.bakongdemo.screen.registration.api.BakongApi;
import kh.org.nbc.bakongdemo.screen.registration.keygenerator.KeysCreator;
import kh.org.nbc.bakongdemo.screen.registration.keygenerator.KeysCreatorImpl;
import kh.org.nbc.bakongdemo.screen.registration.keygenerator.MnemonicInteractor;
import kh.org.nbc.bakongdemo.screen.registration.keygenerator.MnemonicInteractorImpl;
import kh.org.nbc.bakongdemo.screen.registration.keygenerator.MnemonicRepository;
import kh.org.nbc.bakongdemo.screen.registration.keygenerator.MnemonicRepositoryImpl;

@Module
public abstract class RegistrationModule {

    @Provides
    static BakongApi provideBakongApi(NetworkApiCreator apiCreator) {
        return apiCreator.create(BakongApi.class);
    }


    @Binds
    abstract MnemonicRepository provideMnemonicRepository(MnemonicRepositoryImpl mnemonicRepository);

    @Binds
    abstract MnemonicInteractor provideMnemonicInteractor(MnemonicInteractorImpl mnemonicInteractor);

    @Binds
    abstract KeysCreator provideKeysCreator(KeysCreatorImpl mKeysCreator);

    @Provides
    static Ed25519Sha3 provideEd25519Sha3() {
        return new Ed25519Sha3();
    }

    @Provides
    static SecureRandom provideSecureRandom() {
        try {
            return SecureRandom.getInstance("SHA1PRNG");
        } catch (NoSuchAlgorithmException e) {
            return new SecureRandom();
        }
    }

    @Binds
    abstract RegistrationRepository provideRegistrationRepository(RegistrationRepositoryImpl registerRepositoryImpl);

    @Binds
    abstract AesCbcCipher provideAesCbcCipher(AesCbcCipherImpl aesCbcCipherImpl);
    @Binds
    abstract QrRepository provideQrRepository(QrRepositoryImpl qrRepositoryImpl);

    @Singleton
    @Binds
    abstract TimeSource provideTimeSource(TimeSourceImpl timeSource);

    @Binds
    abstract StoragePermissionChecker provideStoragePermissionChecker(StoragePermissionCheckerImpl storagePermissionChecker);

    @Binds
    abstract QrRecoveryRepository provideQrRecoveryRepository(QrRecoveryRepositoryImpl qrRecoveryRepository);

    @Binds
    abstract FileInteractor provideFileInteractor(FileInteractorImpl FileInteractor);

}
