package kh.org.nbc.bakongdemo.bakongcore.exception

import kh.org.nbc.bakongdemo.bakongcore.alert.Alert
import kh.org.nbc.bakongdemo.bakongcore.exception.ServerException

class ChangePhoneException(
    code: Int,
    errorCode: Int,
    message: String,
    alert: Alert
) : ServerException(code, errorCode, message, alert)