package kh.org.nbc.bakongdemo.screen.registration.keygenerator

import io.reactivex.Single

interface MnemonicRepository {
    fun createMnemonic(): Single<String>
    fun entropyFromMnemonic(mnemonic: String): ByteArray
    fun deriveSeedFromMnemonic(
        mnemonic: String,
        password: String,
        project: String,
        purpose: String,
        keyLength: Int
    ): ByteArray
}