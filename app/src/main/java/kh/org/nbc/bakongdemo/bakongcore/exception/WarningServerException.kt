package kh.org.nbc.bakongdemo.bakongcore.exception

import kh.org.nbc.bakongdemo.bakongcore.exception.ServerException

class WarningServerException(
    code: Int,
    errorCode: Int,
    message: String
) : ServerException(code, errorCode, message)
