package kh.org.nbc.bakongdemo.screen.send.data;

import androidx.annotation.NonNull;

import java.util.HashMap;
import java.util.Map;

import kh.org.nbc.bakongdemo.utils.Currency;

public class ServerCurrencyToAssetIdMapper {

    private static final String KHR_ASSET_ID = "khr#nbc";
    private static final String USD_ASSET_ID = "usd#nbc";

    private static final Map<Currency, String> CURRENCIES_TO_ASSET_ID_BUNDLE;
    private static final Map<String, Currency> ASSET_ID_TO_CURRENCIES_BUNDLE;

    static {
        CURRENCIES_TO_ASSET_ID_BUNDLE = new HashMap<>();
        CURRENCIES_TO_ASSET_ID_BUNDLE.put(Currency.KHR, KHR_ASSET_ID);
        CURRENCIES_TO_ASSET_ID_BUNDLE.put(Currency.USD, USD_ASSET_ID);

        ASSET_ID_TO_CURRENCIES_BUNDLE = new HashMap<>();
        ASSET_ID_TO_CURRENCIES_BUNDLE.put(KHR_ASSET_ID, Currency.KHR);
        ASSET_ID_TO_CURRENCIES_BUNDLE.put(USD_ASSET_ID, Currency.USD);
    }

    public static String mapCurrencyToAssetId(@NonNull Currency currency) {
        return CURRENCIES_TO_ASSET_ID_BUNDLE.get(currency);
    }

    public static Currency mapAssetIdToCurrency(@NonNull String assetId) {
        return ASSET_ID_TO_CURRENCIES_BUNDLE.get(assetId);
    }

}
