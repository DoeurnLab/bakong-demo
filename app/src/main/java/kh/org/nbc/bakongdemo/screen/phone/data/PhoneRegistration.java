package kh.org.nbc.bakongdemo.screen.phone.data;

import com.google.gson.annotations.SerializedName;

import java.util.Objects;

public class PhoneRegistration {
    @SerializedName("requestId")
    private String requestId;

    public String getRequestId() {
        return requestId;
    }

    public void setRequestId(String requestId) {
        this.requestId = requestId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof PhoneRegistration)) return false;
        PhoneRegistration that = (PhoneRegistration) o;
        return Objects.equals(getRequestId(), that.getRequestId());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getRequestId());
    }
}
