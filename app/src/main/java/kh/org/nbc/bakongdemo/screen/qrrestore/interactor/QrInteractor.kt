package kh.org.nbc.bakongdemo.screen.qrrestore.interactor

import android.net.Uri

interface QrInteractor {
    fun encode(text: String): String
    fun decode(path: String): String
    fun decode(uri: Uri): String?
}