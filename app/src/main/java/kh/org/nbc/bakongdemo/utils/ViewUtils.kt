package kh.org.nbc.bakongdemo.utils

import android.graphics.Rect
import android.view.View


/**
 * Show the view  (visibility = View.VISIBLE)
 */
fun View.show(): View {
    if (visibility != View.VISIBLE) visibility = View.VISIBLE
    return this
}

/**
 * Hide the view. (visibility = View.INVISIBLE)
 */
fun View.hide(): View {
    if (visibility != View.INVISIBLE) visibility = View.INVISIBLE
    return this
}

/**
 * Remove the view. (visibility = View.GONE)
 */
fun View.gone(): View {
    if (visibility != View.GONE) visibility = View.GONE
    return this
}

fun View.animateGone() {
    this.animate().alpha(0f).withEndAction { this.gone() }.start()
}

fun View.animateShow() {
    this.animate()
        .alpha(1f)
        .withStartAction {
            this.alpha = .2f
            this.show()
        }
        .start()
}

fun View.fetchGlobalVisibleRect() = Rect().apply { getGlobalVisibleRect(this) }
