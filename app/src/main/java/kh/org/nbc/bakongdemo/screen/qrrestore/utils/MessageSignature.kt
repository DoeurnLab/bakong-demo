package kh.org.nbc.bakongdemo.screen.qrrestore.utils

data class MessageSignature(
    val message: String,
    val messageBytes: ByteArray,
    val signature: ByteArray
)