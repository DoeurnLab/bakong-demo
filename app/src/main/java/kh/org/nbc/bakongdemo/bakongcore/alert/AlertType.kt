package kh.org.nbc.bakongdemo.bakongcore.alert

enum class AlertType {
    TOAST,
    DIALOG
}