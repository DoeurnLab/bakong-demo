package kh.org.nbc.bakongdemo.screen.registration.keygenerator

import io.reactivex.Single

interface MnemonicInteractor {
    fun createMnemonic(): Single<String>

    fun entropyFromMnemonic(mnemonic: String): ByteArray
    fun deriveSeedFromMnemonic(mnemonic: String): ByteArray
}