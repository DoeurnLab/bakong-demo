package kh.org.nbc.bakongdemo.screen.send.data;

import java.math.BigDecimal;
import java.util.Objects;

public class PaymentPayload {

    private String mSenderAccountId;
    private String mReceiverAccountId;
    private String mAssetId;
    private String mDescription;
    private BigDecimal mAmount;
    private long mTime;

    public String getSenderAccountId() {
        return mSenderAccountId;
    }

    public void setSenderAccountId(String senderAccountId) {
        mSenderAccountId = senderAccountId;
    }

    public String getReceiverAccountId() {
        return mReceiverAccountId;
    }

    public void setReceiverAccountId(String receiverAccountId) {
        mReceiverAccountId = receiverAccountId;
    }

    public String getAssetId() {
        return mAssetId;
    }

    public void setAssetId(String assetId) {
        mAssetId = assetId;
    }

    public String getDescription() {
        return mDescription;
    }

    public void setDescription(String description) {
        mDescription = description;
    }

    public BigDecimal getAmount() {
        return mAmount;
    }

    public void setAmount(BigDecimal amount) {
        mAmount = amount;
    }

    public long getTime() {
        return mTime;
    }

    public void setTime(long time) {
        mTime = time;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PaymentPayload payload = (PaymentPayload) o;
        return mTime == payload.mTime &&
                Objects.equals(mSenderAccountId, payload.mSenderAccountId) &&
                Objects.equals(mReceiverAccountId, payload.mReceiverAccountId) &&
                Objects.equals(mAssetId, payload.mAssetId) &&
                Objects.equals(mDescription, payload.mDescription) &&
                Objects.equals(mAmount, payload.mAmount);
    }

    @Override
    public int hashCode() {
        return Objects.hash(mSenderAccountId, mReceiverAccountId, mAssetId, mDescription, mAmount, mTime);
    }

    @Override
    public String toString() {
        return "PaymentPayload{" +
                "mSenderAccountId='" + mSenderAccountId + '\'' +
                ", mReceiverAccountId='" + mReceiverAccountId + '\'' +
                ", mAssetId='" + mAssetId + '\'' +
                ", mDescription='" + mDescription + '\'' +
                ", mAmount=" + mAmount +
                ", mTime=" + mTime +
                '}';
    }
}
