package kh.org.nbc.bakongdemo.screen.deposit

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.util.Log
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import kh.org.nbc.bakongdemo.R
import kh.org.nbc.bakongdemo.application.BakongApplication
import kh.org.nbc.bakongdemo.base.BaseActivity
import kh.org.nbc.bakongdemo.screen.deposit.adapter.BankListAdapter
import kh.org.nbc.bakongdemo.utils.hide
import kh.org.nbc.bakongdemo.utils.show
import kotlinx.android.synthetic.main.activity_bank_list.*
import kotlinx.android.synthetic.main.layout_toolbar.*
import javax.inject.Inject

class BankListActivity : BaseActivity() {

    @Inject
    lateinit var viewModel: BankListViewModel

    override fun onCreate(savedInstanceState: Bundle?) {

        (application as BakongApplication).appComponent.inject(this)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_bank_list)


        viewModel.loading.observe(this, Observer {
            if (it){
                showLoading()
            }else hideLoading()
        })

        text_view_title.text = resources.getString(R.string.bank_list)
        button_back.show()
        button_back.setOnClickListener {
            finish()
        }

        viewModel.bankListResponse.observe(this, Observer { data ->
            rv_bank_list.adapter = BankListAdapter(this, data!!) {
                val intent = Intent(this, DepositActivity::class.java)
                intent.putExtra(PARTICIPANT_CODE, it.participantCode)
                intent.putExtra(BANK_ACCOUNT_ID, it.accountId)
                startActivityForResult(intent, DEPOSIT_REQUEST_CODE)
            }
            rv_bank_list.layoutManager = LinearLayoutManager(this)
        })

        viewModel.getBankList()

        viewModel.error.observe(this, Observer {
            onError(it)
        })
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK && requestCode == DEPOSIT_REQUEST_CODE){
            finish()
        }
    }

    private fun showLoading(){
        progressBar.show()
    }
    private fun hideLoading(){
        progressBar.hide()
    }

    companion object{
        const val PARTICIPANT_CODE = "participantCode"
        const val BANK_ACCOUNT_ID = "Bank account Id"
        const val DEPOSIT_REQUEST_CODE = 208
    }
}